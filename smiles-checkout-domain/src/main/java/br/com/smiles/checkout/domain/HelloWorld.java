package br.com.smiles.checkout.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class HelloWorld {

	@NotNull
	@Size(min=3)
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
