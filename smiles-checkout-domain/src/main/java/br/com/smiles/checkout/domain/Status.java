package br.com.smiles.checkout.domain;

public enum Status {

	NEW("NEW"), 
	IN_PROGRESS("IN_PROGRESS"), 
	CANCELLED("CANCELLED"), 
	PROCESSED("PROCESSED"),
	PENDING_PAYMENT("PENDING_PAYMENT"),
	PENDING_APPROVAL("PENDING_APPROVAL");
	
	private String status;
	
	Status(String status){
		this.status = status;
	}
	
	public String getStatus(){
		return status;
	}
	
}
