package br.com.smiles.checkout.domain;

/**
 * Created by adautomartins on 20/09/16.
 */
public class Total {

    Double money;
    Integer miles;

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Integer getMiles() {
        return miles;
    }

    public void setMiles(Integer miles) {
        this.miles = miles;
    }
}
