package br.com.smiles.checkout.domain;

import java.util.List;
import java.util.Date;

public class CheckoutOrderSearch {

	private Date dateInit;
	private Date dateEnd;
	private List<String> status;
	private Long id;
	private String memberId;
	private String recordLocator;
	private String productType;
	private String siebelTransactionId;
	private Boolean cancellationItem;


	public String getRecordLocator() {
		return recordLocator;
	}
	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}
	public Date getDateInit() {
		return dateInit;
	}
	public void setDateInit(Date dateInit) {
		this.dateInit = dateInit;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	public List<String> getStatus() {
		return status;
	}
	public void setStatus(List<String> status) {
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getSiebelTransactionId() {
		return siebelTransactionId;
	}
	public void setSiebelTransactionId(String siebelTransactionId) {
		this.siebelTransactionId = siebelTransactionId;
	}
	public Boolean getCancellationItem() {
		return cancellationItem;
	}
	public void setCancellationItem(Boolean cancellationItem) {
		this.cancellationItem = cancellationItem;
	}
}
