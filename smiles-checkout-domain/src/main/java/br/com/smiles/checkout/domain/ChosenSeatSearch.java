package br.com.smiles.checkout.domain;

import java.util.List;

public class ChosenSeatSearch {
	
	private List<String> status;
	private String memberNumber;
	private Long orderid;
	private String tokenGds;
	private String recordLocator;
	
	
	public List<String> getStatus() {
		return status;
	}
	public void setStatus(List<String> status) {
		this.status = status;
	}

	public String getMemberNumber() {
		return memberNumber;
	}
	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}
	public Long getOrderid() {
		return orderid;
	}
	public void setOrderid(Long orderid) {
		this.orderid = orderid;
	}
	public String getTokenGds() {
		return tokenGds;
	}
	public void setTokenGds(String tokenGds) {
		this.tokenGds = tokenGds;
	}
	public String getRecordLocator() {
		return recordLocator;
	}
	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}

}
