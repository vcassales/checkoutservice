package br.com.smiles.checkout.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T18:08:18.346-0300")
@StaticMetamodel(Receiver.class)
public class Receiver_ {
	public static volatile SingularAttribute<Receiver, Long> pkId;
	public static volatile SingularAttribute<Receiver, String> memberNumber;
	public static volatile SingularAttribute<Receiver, String> cpf;
	public static volatile SingularAttribute<Receiver, String> name;
	public static volatile SingularAttribute<Receiver, Miles> miles;
}
