package br.com.smiles.checkout.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T14:17:22.095-0300")
@StaticMetamodel(SiebelTransaction.class)
public class SiebelTransaction_ {
	public static volatile SingularAttribute<SiebelTransaction, String> id;
	public static volatile SingularAttribute<SiebelTransaction, String> type;
	public static volatile SingularAttribute<SiebelTransaction, String> transactionId;
	public static volatile SingularAttribute<SiebelTransaction, Item> item;
}
