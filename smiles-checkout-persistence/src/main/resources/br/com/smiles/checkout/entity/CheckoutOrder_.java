package br.com.smiles.checkout.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T18:08:18.234-0300")
@StaticMetamodel(CheckoutOrder.class)
public class CheckoutOrder_ {
	public static volatile SingularAttribute<CheckoutOrder, Long> id;
	public static volatile SingularAttribute<CheckoutOrder, Date> date;
	public static volatile SingularAttribute<CheckoutOrder, String> status;
	public static volatile SingularAttribute<CheckoutOrder, String> subStatus;
	public static volatile SingularAttribute<CheckoutOrder, MemberData> memberData;
	public static volatile ListAttribute<CheckoutOrder, Item> items;
}
