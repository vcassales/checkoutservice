package br.com.smiles.checkout.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T18:08:18.304-0300")
@StaticMetamodel(Miles.class)
public class Miles_ extends Item_ {
	public static volatile SingularAttribute<Miles, Integer> quantity;
	public static volatile SingularAttribute<Miles, String> optionId;
	public static volatile SingularAttribute<Miles, String> convenienceFee;
	public static volatile SingularAttribute<Miles, String> tax;
	public static volatile SingularAttribute<Miles, Double> unitCost;
	public static volatile SingularAttribute<Miles, Date> expiryDate;
	public static volatile SingularAttribute<Miles, String> operation;
	public static volatile SingularAttribute<Miles, Receiver> receiver;
	public static volatile SingularAttribute<Miles, Event> event;
}
