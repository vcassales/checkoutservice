package br.com.smiles.checkout.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T18:08:18.269-0300")
@StaticMetamodel(Item.class)
public class Item_ {
	public static volatile SingularAttribute<Item, Long> id;
	public static volatile SingularAttribute<Item, Date> date;
	public static volatile SingularAttribute<Item, String> status;
	public static volatile SingularAttribute<Item, String> subStatus;
	public static volatile SingularAttribute<Item, String> voucherNumber;
	public static volatile SingularAttribute<Item, Integer> maxInstallments;
	public static volatile SingularAttribute<Item, PaymentData> paymentData;
	public static volatile ListAttribute<Item, SiebelTransaction> siebelTransaction;
	public static volatile SingularAttribute<Item, CheckoutOrder> checkoutOrder;
}
