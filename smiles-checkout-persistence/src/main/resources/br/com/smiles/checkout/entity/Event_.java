package br.com.smiles.checkout.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T18:08:18.253-0300")
@StaticMetamodel(Event.class)
public class Event_ {
	public static volatile SingularAttribute<Event, Long> pkId;
	public static volatile SingularAttribute<Event, String> id;
	public static volatile SingularAttribute<Event, String> description;
	public static volatile SingularAttribute<Event, String> memberNumber;
	public static volatile SingularAttribute<Event, Miles> miles;
}
