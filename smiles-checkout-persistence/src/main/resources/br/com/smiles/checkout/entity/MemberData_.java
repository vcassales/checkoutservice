package br.com.smiles.checkout.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T18:08:18.291-0300")
@StaticMetamodel(MemberData.class)
public class MemberData_ {
	public static volatile SingularAttribute<MemberData, Long> id;
	public static volatile SingularAttribute<MemberData, String> memberNumber;
	public static volatile SingularAttribute<MemberData, String> documentNumber;
	public static volatile SingularAttribute<MemberData, String> country;
	public static volatile SingularAttribute<MemberData, CheckoutOrder> checkoutOrder;
}
