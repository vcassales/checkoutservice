package br.com.smiles.checkout.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T18:08:18.323-0300")
@StaticMetamodel(PaymentData.class)
public class PaymentData_ {
	public static volatile SingularAttribute<PaymentData, Long> id;
	public static volatile SingularAttribute<PaymentData, String> orderNumber;
	public static volatile SingularAttribute<PaymentData, String> amount;
	public static volatile SingularAttribute<PaymentData, String> shopper;
	public static volatile SingularAttribute<PaymentData, String> installments;
	public static volatile SingularAttribute<PaymentData, String> pspReference;
	public static volatile SingularAttribute<PaymentData, String> resultCode;
	public static volatile SingularAttribute<PaymentData, String> authCode;
	public static volatile SingularAttribute<PaymentData, String> transactionId;
	public static volatile SingularAttribute<PaymentData, String> refusalReason;
	public static volatile SingularAttribute<PaymentData, String> cardHolderName;
	public static volatile SingularAttribute<PaymentData, String> cardSummary;
	public static volatile SingularAttribute<PaymentData, String> expiryDate;
	public static volatile SingularAttribute<PaymentData, String> cardBin;
	public static volatile SingularAttribute<PaymentData, String> paymentMethodVariant;
	public static volatile SingularAttribute<PaymentData, Item> item;
}
