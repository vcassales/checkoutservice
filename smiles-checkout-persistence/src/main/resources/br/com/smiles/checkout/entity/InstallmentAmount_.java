package br.com.smiles.checkout.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-11T14:26:59.227-0300")
@StaticMetamodel(InstallmentAmount.class)
public class InstallmentAmount_ {
	public static volatile SingularAttribute<InstallmentAmount, Long> id;
	public static volatile SingularAttribute<InstallmentAmount, String> installment;
	public static volatile SingularAttribute<InstallmentAmount, Double> amount;
	public static volatile SingularAttribute<InstallmentAmount, PaymentData> paymentData;
}
