package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.domain.Total;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

/**
 * Entity implementation class for Entity: Order
 *
 */
@Entity
@Cacheable(false)
@NamedQueries ( {
	@NamedQuery(name = "findOrder", query = "select o FROM CheckoutOrder o WHERE (o.date between :dateIni and :dateEnd) AND (o.memberData.memberNumber = :memberID) ", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),	
	@NamedQuery(name = "findOrderStatus", query = "select o FROM CheckoutOrder o WHERE (o.date between :dateIni and :dateEnd) AND (o.status in :statusList) AND (o.memberData.memberNumber = :memberID) ", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findOrderRecordLocator", query = "select distinct o.checkoutOrder FROM BookingFlight o join o.chosenFlightSegment f WHERE (f.recordLocator = :recordLocator) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) UNION select distinct o.checkoutOrder FROM BookingFlightSeat o join o.chosenFlightSeats s WHERE (s.recordLocator = :recordLocator) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) UNION select distinct o.checkoutOrder FROM BookingFlightBaggage o join o.chosenFlightBaggages b WHERE (b.recordLocator = :recordLocator) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) UNION select distinct o.checkoutOrder FROM Product o join o.travel b WHERE (b.recordLocator = :recordLocator) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findOrderRecordLocatorStatus", query = "select distinct o.checkoutOrder FROM BookingFlight o join o.chosenFlightSegment f WHERE (f.recordLocator = :recordLocator) AND (o.checkoutOrder.status in :statusList) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) UNION select distinct o.checkoutOrder FROM BookingFlightSeat o join o.chosenFlightSeats s WHERE (s.recordLocator = :recordLocator) AND (o.checkoutOrder.status in :statusList) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) UNION select distinct o.checkoutOrder FROM BookingFlightBaggage o join o.chosenFlightBaggages b WHERE (b.recordLocator = :recordLocator) AND (o.checkoutOrder.status in :statusList) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) UNION select distinct o.checkoutOrder FROM Product o join o.travel b WHERE (b.recordLocator = :recordLocator) AND (o.checkoutOrder.status in :statusList) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findOrderProductType", query = "SELECT  o.checkoutOrder from Product o WHERE o.type = :productType AND (o.checkoutOrder.date between :dateIni and :dateEnd) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) and (:cancellationItem is null or o.isCancellation = :cancellationItem)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findOrderProductTypeStatus", query = "SELECT  o.checkoutOrder from Product o WHERE o.type = :productType AND (o.checkoutOrder.date between :dateIni and :dateEnd) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) and (:cancellationItem is null or o.isCancellation = :cancellationItem) AND (o.checkoutOrder.status in :statusList)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findOrderSiebeltransaction", query = "SELECT distinct o.checkoutOrder FROM Product o JOIN o.siebelTransaction t WHERE (:cancellationItem is null or o.isCancellation = :cancellationItem) AND (t.transactionId = :siebelTransactionId) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID) UNION SELECT distinct o.checkoutOrder FROM Product o JOIN o.travel t JOIN t.flightPassenger p JOIN p.siebelTransaction s WHERE (:cancellationItem is null or o.isCancellation = :cancellationItem) AND (s.transactionId = :siebelTransactionId) AND (:memberID is null or o.checkoutOrder.memberData.memberNumber = :memberID)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) )
})
@XmlRootElement
public class CheckoutOrder implements Serializable {

	@Id
	@SequenceGenerator( name = "CHECKOUTORDER_ID", sequenceName = "CHECKOUTORDER_SEQ", allocationSize = 3 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "CHECKOUTORDER_ID" )
	private Long id;
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "DATE_TIME")
	private Date date;
	private String status;
	private String subStatus;
	private String partnerAlias;
	private String promotionalChannel;
	
	@JoinFetch(JoinFetchType.INNER)
	@OneToOne(cascade = { PERSIST, MERGE }, mappedBy = "checkoutOrder")
	@XmlInverseReference(mappedBy = "checkoutOrder")
	@XmlElement
	private MemberData memberData;
	@JoinFetch(JoinFetchType.INNER)
	@OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "checkoutOrder")
	@XmlInverseReference(mappedBy = "checkoutOrder")
	@XmlElement
	private List<Item> items;
	private String channel;
	@Transient
	private Double totalCost;
	@Transient
	private Integer totalCostMiles;
	@Transient
	private Total totalRefund;
		
	@OneToOne(cascade = { PERSIST, MERGE }, mappedBy = "checkoutOrder")
	@XmlInverseReference(mappedBy = "checkoutOrder")
	@XmlElement
	private Benefit benefit;
	
	private String region;
	
	private String availableSmilesClubPlanId;
	
	
	public String getPromotionalChannel() {
		return promotionalChannel;
	}

	public void setPromotionalChannel(String promotionalChannel) {
		this.promotionalChannel = promotionalChannel;
	}

	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public MemberData getMemberData() {
		return memberData;
	}

	public void setMemberData(MemberData memberData) {
		this.memberData = memberData;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public Integer getTotalCostMiles() {
		return totalCostMiles;
	}

	public void setTotalCostMiles(Integer totalCostMiles) {
		this.totalCostMiles = totalCostMiles;
	}
	public Total getTotalRefund() {
		return totalRefund;
	}

	public void setTotalRefund(Total totalRefund) {
		this.totalRefund = totalRefund;
	}
	
	@Transient
	private Integer totalExpiredMiles;

	public Integer getTotalExpiredMiles() {
		return totalExpiredMiles;
	}

	public void setTotalExpiredMiles(Integer totalExpiredMiles) {
		this.totalExpiredMiles = totalExpiredMiles;
	}

	public String getPartnerAlias() {
		return partnerAlias;
	}

	public void setPartnerAlias(String partnerAlias) {
		this.partnerAlias = partnerAlias;
	}

	public Benefit getBenefit() {
		return benefit;
	}

	public void setBenefit(Benefit benefit) {
		this.benefit = benefit;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAvailableSmilesClubPlanId() {
		return availableSmilesClubPlanId;
	}

	public void setAvailableSmilesClubPlanId(String availableSmilesClubPlanId) {
		this.availableSmilesClubPlanId = availableSmilesClubPlanId;
	}
}
