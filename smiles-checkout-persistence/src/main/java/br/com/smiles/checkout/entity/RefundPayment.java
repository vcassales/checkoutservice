package br.com.smiles.checkout.entity;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by adautomartins on 15/08/16.
 */
@Entity
public class RefundPayment {
    private Double amount;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Id
    @SequenceGenerator( name = "REFUNDPAYMENT_ID", sequenceName = "REFUNDPAYMENT_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "REFUNDPAYMENT_ID" )
    @XmlTransient
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @XmlInverseReference(mappedBy = "refundPayment")
    @XmlElement
    private PaymentData paymentData;

    public PaymentData getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(PaymentData paymentData) {
        this.paymentData = paymentData;
    }
    
    
    private String cardBrand;
    private String pspReference;
	private String resultCode;
	private String authCode;
	private String transactionId;
	private String cardHolderName;
	private String cardSummary;
	private String expiryDate;
	private String cardBin;
	private String paymentMethodVariant;
	private String accountType;
	private String nsu;
	private String token;

	
	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	public String getPspReference() {
		return pspReference;
	}

	public void setPspReference(String pspReference) {
		this.pspReference = pspReference;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getCardSummary() {
		return cardSummary;
	}

	public void setCardSummary(String cardSummary) {
		this.cardSummary = cardSummary;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCardBin() {
		return cardBin;
	}

	public void setCardBin(String cardBin) {
		this.cardBin = cardBin;
	}

	public String getPaymentMethodVariant() {
		return paymentMethodVariant;
	}

	public void setPaymentMethodVariant(String paymentMethodVariant) {
		this.paymentMethodVariant = paymentMethodVariant;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getNsu() {
		return nsu;
	}

	public void setNsu(String nsu) {
		this.nsu = nsu;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
