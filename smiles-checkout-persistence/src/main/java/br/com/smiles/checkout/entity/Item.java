package br.com.smiles.checkout.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.InheritanceType.JOINED;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Item
 */
@Entity
@Cacheable(false)
@Inheritance(strategy = JOINED)
@DiscriminatorColumn(name = "PRODUCT_TYPE")
@NamedQueries({@NamedQuery(name = "findById", query = "SELECT e FROM Item e WHERE e.checkoutOrder.id = :checkoutOrder and e.id = :itemId", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE)), @NamedQuery(name = "findByOrderId", query = "SELECT e FROM Item e WHERE e.checkoutOrder.id = :checkoutOrder", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE))})
@XmlSeeAlso({Miles.class, BookingFlightSeat.class, BookingFlight.class, Fee.class,Membership.class,FamilyAccount.class,BookingFlightBaggage.class,Product.class})
public class Item<T extends Object> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4075795487229738622L;

    @Id
    @SequenceGenerator(name = "ITEM_ID", sequenceName = "ITEM_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ITEM_ID")
    private Long id;
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "DATE_TIME")
    private Date date;
    private String status;
    private String subStatus;
    private String voucherNumber;
    private String voucherType;
    private Integer maxInstallments;
    @JoinFetch(JoinFetchType.OUTER)
    @OneToOne(cascade = {PERSIST, MERGE, REMOVE}, mappedBy = "item")
    @XmlInverseReference(mappedBy = "item")
    @XmlElement
    private PaymentData paymentData;
    @ManyToOne
    @XmlInverseReference(mappedBy = "items")
    @XmlElement
    private CheckoutOrder checkoutOrder;
    
	@OneToOne(cascade = { PERSIST, MERGE }, mappedBy = "item")
	@XmlInverseReference(mappedBy = "item")
	@XmlElement
	private Benefit benefit;
	@XmlElement
    private Boolean isNoShow;
    @XmlElement
    private Boolean refundByNoShow;
	@XmlElement
	private Boolean recurringPayment;	

    public Item() {
        super();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubStatus() {
        return this.subStatus;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Integer getMaxInstallments() {
        return maxInstallments;
    }

    public void setMaxInstallments(Integer maxInstallments) {
        this.maxInstallments = maxInstallments;
    }

    public PaymentData getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(PaymentData paymentData) {
        this.paymentData = paymentData;
    }

    public CheckoutOrder getCheckoutOrder() {
        return checkoutOrder;
    }

    public void setCheckoutOrder(CheckoutOrder checkoutOrder) {
        this.checkoutOrder = checkoutOrder;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Transient
    private T totalCost;

    public T getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(T totalCost) {
        this.totalCost = totalCost;
    }


    @Transient
    private Integer totalExpiredMiles;

    public Integer getTotalExpiredMiles() {
        return totalExpiredMiles;
    }

    public void setTotalExpiredMiles(Integer totalExpiredMiles) {
        this.totalExpiredMiles = totalExpiredMiles;
    }

    @Basic
    @Column(columnDefinition = "NUMBER(1,0) default '0'", nullable = false)
    private Boolean isCancellation =  Boolean.FALSE;

    public Boolean getCancellation() {
        return isCancellation;
    }

    public void setCancellation(Boolean cancellation) {
        isCancellation = cancellation;
    }

	public Benefit getBenefit() {
		return benefit;
	}

	public void setBenefit(Benefit benefit) {
		this.benefit = benefit;
	}
    
	public Boolean getIsNoShow() {
        return isNoShow;
    }

    public void setIsNoShow(Boolean isNoShow) {
        this.isNoShow = isNoShow;
    }

    public Boolean getRefundByNoShow() {
        return refundByNoShow;
    }

    public void setRefundByNoShow(Boolean refundByNoShow) {
        this.refundByNoShow = refundByNoShow;
    }
	
	public Boolean getRecurringPayment() {
		return recurringPayment;
	}

	public void setRecurringPayment(Boolean recurringPayment) {
		this.recurringPayment = recurringPayment;
	} 
}
