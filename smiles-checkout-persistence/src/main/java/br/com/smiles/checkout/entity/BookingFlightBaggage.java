package br.com.smiles.checkout.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import br.com.smiles.checkout.domain.Total;


@Entity
@Cacheable(false)
@XmlRootElement
public class BookingFlightBaggage extends Item<Total> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7990895997026366722L;
	
	@Basic
    private String tokenGds;
	
	@JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name = "bookingFlightBaggage_id", columnDefinition = "id")
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "bookingFlightBaggage")
    @XmlInverseReference(mappedBy = "bookingFlightBaggage")
    @XmlElement
    private List<ChosenFlightBaggage> chosenFlightBaggages;

	public String getTokenGds() {
		return tokenGds;
	}

	public void setTokenGds(String tokenGds) {
		this.tokenGds = tokenGds;
	}

	public List<ChosenFlightBaggage> getChosenFlightBaggages() {
		return chosenFlightBaggages;
	}

	public void setChosenFlightBaggages(List<ChosenFlightBaggage> chosenFlightBaggages) {
		this.chosenFlightBaggages = chosenFlightBaggages;
	}
   
}
