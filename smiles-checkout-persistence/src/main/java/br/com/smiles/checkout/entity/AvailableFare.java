package br.com.smiles.checkout.entity;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by smiles on 23/01/19.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class AvailableFare {

    @Id
    @SequenceGenerator( name = "AVAILABLEFARE_ID", sequenceName = "AVAILABLEFARE_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "AVAILABLEFARE_ID" )
    private Long id;

    @Basic
    private String cacheId;
    
    @Basic
    private String type;
    
    @Basic
    private Integer miles;
    
    @Basic
    private Double money;
    
    @Basic
    private String highlightText;
    
    @Basic
    private Integer baseMiles;
    
    @Basic
    private Double airlineFareAmount;
    
    @Basic
    private Double airlineFare;
    
    @Basic
    private String sellKey;
    
    @Basic
    private String family;
    
    @Basic
    private String description;
    
    @Basic
    private String basics;
    
    @Basic
    private String bookingClass;
    
    @Basic
    private Double yieldSM;
    
    @Basic
    private Double yield;
    
    @Basic
    private Double loadFactor;
    
    @Basic
    private Integer choosedOffer;
    
    @Basic
    private Double publicValue;
    
    @Basic
    private String legListCost;
    
    @Basic
    private String legListCurrency;
    
    @Basic
    private String negotiatedFareCode;

    @ManyToOne
    @XmlInverseReference(mappedBy = "availableFare")
    @XmlElement
    private ChosenFlightSegment chosenFlightSegment;
    
    @Basic
    private String fareInfo;

    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCacheId() {
		return cacheId;
	}

	public void setCacheId(String cacheId) {
		this.cacheId = cacheId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getMiles() {
		return miles;
	}

	public void setMiles(Integer miles) {
		this.miles = miles;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public String getHighlightText() {
		return highlightText;
	}

	public void setHighlightText(String highlightText) {
		this.highlightText = highlightText;
	}

	public Integer getBaseMiles() {
		return baseMiles;
	}

	public void setBaseMiles(Integer baseMiles) {
		this.baseMiles = baseMiles;
	}

	public Double getAirlineFareAmount() {
		return airlineFareAmount;
	}

	public void setAirlineFareAmount(Double airlineFareAmount) {
		this.airlineFareAmount = airlineFareAmount;
	}

	public Double getAirlineFare() {
		return airlineFare;
	}

	public void setAirlineFare(Double airlineFare) {
		this.airlineFare = airlineFare;
	}

	public String getSellKey() {
		return sellKey;
	}

	public void setSellKey(String sellKey) {
		this.sellKey = sellKey;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBasics() {
		return basics;
	}

	public void setBasics(String basics) {
		this.basics = basics;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public Double getYieldSM() {
		return yieldSM;
	}

	public void setYieldSM(Double yieldSM) {
		this.yieldSM = yieldSM;
	}

	public Double getYield() {
		return yield;
	}

	public void setYield(Double yield) {
		this.yield = yield;
	}

	public Double getLoadFactor() {
		return loadFactor;
	}

	public void setLoadFactor(Double loadFactor) {
		this.loadFactor = loadFactor;
	}

	public Integer getChoosedOffer() {
		return choosedOffer;
	}

	public void setChoosedOffer(Integer choosedOffer) {
		this.choosedOffer = choosedOffer;
	}

	public Double getPublicValue() {
		return publicValue;
	}

	public void setPublicValue(Double publicValue) {
		this.publicValue = publicValue;
	}

	public String getLegListCost() {
		return legListCost;
	}

	public void setLegListCost(String legListCost) {
		this.legListCost = legListCost;
	}

	public String getLegListCurrency() {
		return legListCurrency;
	}

	public void setLegListCurrency(String legListCurrency) {
		this.legListCurrency = legListCurrency;
	}

	public String getNegotiatedFareCode() {
		return negotiatedFareCode;
	}

	public void setNegotiatedFareCode(String negotiatedFareCode) {
		this.negotiatedFareCode = negotiatedFareCode;
	}

	public ChosenFlightSegment getChosenFlightSegment() {
		return chosenFlightSegment;
	}

	public void setChosenFlightSegment(ChosenFlightSegment chosenFlightSegment) {
		this.chosenFlightSegment = chosenFlightSegment;
	}

	public String getFareInfo() {
		return fareInfo;
	}

	public void setFareInfo(String fareInfo) {
		this.fareInfo = fareInfo;
	}

}
