package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.domain.Total;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

/**
 * Created by adautomartins on 12/09/16.
 */
@Entity
@NamedQueries ( {
	@NamedQuery(name = "findChosenRecordLocator", query = "select o FROM ChosenFlightSegment o  WHERE (o.recordLocator = :recordLocator)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) )
})

@Cacheable(false)
@XmlRootElement
public class BookingFlight extends Item<Total> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1722623251751123392L;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Basic
    private String tokenGds;

    public String getTokenGds() {
        return tokenGds;
    }

    public void setTokenGds(String tokenGds) {
        this.tokenGds = tokenGds;
    }
    
	@Basic
    private String sourceGds;

    public String getSourceGds() {
        return sourceGds;
    }

    public void setSourceGds(String sourceGds) {
        this.sourceGds = sourceGds;
    }

    @Basic
    private String tripType;

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }
    
    @Basic
    private String cancellationType;

    public String getCancellationType() {
        return cancellationType;
    }

    public void setCancellationType(String cancellationType) {
        this.cancellationType = cancellationType;
    }

    @ElementCollection
    @CollectionTable
    private List<FlightSearch> flightSearch;

    public List<FlightSearch> getFlightSearch() {
        return flightSearch;
    }

    public void setFlightSearch(List<FlightSearch> flightSearch) {
        this.flightSearch = flightSearch;
    }

    @Basic
    private Integer adults;

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    @Basic
    private Integer children;

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    @Basic
    private Integer infants;

    public Integer getInfants() {
        return infants;
    }

    public void setInfants(Integer infants) {
        this.infants = infants;
    }

    @JoinFetch(JoinFetchType.INNER)
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "bookingFlight")
    @XmlInverseReference(mappedBy = "bookingFlight")
    @XmlElement
    private List<ChosenFlightSegment> chosenFlightSegment;

    public List<ChosenFlightSegment> getChosenFlightSegment() {
        return chosenFlightSegment;
    }

    public void setChosenFlightSegment(List<ChosenFlightSegment> chosenFlightSegment) {
        this.chosenFlightSegment = chosenFlightSegment;
    }

    @Basic
    private Boolean isInvoluntary;

    public Boolean getIsInvoluntary() {
		return isInvoluntary;
	}

	public void setIsInvoluntary(Boolean isInvoluntary) {
		this.isInvoluntary = isInvoluntary;
	}

	@Basic
    private Boolean mustTwoPnr;

    public Boolean getMustTwoPnr() {
        return mustTwoPnr;
    }

    public void setMustTwoPnr(Boolean mustTwoPnr) {
    	this.mustTwoPnr = mustTwoPnr;
    }
    
    @Basic
    private Boolean isFraud;

    public Boolean getIsFraud() {
		return isFraud;
	}

	public void setIsFraud(Boolean isFraud) {
		this.isFraud = isFraud;
	}

	@Transient
    private Total totalFlightFare;

    @Transient
    private Total totalBoardingTax;

    public Total getTotalFlightFare() {
        return totalFlightFare;
    }

    public void setTotalFlightFare(Total totalFlightFare) {
        this.totalFlightFare = totalFlightFare;
    }

    public Total getTotalBoardingTax() {
        return totalBoardingTax;
    }

    public void setTotalBoardingTax(Total totalBoardingTax) {
        this.totalBoardingTax = totalBoardingTax;
    }
    
    @Transient
    private Total totalAirlineTax;

    public Total getTotalAirlineTax() {
        return totalAirlineTax;
    }
    
    public void setTotalAirlineTax(Total totalAirlineTax) {
        this.totalAirlineTax = totalAirlineTax;
    }

    @Basic
    private Double discount;

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
    
    @Basic
    private Integer discountMiles;

    public Integer getDiscountMiles() {
        return discountMiles;
    }

    public void setDiscountMiles(Integer discountMiles) {
        this.discountMiles = discountMiles;
    }
    
    @Basic
    private String loanId;

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    
    }
    
    @Basic
    private String currencyCode;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
    
    @Transient
	private Integer subTotalCostMiles;
    
	public Integer getSubTotalCostMiles() {
		return subTotalCostMiles;
	}

	public void setSubTotalCostMiles(Integer subTotalCostMiles) {
		this.subTotalCostMiles = subTotalCostMiles;
	}
        
    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "bookingFlight")
    @XmlInverseReference(mappedBy = "bookingFlight")
    @XmlElement
    private List<SiebelTransaction> siebelTransaction;

    public List<SiebelTransaction> getSiebelTransaction() {
        return siebelTransaction;
    }

    public void setSiebelTransaction(List<SiebelTransaction> siebelTransaction) {
        this.siebelTransaction = siebelTransaction;
    }
        
	private Boolean isHasVoucher;
    
    public Boolean getIsHasVoucher() {
        return this.isHasVoucher;
    }

    public void setIsHasVoucher(Boolean isHasVoucher) {
    	this.isHasVoucher = isHasVoucher;
    }  
    
    @Basic
    private Integer availableDiscountMiles;

	public Integer getAvailableDiscountMiles() {
		return availableDiscountMiles;
	}

	public void setAvailableDiscountMiles(Integer availableDiscountMiles) {
		this.availableDiscountMiles = availableDiscountMiles;
	}
}
