package br.com.smiles.checkout.entity;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Table;

/**
 * Created by thiagopremoli on 22/04/18.
 */
@Embeddable
public class InsuranceService {
	@Basic
	private Integer code;
	
	@Basic
	private String description;
	
	@Basic
	private Double cost;

	
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

}
