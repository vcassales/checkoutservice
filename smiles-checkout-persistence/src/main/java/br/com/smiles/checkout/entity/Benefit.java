package br.com.smiles.checkout.entity;

import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Benefit
 *
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Benefit implements Serializable{
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 2806421523588536025L;
	
	@Id
	@SequenceGenerator( name = "BENEFIT_ID", sequenceName = "BENEFIT_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "BENEFIT_ID" )
	private Long id;
	private String cardBrand;
	private String product;
	private String description;
	private Integer bonus;
	private Double discount;
	private Boolean isFirstCard;
	private Boolean isFirstPurchase;
	private Boolean isClub;
	private String category;
	
	@XmlSchemaType(name = "date")
    @Temporal(value = TemporalType.DATE)
	private Date startDate;
	
	@XmlSchemaType(name = "date")
    @Temporal(value = TemporalType.DATE)
	private Date endDate;
	
	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "benefit")
	@XmlElement
	private CheckoutOrder checkoutOrder;
	
	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "benefit")
	@XmlElement
	private Item item;
	
	private String partner;
	
	private String bonusProduct;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getBonus() {
		return bonus;
	}

	public void setBonus(Integer bonus) {
		this.bonus = bonus;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Boolean getIsFirstCard() {
		return isFirstCard;
	}

	public void setIsFirstCard(Boolean isFirstCard) {
		this.isFirstCard = isFirstCard;
	}

	public Boolean getIsFirstPurchase() {
		return isFirstPurchase;
	}

	public void setIsFirstPurchase(Boolean isFirstPurchase) {
		this.isFirstPurchase = isFirstPurchase;
	}

	public Boolean getIsClub() {
		return isClub;
	}

	public void setIsClub(Boolean isClub) {
		this.isClub = isClub;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public CheckoutOrder getCheckoutOrder() {
		return checkoutOrder;
	}

	public void setCheckoutOrder(CheckoutOrder checkoutOrder) {
		this.checkoutOrder = checkoutOrder;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getBonusProduct() {
		return bonusProduct;
	}

	public void setBonusProduct(String bonusProduct) {
		this.bonusProduct = bonusProduct;
	}

}
