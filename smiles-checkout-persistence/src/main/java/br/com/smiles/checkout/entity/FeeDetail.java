package br.com.smiles.checkout.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

@Entity
@Cacheable(false)
@XmlRootElement
public class FeeDetail {
	@Id
	@SequenceGenerator(name = "FEEDETAIL_ID", sequenceName = "FEEDETAIL_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FEEDETAIL_ID")
	private Long id;
	private String sourceFare;
	private Integer miles;
	private Double money;

	@JoinFetch(JoinFetchType.OUTER)
    @OneToOne(cascade = {PERSIST, MERGE, REMOVE}, mappedBy = "feeDetail")
    @XmlInverseReference(mappedBy = "feeDetail")
    @XmlElement
    private PaymentData paymentData;
	
    @ManyToOne
    @JoinFetch(JoinFetchType.OUTER)
    @XmlInverseReference(mappedBy = "feeDetail")
    @XmlElement
	private Fee fee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSourceFare() {
		return sourceFare;
	}

	public void setSourceFare(String sourceFare) {
		this.sourceFare = sourceFare;
	}

	public Integer getMiles() {
		return miles;
	}

	public void setMiles(Integer miles) {
		this.miles = miles;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public PaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}

}
