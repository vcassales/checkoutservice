package br.com.smiles.checkout.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.smiles.checkout.entity.CheckoutOrder;
import br.com.smiles.checkout.entity.Item;

public class ItemDao {

    private EntityManagerFactory emf;
    private EntityManager        em;

    public void startConnection() {
        emf = Persistence.createEntityManagerFactory("smiles-checkout");
        em = emf.createEntityManager();
        em.getTransaction().begin();
    }

    public void closeConnection() {
        em.getTransaction().commit();
        emf.close();
    }

    public void updateStatus(Long orderId, Long itemId, String newStatus, String newSubStatus) {
        Item item = this.findById(orderId, itemId);
        item.setStatus(newStatus);
        item.setSubStatus(newSubStatus);
        
        em.merge(item);
    }

    public Item findById(Long orderId, Long itemId) {
        CheckoutOrder order = new CheckoutOrder();
        order.setId(orderId);
        return (Item) em.createNamedQuery("findById").setParameter("order", order).setParameter("itemId", itemId)
                .getSingleResult();
    }
    
    public static void main(String[] args) {
        ItemDao dao = new ItemDao();

        dao.startConnection();

        try {
            dao.updateStatus(7l, 8l, "NEW", "NEW_SUB");
        } catch (Exception e) {
            System.out.println("Ops, something happen: " + e.getMessage());
            e.printStackTrace();
        } finally {
            dao.closeConnection();
        }
    }
}
