package br.com.smiles.checkout.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.smiles.checkout.domain.Total;

@Entity
@Cacheable(false)
@XmlRootElement
public class FamilyAccount extends Item<Total> implements Serializable {
	
     @Basic
	 private String familyAccountId;
     
     @Basic
	 private String integrantType;
     
     @Basic
	 private String invitationCode;
     
     @Basic
	 private String invitationResponse;
     
     @Basic
	 private String eligibilityId;
     
	 @Basic
	 private Double money;
	 
	 @Basic
	 private String familyVinculationId;

	
	
	public String getFamilyAccountId() {
		return familyAccountId;
	}

	public void setFamilyAccountId(String familyAccountId) {
		this.familyAccountId = familyAccountId;
	}

	public String getIntegrantType() {
		return integrantType;
	}

	public void setIntegrantType(String integrantType) {
		this.integrantType = integrantType;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getInvitationResponse() {
		return invitationResponse;
	}

	public void setInvitationResponse(String invitationResponse) {
		this.invitationResponse = invitationResponse;
	}

	public String getEligibilityId() {
		return eligibilityId;
	}

	public void setEligibilityId(String eligibilityId) {
		this.eligibilityId = eligibilityId;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public String getFamilyVinculationId() {
		return familyVinculationId;
	}

	public void setFamilyVinculationId(String familyVinculationId) {
		this.familyVinculationId = familyVinculationId;
	}
}
