package br.com.smiles.checkout.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by adautomartins on 12/09/16.
 */
@Embeddable
@Cacheable(false)
@XmlRootElement
public class FlightSearch {
    private String originAirportCode;

    @Basic
    public String getOriginAirportCode() {
        return originAirportCode;
    }

    public void setOriginAirportCode(String originAirportCode) {
        this.originAirportCode = originAirportCode;
    }

    private String destinationAirportCode;

    @Basic
    public String getDestinationAirportCode() {
        return destinationAirportCode;
    }

    public void setDestinationAirportCode(String destinationAirportCode) {
        this.destinationAirportCode = destinationAirportCode;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE not null")
    private Calendar departureDate;

    @Basic
    public Calendar getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Calendar departureDate) {
        this.departureDate = departureDate;
    }

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private Calendar returnDate;

    @Basic
    public Calendar getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Calendar arrivalDate) {
        this.returnDate = arrivalDate;
    }
}
