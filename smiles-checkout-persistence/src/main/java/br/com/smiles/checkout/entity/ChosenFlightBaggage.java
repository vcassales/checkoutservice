package br.com.smiles.checkout.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import br.com.smiles.checkout.util.FlightDateAdapter;

@Entity
@Cacheable(false)
@NamedQueries ( {
	@NamedQuery(name = "findChosenFlightBaggageId", query = "select o FROM ChosenFlightBaggage o WHERE (o.id = :chosenId)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findChosenFlightBaggageStatus", query = "select o FROM ChosenFlightBaggage o WHERE (o.bookingFlightBaggage.checkoutOrder.status in :orderStatus) AND (:memberNumber is null or o.bookingFlightBaggage.checkoutOrder.memberData.memberNumber = :memberNumber) AND (:orderId is null or o.bookingFlightBaggage.checkoutOrder.id = :orderId) AND  (:tokenGds is null or o.bookingFlightBaggage.tokenGds = :tokenGds) AND (:recordLocator is null or o.recordLocator = :recordLocator)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findChosenFlightBaggage", query = "select o FROM ChosenFlightBaggage o WHERE (:orderStatus is null or o.bookingFlightBaggage.checkoutOrder.status in :orderStatus) AND (:memberNumber is null or o.bookingFlightBaggage.checkoutOrder.memberData.memberNumber = :memberNumber) AND (:orderId is null or o.bookingFlightBaggage.checkoutOrder.id = :orderId) AND  (:tokenGds is null or o.bookingFlightBaggage.tokenGds = :tokenGds) AND (:recordLocator is null or o.recordLocator = :recordLocator)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) )
})

@XmlRootElement
public class ChosenFlightBaggage {

    @Id
    @SequenceGenerator( name = "CHOSENFLIGHTBAGGAGE_ID", sequenceName = "CHOSENFLIGHTBAGGAGE_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "CHOSENFLIGHTBAGGAGE_ID" )
    private Long id;
 
	private String recordLocator;
    private String flightNumber;
    
    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    private Date departureDate;
    
    private String departureAirportCode;
    
    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    private Date arrivalDate;
    
    private String arrivalAirportCode;
    
    private String flightSellKey;
    
    @ManyToOne
    @XmlInverseReference(mappedBy = "chosenFlightBaggages")
    @XmlElement
    private BookingFlightBaggage bookingFlightBaggage;    
    
    @JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name = "chosenFlightBaggage_id", columnDefinition = "id")
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "chosenFlightBaggage")
    @XmlInverseReference(mappedBy = "chosenFlightBaggage")
    @XmlElement
    private List<Baggage> baggages;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRecordLocator() {
		return recordLocator;
	}

	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public BookingFlightBaggage getBookingFlightBaggage() {
		return bookingFlightBaggage;
	}

	public void setBookingFlightBaggage(BookingFlightBaggage bookingFlightBaggage) {
		this.bookingFlightBaggage = bookingFlightBaggage;
	}

	public List<Baggage> getBaggages() {
		return baggages;
	}

	public void setBaggages(List<Baggage> baggages) {
		this.baggages = baggages;
	}

	public String getFlightSellKey() {
		return flightSellKey;
	}

	public void setFlightSellKey(String flightSellKey) {
		this.flightSellKey = flightSellKey;
	}
	
}