package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.domain.Total;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

/**
 * Created by adautomartins on 13/09/16.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Fee extends Item<Total> implements Serializable {

	private static final long serialVersionUID = -5518433299728144216L;
	
	@Basic
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    private String subType;

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    @Basic
    private Integer miles;

    public Integer getMiles() {
        return miles;
    }

    public void setMiles(Integer miles) {
        this.miles = miles;
    }
    
    @Basic
    private Integer baseMiles;

    public Integer getBaseMiles() {
        return baseMiles;
    }

    public void setBaseMiles(Integer baseMiles) {
        this.baseMiles = baseMiles;
    }
    
    @Basic
    private Double money;

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
    
    @Basic
    private Double baseMoney;

    public Double getBaseMoney() {
        return baseMoney;
    }

    public void setBaseMoney(Double baseMoney) {
        this.baseMoney = baseMoney;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "fee")
    @XmlInverseReference(mappedBy = "fee")
    @XmlElement
    private List<SiebelTransaction> siebelTransaction;

    public List<SiebelTransaction> getSiebelTransaction() {
        return siebelTransaction;
    }

    public void setSiebelTransaction(List<SiebelTransaction> siebelTransaction) {
        this.siebelTransaction = siebelTransaction;
    }
  
    
	@JoinFetch(JoinFetchType.OUTER)
    @JoinColumn(name = "fee_id", columnDefinition = "id")
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "fee")
    @XmlInverseReference(mappedBy = "fee")
    @XmlElement
    private List<FeeDetail> feeDetail;

	public List<FeeDetail> getFeeDetail() {
		return feeDetail;
	}

	public void setFeeDetail(List<FeeDetail> feeDetail) {
		this.feeDetail = feeDetail;
	}

    
    @JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name = "fee_id", columnDefinition = "id")
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "fee")
    @XmlInverseReference(mappedBy = "fee")
    @XmlElement
    private List<FeeAvailableFare> feeAvailableFare;

    public List<FeeAvailableFare> getFeeAvailableFare() {
            return feeAvailableFare;
    }

    public void setFeeAvailableFare(List<FeeAvailableFare> feeAvailableFare) {
            this.feeAvailableFare = feeAvailableFare;
    }
}
