package br.com.smiles.checkout.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import br.com.smiles.checkout.domain.Total;

@Entity
@Cacheable(false)
@XmlRootElement
public class Product extends Item<Total> implements Serializable{

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -866021089077270629L;
	
    private String type;
	private String subType;
	private Double creditValue;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date sendApprovalDate;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date returnAnalysisDate;
	
	private String gift;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date giftDate;
	
	private String transactionId;
        private Integer miles;
        private Double money;
        private Integer bonusMiles;
    
    
	@Temporal(value = TemporalType.TIMESTAMP)
    private Date cancelDate;
    
   
    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "product")
    @XmlInverseReference(mappedBy = "product")
    @XmlElement
    private List<SiebelTransaction> siebelTransaction;
    
    
    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "product")
    @XmlInverseReference(mappedBy = "product")
    @XmlElement
    private List<Travel> travel;


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getSubType() {
		return subType;
	}


	public void setSubType(String subType) {
		this.subType = subType;
	}


	public Double getCreditValue() {
		return creditValue;
	}


	public void setCreditValue(Double creditValue) {
		this.creditValue = creditValue;
	}


	public Date getSendApprovalDate() {
		return sendApprovalDate;
	}


	public void setSendApprovalDate(Date sendApprovalDate) {
		this.sendApprovalDate = sendApprovalDate;
	}

	public String getGift() {
		return gift;
	}


	public void setGift(String gift) {
		this.gift = gift;
	}


	public Date getGiftDate() {
		return giftDate;
	}


	public void setGiftDate(Date giftDate) {
		this.giftDate = giftDate;
	}

	public String getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	public Integer getMiles() {
		return miles;
	}


	public void setMiles(Integer miles) {
		this.miles = miles;
	}


	public Double getMoney() {
		return money;
	}


	public void setMoney(Double money) {
		this.money = money;
	}


	public Integer getBonusMiles() {
		return bonusMiles;
	}


	public void setBonusMiles(Integer bonusMiles) {
		this.bonusMiles = bonusMiles;
	}


	public Date getCancelDate() {
		return cancelDate;
	}


	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}


	public List<SiebelTransaction> getSiebelTransaction() {
		return siebelTransaction;
	}


	public void setSiebelTransaction(List<SiebelTransaction> siebelTransaction) {
		this.siebelTransaction = siebelTransaction;
	}


	public Date getReturnAnalysisDate() {
		return returnAnalysisDate;
	}


	public void setReturnAnalysisDate(Date returnAnalysisDate) {
		this.returnAnalysisDate = returnAnalysisDate;
	}


	public List<Travel> getTravel() {
		return travel;
	}


	public void setTravel(List<Travel> travel) {
		this.travel = travel;
	}
}
