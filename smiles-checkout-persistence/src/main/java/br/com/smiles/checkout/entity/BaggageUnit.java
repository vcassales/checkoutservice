package br.com.smiles.checkout.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@NamedQueries ( {
	@NamedQuery(name = "findBaggageUnitById", query = "select o FROM BaggageUnit o WHERE (o.id = :bagUnitId)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) )
})
@Cacheable(false)
@XmlRootElement
public class BaggageUnit implements Serializable {
	@Id
	@SequenceGenerator( name = "BAGGAGEUNIT_ID", sequenceName = "BAGGAGEUNIT_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "BAGGAGEUNIT_ID" )
	private String id;
	
	private Integer unitNumber;
	private Integer costMiles;
    private Double costMoney;
    
    @ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "baggageUnit")
	@XmlElement
	private Baggage baggage;
    
    
    private static final long serialVersionUID = 1L;

    
	public BaggageUnit() {
		super();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Integer getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(Integer unitNumber) {
		this.unitNumber = unitNumber;
	}

	public Integer getCostMiles() {
		return costMiles;
	}

	public void setCostMiles(Integer costMiles) {
		this.costMiles = costMiles;
	}

	public Double getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(Double costMoney) {
		this.costMoney = costMoney;
	}

	public Baggage getBaggage() {
		return baggage;
	}

	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}
}
