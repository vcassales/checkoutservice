package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.domain.Total;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

import java.util.Date;
import java.util.List;

/**
 * Created by adautomartins on 12/09/16.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class FlightPassenger {

    @Id
    @SequenceGenerator( name = "FLIGHTPASSENGER_ID", sequenceName = "FLIGHTPASSENGER_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "FLIGHTPASSENGER_ID" )
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @XmlInverseReference(mappedBy = "flightPassenger")
    @XmlElement
    private ChosenFlightSegment chosenFlightSegment;

    public ChosenFlightSegment getChosenFlightSegment() {
        return chosenFlightSegment;
    }

    public void setChosenFlightSegment(ChosenFlightSegment chosenFlightSegment) {
        this.chosenFlightSegment = chosenFlightSegment;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "flightPassenger")
    @XmlInverseReference(mappedBy = "flightPassenger")
    @XmlElement
    private List<SiebelTransaction> siebelTransaction;

    public List<SiebelTransaction> getSiebelTransaction() {
        return siebelTransaction;
    }

    public void setSiebelTransaction(List<SiebelTransaction> siebelTransaction) {
        this.siebelTransaction = siebelTransaction;
    }

    private Integer indexGds;

    @Basic
    public Integer getIndexGds() {
        return indexGds;
    }

    public void setIndexGds(Integer indexGds) {
        this.indexGds = indexGds;
    }

    private String type;

    @Basic
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String firstName;

    @Basic
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String lastName;

    @Basic
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String gender;

    @Basic
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @XmlSchemaType(name = "date")
    @Temporal(value = TemporalType.DATE)
    private Date birthday;

    @Basic
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    private String email;

    @Basic
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String redressNumber;

    @Basic
    public String getRedressNumber() {
        return redressNumber;
    }

    public void setRedressNumber(String redressNumber) {
        this.redressNumber = redressNumber;
    }

    private String passportNumber;

    @Basic
    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    @XmlSchemaType(name = "date")
    @Temporal(value = TemporalType.DATE)
    private Date passportExpireDate;

    @Basic
    public Date getPassportExpireDate() {
        return passportExpireDate;
    }

    public void setPassportExpireDate(Date passportExpireDate) {
        this.passportExpireDate = passportExpireDate;
    }

    private String passportCountry;

    @Basic
    public String getPassportCountry() {
        return passportCountry;
    }

    public void setPassportCountry(String passportCountry) {
        this.passportCountry = passportCountry;
    }

    @ElementCollection
    @CollectionTable(name = "FlightPassenger_SSR")
    private List<PassengerSpecialService> requestSpecialService;

    public List<PassengerSpecialService> getRequestSpecialService() {
        return requestSpecialService;
    }

    public void setRequestSpecialService(List<PassengerSpecialService> requestSpecialService) {
        this.requestSpecialService = requestSpecialService;
    }

    @Basic
    private String ticketNumber;

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    @Basic
    private Double interestValue;

    public Double getInterestValue() {
        return interestValue;
    }

    public void setInterestValue(Double interestValue) {
        this.interestValue = interestValue;
    }
    
    @Transient
    private Integer realFareMiles;
    public Integer getRealFareMiles() {
        return realFareMiles;
    }

    public void setRealFareMiles(Integer realFareMiles) {
        this.realFareMiles = realFareMiles;
    }
    
    @Basic
    private String memberNumber;

	public String getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}
	
	
	@JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "flightPassenger")
    @XmlInverseReference(mappedBy = "flightPassenger")
    @XmlElement
    private List<Document> document;

	public List<Document> getDocument() {
		return document;
	}

	public void setDocument(List<Document> document) {
		this.document = document;
	}
	
	@Basic
	private String tier;

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}
	
	@Basic
	private Double fareCost;

	@Basic
	private Double boardingTaxCost;

	
	public Double getFareCost() {
		return fareCost;
	}

	public void setFareCost(Double fareCost) {
		this.fareCost = fareCost;
	}

	public Double getBoardingTaxCost() {
		return boardingTaxCost;
	}

	public void setBoardingTaxCost(Double boardingTaxCost) {
		this.boardingTaxCost = boardingTaxCost;
	}
	
	@ManyToOne
    @XmlInverseReference(mappedBy = "flightPassenger")
    @XmlElement
    private Travel travel;

	public Travel getTravel() {
		return travel;
	}

	public void setTravel(Travel travel) {
		this.travel = travel;
	}
	
	@OneToOne(cascade = { PERSIST, MERGE }, mappedBy = "flightPassenger")
	@XmlInverseReference(mappedBy = "flightPassenger")
	@XmlElement
	private TravelInsurance travelInsurance;

	public TravelInsurance getTravelInsurance() {
		return travelInsurance;
	}

	public void setTravelInsurance(TravelInsurance travelInsurance) {
		this.travelInsurance = travelInsurance;
	}
	
	@OneToOne(cascade = { PERSIST, MERGE }, mappedBy = "flightPassenger")
	@XmlInverseReference(mappedBy = "flightPassenger")
	@XmlElement
    private Address address;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Basic
	private String phoneNumber;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Basic
	private String nationality;

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

}
