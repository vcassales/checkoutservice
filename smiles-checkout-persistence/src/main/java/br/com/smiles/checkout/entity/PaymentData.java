package br.com.smiles.checkout.entity;

import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class PaymentData implements Serializable {
	@Id
	@SequenceGenerator( name = "PAYMENTDATA_ID", sequenceName = "PAYMENTDATA_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "PAYMENTDATA_ID" )
	private Long id;

	// Request params for authorization
	private String orderNumber;
	private String amount;
	private String shopper;
	private String installments;
	private String cardBrand;

	// Response params for authorization
	private String pspReference;
	private String resultCode;
	private String authCode;
	private String transactionId;
	private String refusalReason;
	private String cardHolderName;
	private String cardSummary;
	private String expiryDate;
	private String cardBin;
	private String paymentMethodVariant;
	private String accountType;

	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "paymentData")
	@XmlElement
	private Item item;
	@JoinFetch(JoinFetchType.OUTER)
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "paymentData")
	@XmlInverseReference(mappedBy = "paymentData")
	@XmlElement
	private List<InstallmentAmount> installmentAmount;
	@JoinFetch(JoinFetchType.OUTER)
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "paymentData")
	@XmlInverseReference(mappedBy = "paymentData")
	@XmlElement
	private List<RefundPayment> refundPayment;
	
	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "paymentData")
	@XmlElement
	private ChosenFlightSegment chosenFlightSegment;
	
	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "paymentData")
	@XmlElement
	private FeeDetail feeDetail;
	
	public List<RefundPayment> getRefundPayment() {
		return refundPayment;
	}

	public void setRefundPayment(List<RefundPayment> refundPayment) {
		this.refundPayment = refundPayment;
	}

	private String nsu;
	
	private String token;
	
	private String paymentServiceType;

	

	private static final long serialVersionUID = 1L;

	public PaymentData() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getShopper() {
		return shopper;
	}

	public void setShopper(String shopper) {
		this.shopper = shopper;
	}

	public String getInstallments() {
		return installments;
	}

	public void setInstallments(String installments) {
		this.installments = installments;
	}

	public String getCardBrand() {
		return cardBrand;
	}

	public void setCardBrand(String cardBrand) {
		this.cardBrand = cardBrand;
	}

	public String getPspReference() {
		return pspReference;
	}

	public void setPspReference(String pspReference) {
		this.pspReference = pspReference;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getRefusalReason() {
		return refusalReason;
	}

	public void setRefusalReason(String refusalReason) {
		this.refusalReason = refusalReason;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getCardSummary() {
		return cardSummary;
	}

	public void setCardSummary(String cardSummary) {
		this.cardSummary = cardSummary;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCardBin() {
		return cardBin;
	}

	public void setCardBin(String cardBin) {
		this.cardBin = cardBin;
	}

	public String getPaymentMethodVariant() {
		return paymentMethodVariant;
	}

	public void setPaymentMethodVariant(String paymentMethodVariant) {
		this.paymentMethodVariant = paymentMethodVariant;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public List<InstallmentAmount> getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(List<InstallmentAmount> installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public String getNsu() {
		return nsu;
	}

	public void setNsu(String nsu) {
		this.nsu = nsu;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPaymentServiceType() {
		return paymentServiceType;
	}

	public void setPaymentServiceType(String paymentServiceType) {
		this.paymentServiceType = paymentServiceType;
	}

	public ChosenFlightSegment getChosenFlightSegment() {
		return chosenFlightSegment;
	}

	public void setChosenFlightSegment(ChosenFlightSegment chosenFlightSegment) {
		this.chosenFlightSegment = chosenFlightSegment;
	}

	public FeeDetail getFeeDetail() {
		return feeDetail;
	}

	public void setFeeDetail(FeeDetail feeDetail) {
		this.feeDetail = feeDetail;
	}
}
