package br.com.smiles.checkout.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;
import java.util.Date;

/**
 * Created by thiago on 04/07/17.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Document {
	
	 @Id
	    @SequenceGenerator( name = "DOCUMENT_ID", sequenceName = "DOCUMENT_SEQ", allocationSize = 1 )
	    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "DOCUMENT_ID" )
	    private Long id;

	    public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }
	
	
    @Basic
    private String documentNumber;
    
    public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}


	@Basic
    private String documentType;

    public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}


	@Basic
    private String issuerOrganization;
    
    public String getIssuerOrganization() {
		return issuerOrganization;
	}

	public void setIssuerOrganization(String issuerOrganization) {
		this.issuerOrganization = issuerOrganization;
	}


	@Basic
    private String issuerState;

    public String getIssuerState() {
		return issuerState;
	}

	public void setIssuerState(String issuerState) {
		this.issuerState = issuerState;
	}


	@XmlSchemaType(name = "date")
    @Temporal(value = TemporalType.DATE)
    private Date expeditionDate;

    @Basic
    public Date getExpeditionDate() {
        return expeditionDate;
    }

    public void setExpeditionDate(Date expeditionDate) {
        this.expeditionDate = expeditionDate;
    }


	@XmlSchemaType(name = "date")
    @Temporal(value = TemporalType.DATE)
    private Date validityDate;

    @Basic
    public Date getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(Date validityDate) {
        this.validityDate = validityDate;
    }
    

    @ManyToOne
    @XmlInverseReference(mappedBy = "document")
    @XmlElement
    private FlightPassenger flightPassenger;

	public FlightPassenger getFlightPassenger() {
		return flightPassenger;
	}

	public void setFlightPassenger(FlightPassenger flightPassenger) {
		this.flightPassenger = flightPassenger;
	}
    
    
}
