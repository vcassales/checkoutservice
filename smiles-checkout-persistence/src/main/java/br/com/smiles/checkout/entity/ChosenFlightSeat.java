package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.domain.Total;
import br.com.smiles.checkout.util.FlightDateAdapter;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.List;

/**
 * Created by adautomartins on 11/08/16.
 */
@Entity
@Cacheable(false)
@NamedQueries ( {
	@NamedQuery(name = "findChosenFlightSeatId", query = "select o FROM ChosenFlightSeat o WHERE (o.id = :chosenId)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findChosenFlightSeatStatus", query = "select o FROM ChosenFlightSeat o WHERE (o.bookingFlightSeat.checkoutOrder.status in :orderStatus) AND (:memberNumber is null or o.bookingFlightSeat.checkoutOrder.memberData.memberNumber = :memberNumber) AND (:orderId is null or o.bookingFlightSeat.checkoutOrder.id = :orderId) AND  (:tokenGds is null or o.bookingFlightSeat.tokenGds = :tokenGds) AND (:recordLocator is null or o.recordLocator = :recordLocator)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) ),
	@NamedQuery(name = "findChosenFlightSeat", query = "select o FROM ChosenFlightSeat o WHERE (:orderStatus is null or o.bookingFlightSeat.checkoutOrder.status in :orderStatus) AND (:memberNumber is null or o.bookingFlightSeat.checkoutOrder.memberData.memberNumber = :memberNumber) AND (:orderId is null or o.bookingFlightSeat.checkoutOrder.id = :orderId) AND  (:tokenGds is null or o.bookingFlightSeat.tokenGds = :tokenGds) AND (:recordLocator is null or o.recordLocator = :recordLocator)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) )
})
@XmlRootElement
public class ChosenFlightSeat {
    private String flightNumber;

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    private String recordLocator;

    public String getRecordLocator() {
        return recordLocator;
    }

    public void setRecordLocator(String recordLocator) {
        this.recordLocator = recordLocator;
    }

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    private Date departureDate;

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    private String departureAirportCode;

    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    private Date arrivalDate;

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    private String arrivalAirportCode;

    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    private String passengerId;

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    private String seatNumber;

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatDesignator) {
        this.seatNumber = seatDesignator;
    }
    
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    private Integer costMiles;

    public Integer getCostMiles() {
        return costMiles;
    }

    public void setCostMiles(Integer costMiles) {
        this.costMiles = costMiles;
    }

    private Double costMoney;

    public Double getCostMoney() {
        return costMoney;
    }

    public void setCostMoney(Double costMoney) {
        this.costMoney = costMoney;
    }

    @ManyToOne
    @XmlInverseReference(mappedBy = "chosenFlightSeats")
    @XmlElement
    private BookingFlightSeat bookingFlightSeat;

    public BookingFlightSeat getBookingFlightSeat() {
        return bookingFlightSeat;
    }

    public void setBookingFlightSeat(BookingFlightSeat bookingFlightSeat) {
        this.bookingFlightSeat = bookingFlightSeat;
    }

    @Id
    @SequenceGenerator( name = "CHOSEN_FLIGHT_SEAT_ID", sequenceName = "CHOSEN_FLIGHT_SEAT_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "CHOSEN_FLIGHT_SEAT_ID" )
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @JoinFetch(JoinFetchType.OUTER)
    @OneToOne
    @XmlInverseReference(mappedBy = "chosenFlightSeatChange")
    @XmlElement
    private ChosenFlightSeat chosenFlightSeatOlder;

    public ChosenFlightSeat getChosenFlightSeatOlder() {
        return chosenFlightSeatOlder;
    }

    public void setChosenFlightSeatOlder(ChosenFlightSeat chosenFlightSeatOlder) {
        this.chosenFlightSeatOlder = chosenFlightSeatOlder;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany
    @JoinColumn(name = "chosenFlightSeatOlder_id", columnDefinition = "id")
    @XmlElement
    private List<ChosenFlightSeat> chosenFlightSeatChange;

    public List<ChosenFlightSeat> getChosenFlightSeatChange() {
        return chosenFlightSeatChange;
    }

    public void setChosenFlightSeatChange(List<ChosenFlightSeat> chosenFlightSeatChange) {
        this.chosenFlightSeatChange = chosenFlightSeatChange;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "chosenFlightSeat")
    @XmlInverseReference(mappedBy = "chosenFlightSeat")
    @XmlElement
    private SiebelTransaction siebelTransaction;

    public SiebelTransaction getSiebelTransaction() {
        return siebelTransaction;
    }

    public void setSiebelTransaction(SiebelTransaction siebelTransaction) {
        this.siebelTransaction = siebelTransaction;
    }

    private String chosenCost;

    public String getChosenCost() {
        return chosenCost;
    }

    public void setChosenCost(String chosenCost) {
        this.chosenCost = chosenCost;
    }
    
    private String firstName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
