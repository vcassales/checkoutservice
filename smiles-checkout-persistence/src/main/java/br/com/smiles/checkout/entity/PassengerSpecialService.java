package br.com.smiles.checkout.entity;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Table;

/**
 * Created by adautomartins on 12/09/16.
 */
@Embeddable
public class PassengerSpecialService {
    private String specialServiceCode;

    @Basic
    public String getSpecialServiceCode() {
        return specialServiceCode;
    }

    public void setSpecialServiceCode(String specialServiceCode) {
        this.specialServiceCode = specialServiceCode;
    }
}
