package br.com.smiles.checkout.entity;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Vinicius Cassales
 */

@Entity
@Cacheable(false)
@XmlRootElement
public class FeeAvailableFare {
    @Id
    @SequenceGenerator( name = "FEEAVAILABLEFARE_ID", sequenceName = "FEEAVAILABLEFARE_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "FEEAVAILABLEFARE_ID" )
    private Long id;

    @Basic
    private String cacheId;
    
    @Basic
    private Integer offer;
    
    @Basic
    private Integer miles;
    
    @Basic
    private Double money;
    
    @ManyToOne
    @XmlInverseReference(mappedBy = "feeAvailableFare")
    @XmlElement
    private Fee fee;

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getCacheId() {
        return cacheId;
    }
    
    public void setCacheId(String cacheId) {
        this.cacheId = cacheId;
    }

    public Integer getOffer() {
        return offer;
    }
     
    public void setOffer(Integer offer) {
        this.offer = offer;
    }
    
    public Integer getMiles() {
        return miles;
    }
    
    public void setMiles(Integer miles) {
        this.miles = miles;
    }

    public Double getMoney() {
        return money;
    }
    
    public void setMoney(Double money) {
        this.money = money;
    }

     public Fee getFee() {
        return fee;
    }
     
    public void setFee(Fee fee) {
        this.fee = fee;
    }

}
