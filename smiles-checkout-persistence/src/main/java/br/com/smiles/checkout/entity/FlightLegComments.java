package br.com.smiles.checkout.entity;

import javax.persistence.Basic;
import javax.persistence.Embeddable;


/**
 * Created by adautomartins on 12/09/16.
 */
@Embeddable
public class FlightLegComments {
    private String comments;

    @Basic
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
