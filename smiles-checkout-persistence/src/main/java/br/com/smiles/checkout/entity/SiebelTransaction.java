package br.com.smiles.checkout.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@NamedQueries ( {
	@NamedQuery(name = "findSiebelTransactionById", query = "select o FROM SiebelTransaction o WHERE (o.id = :siebelId)", hints = @QueryHint(name = QueryHints.BIND_PARAMETERS, value = HintValues.TRUE) )
})
@Cacheable(false)
@XmlRootElement
public class SiebelTransaction implements Serializable {
	@Id
	@SequenceGenerator( name = "SIEBELTRANSACTION_ID", sequenceName = "SIEBELTRANSACTION_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SIEBELTRANSACTION_ID" )
	private String id;
	private String type;
	private String transactionId;
	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	private Miles miles;
	@OneToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private ChosenFlightSeat chosenFlightSeat;

	private static final long serialVersionUID = 1L;

	public SiebelTransaction() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Miles getMiles() {
		return miles;
	}

	public void setMiles(Miles miles) {
		this.miles = miles;
	}

	public ChosenFlightSeat getChosenFlightSeat() {
		return chosenFlightSeat;
	}

	public void setChosenFlightSeat(ChosenFlightSeat chosenFlightSeat) {
		this.chosenFlightSeat = chosenFlightSeat;
	}

	private Boolean cancelled;

	public Boolean getCancelled() {
		return cancelled;
	}

	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private FlightPassenger flightPassenger;

	public FlightPassenger getFlightPassenger() {
		return flightPassenger;
	}

	public void setFlightPassenger(FlightPassenger flightPassenger) {
		this.flightPassenger = flightPassenger;
	}

	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private Fee fee;

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}
	
	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private FamilyAccount familyAccount;

	public FamilyAccount getFamilyAccount() {
		return familyAccount;
	}

	public void setFamilyAccount(FamilyAccount familyAccount) {
		this.familyAccount = familyAccount;
	}
	
	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private Membership membership;

	public Membership getMembership() {
		return membership;
	}

	public void setMembership(Membership membership) {
		this.membership = membership;
	}
	
	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private BookingFlight bookingFlight;

	public BookingFlight getBookingFlight() {
		return bookingFlight;
	}

	public void setBookingFlight(BookingFlight bookingFlight) {
		this.bookingFlight = bookingFlight;
	}

	@Basic
	private Integer expiredMiles;

	public Integer getExpiredMiles() {
		return expiredMiles;
	}

	public void setExpiredMiles(Integer expiredMiles) {
		this.expiredMiles = expiredMiles;
	}

	@JoinFetch(JoinFetchType.OUTER)
	@OneToOne
	@XmlInverseReference(mappedBy = "siebelTransactionChange")
	@XmlElement
	private SiebelTransaction siebelTransactionOlder;

	public SiebelTransaction getSiebelTransactionOlder() {
		return siebelTransactionOlder;
	}

	public void setSiebelTransactionOlder(SiebelTransaction siebelTransactionOlder) {
		this.siebelTransactionOlder = siebelTransactionOlder;
	}

	@JoinFetch(JoinFetchType.OUTER)
	@OneToMany
	@JoinColumn(name = "siebelTransactionOlder_id", columnDefinition = "id")
	@XmlElement
	private List<SiebelTransaction> siebelTransactionChange;

	public List<SiebelTransaction> getSiebelTransactionChange() {
		return siebelTransactionChange;
	}

	public void setSiebelTransactionChange(List<SiebelTransaction> siebelTransactionChange) {
		this.siebelTransactionChange = siebelTransactionChange;
	}
	
	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private Baggage baggage;
	
	public Baggage getBaggage() {
		return baggage;
	}

	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}
	
	@ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "siebelTransaction")
	@XmlElement
	private Product product;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	
	private String serviceRequestNumber;

	public String getServiceRequestNumber() {
		return serviceRequestNumber;
	}

	public void setServiceRequestNumber(String serviceRequestNumber) {
		this.serviceRequestNumber = serviceRequestNumber;
	}
	
	
}
