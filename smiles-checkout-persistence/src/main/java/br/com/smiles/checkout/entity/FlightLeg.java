package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.util.FlightDateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import java.util.Date;
import java.util.List;

/**
 * Created by adautomartins on 12/09/16.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class FlightLeg {
	
	 @Id
	    @SequenceGenerator( name = "FLIGHTLEG_ID", sequenceName = "FLIGHTLEG_SEQ", allocationSize = 1 )
	    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "FLIGHTLEG_ID" )
	    private Long id;

	    public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }
	
	
    @Basic
    private String cabin;

    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(columnDefinition = "TIMESTAMP not null")
    private Date departureDate;

    @Basic
    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @Basic
    private String departureAirportCode;

    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(columnDefinition = "TIMESTAMP not null")
    private Date arrivalDate;

    @Basic
    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    @Basic
    private String arrivalAirportCode;

    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    @Basic
    private String marketingAirlineCode;

    public String getMarketingAirlineCode() {
        return marketingAirlineCode;
    }

    public void setMarketingAirlineCode(String marketingAirlineCode) {
        this.marketingAirlineCode = marketingAirlineCode;
    }

    @Basic
    private String operationAirlineCode;

    public String getOperationAirlineCode() {
        return operationAirlineCode;
    }

    public void setOperationAirlineCode(String operationAirlineCode) {
        this.operationAirlineCode = operationAirlineCode;
    }

    @Basic
    private Integer flightNumber;

    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }
    
    @Basic
    private String equipment;

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }
    
    @Basic
    private Boolean main;

    public Boolean getMain() {
        return main;
    }

    public void setMain(Boolean main) {
    	this.main = main;
    }
    
    @ElementCollection
    @CollectionTable(name = "FlightLeg_Comments")
    private List<FlightLegComments> flightLegComments;

    public List<FlightLegComments> getFlightLegComments() {
        return flightLegComments;
    }

    public void setFlightLegComments(List<FlightLegComments> flightLegComments) {
        this.flightLegComments = flightLegComments;
    }

    @ManyToOne
    @XmlInverseReference(mappedBy = "flightLeg")
    @XmlElement
    private ChosenFlightSegment chosenFlightSegment;

    public ChosenFlightSegment getChosenFlightSegment() {
        return chosenFlightSegment;
    }

    public void setChosenFlightSegment(ChosenFlightSegment chosenFlightSegment) {
        this.chosenFlightSegment = chosenFlightSegment;
    }
    
    private String bookingClass;

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
	
	
	@Basic
    private Boolean original;
	
	@Basic
    private Integer stops;

	
	public Boolean getOriginal() {
		return original;
	}

	public void setOriginal(Boolean original) {
		this.original = original;
	}

	public Integer getStops() {
		return stops;
	}

	public void setStops(Integer stops) {
		this.stops = stops;
	}
	
	private Double loadFactor;
	
	public Double getLoadFactor() {
		return loadFactor;
	}
	
	public void setLoadFactor(Double loadFactor) {
		this.loadFactor = loadFactor;
	}
    
}
