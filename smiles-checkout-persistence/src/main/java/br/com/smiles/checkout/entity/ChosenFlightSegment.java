package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.domain.Total;
import br.com.smiles.checkout.util.FlightDateAdapter;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;

/**
 * Created by adautomartins on 12/09/16.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class ChosenFlightSegment {

    @Id
    @SequenceGenerator( name = "CHOSENFLIGHTSEGMENT_ID", sequenceName = "CHOSENFLIGHTSEGMENT_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "CHOSENFLIGHTSEGMENT_ID" )
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String type;

    @Basic
    public String getType() {
        return type;
    }

    public void setType(String basic) {
        this.type = basic;
    }

    @ManyToOne
    @XmlInverseReference(mappedBy = "chosenFlightSegment")
    @XmlElement
    private BookingFlight bookingFlight;

    public BookingFlight getBookingFlight() {
        return bookingFlight;
    }

    public void setBookingFlight(BookingFlight bookingFlight) {
        this.bookingFlight = bookingFlight;
    }

    private String recordLocator;

    @Basic
    public String getRecordLocator() {
        return recordLocator;
    }

    public void setRecordLocator(String recordLocator) {
        this.recordLocator = recordLocator;
    }

    private String cabin;

    @Basic
    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    private String departureAirportCode;

    @Basic
    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(columnDefinition = "TIMESTAMP not null")
    private Date departureDate;

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    private String arrivalAirportCode;

    @Basic
    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(columnDefinition = "TIMESTAMP not null")
    private Date arrivalDate;

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    private String chosenFareType;

    @Basic
    public String getChosenFareType() {
        return chosenFareType;
    }

    public void setChosenFareType(String chosenFareType) {
        this.chosenFareType = chosenFareType;
    }

    private String chosenFareHighlightText;

    @Basic
    public String getChosenFareHighlightText() {
        return chosenFareHighlightText;
    }

    public void setChosenFareHighlightText(String chosenFareHighlightText) {
        this.chosenFareHighlightText = chosenFareHighlightText;
    }
    
    private Integer chosenFareMiles;

    @Basic
    public Integer getChosenFareMiles() {
        return chosenFareMiles;
    }

    public void setChosenFareMiles(Integer chosenFareMiles) {
        this.chosenFareMiles = chosenFareMiles;
    }

    @Transient
    private Integer realFareMiles;
    public Integer getRealFareMiles() {
        return realFareMiles;
    }

    public void setRealFareMiles(Integer realFareMiles) {
        this.realFareMiles = realFareMiles;
    }
    
    private Integer chosenFareBaseMiles;

    @Basic
    public Integer getChosenFareBaseMiles() {
        return chosenFareBaseMiles;
    }

    public void setChosenFareBaseMiles(Integer chosenFareBaseMiles) {
        this.chosenFareBaseMiles = chosenFareBaseMiles;
    }
    
    private Double chosenFareMoney;

    @Basic
    public Double getChosenFareMoney() {
        return chosenFareMoney;
    }

    public void setChosenFareMoney(Double chosenFareMoney) {
        this.chosenFareMoney = chosenFareMoney;
    }

    private Integer chosenTaxMiles;

    @Basic
    public Integer getChosenTaxMiles() {
        return chosenTaxMiles;
    }

    public void setChosenTaxMiles(Integer chosenTaxMiles) {
        this.chosenTaxMiles = chosenTaxMiles;
    }

    private Double chosenTaxMoney;

    @Basic
    public Double getChosenTaxMoney() {
        return chosenTaxMoney;
    }

    public void setChosenTaxMoney(Double chosenTaxMoney) {
        this.chosenTaxMoney = chosenTaxMoney;
    }
    
    private Double chosenAirlineTax;

    @Basic
    public Double getChosenAirlineTax() {
        return chosenAirlineTax;
    }

    public void setChosenAirlineTax(Double chosenAirlineTax) {
        this.chosenAirlineTax = chosenAirlineTax;
    }
    
    private Double cancellationTax;

    @Basic
    public Double getCancellationTax() {
        return cancellationTax;
    }

    public void setCancellationTax(Double cancellationTax) {
        this.cancellationTax = cancellationTax;
    }

    private String chosenTaxType;

    @Basic
    public String getChosenTaxType() {
        return chosenTaxType;
    }

    public void setChosenTaxType(String chosenTaxType) {
        this.chosenTaxType = chosenTaxType;
    }
    
    private String airlineRecordLocator;

    @Basic
    public String getAirlineRecordLocator() {
        return airlineRecordLocator;
    }

    public void setAirlineRecordLocator(String airlineRecordLocator) {
        this.airlineRecordLocator = airlineRecordLocator;
    }
    
    private String luggageRestriction;

    @Basic
    public String getLuggageRestriction() {
        return luggageRestriction;
    }

    public void setLuggageRestriction(String luggageRestriction) {
        this.luggageRestriction = luggageRestriction;
    }
    
    private String tripType;
    
    @Basic
    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "chosenFlightSegment")
    @XmlInverseReference(mappedBy = "chosenFlightSegment")
    @XmlElement
    private List<FlightPassenger> flightPassenger;

    public List<FlightPassenger> getFlightPassenger() {
        return flightPassenger;
    }

    public void setFlightPassenger(List<FlightPassenger> flightPassenger) {
        this.flightPassenger = flightPassenger;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "chosenFlightSegment")
    @XmlInverseReference(mappedBy = "chosenFlightSegment")
    @XmlElement
    private List<FlightLeg> flightLeg;

    public List<FlightLeg> getFlightLeg() {
        return flightLeg;
    }

    public void setFlightLeg(List<FlightLeg> flightLeg) {
        this.flightLeg = flightLeg;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToOne
    @XmlInverseReference(mappedBy = "chosenFlightSegmentOlder")
    @XmlElement
    private ChosenFlightSegment chosenFlightSegmentOlder;

    public ChosenFlightSegment getChosenFlightSegmentOlder() {
        return chosenFlightSegmentOlder;
    }

    public void setChosenFlightSegmentOlder(ChosenFlightSegment chosenFlightSegmentOlder) {
        this.chosenFlightSegmentOlder = chosenFlightSegmentOlder;
    }

    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany
    @JoinColumn(name = "chosenFlightSegmentOlder_id", columnDefinition = "id")
    @XmlElement
    private List<ChosenFlightSegment> chosenFlightSegmentChange;

    public List<ChosenFlightSegment> getChosenFlightSegmentChange() {
        return chosenFlightSegmentChange;
    }

    public void setChosenFlightSegmentChange(List<ChosenFlightSegment> chosenFlightSegmentChange) {
        this.chosenFlightSegmentChange = chosenFlightSegmentChange;
    }

    @Basic
    private Double airlineFareAmount;

    public Double getAirlineFareAmount() {
        return airlineFareAmount;
    }

    public void setAirlineFareAmount(Double airlineFareAmount) {
        this.airlineFareAmount = airlineFareAmount;
    }
    
    @Basic
    private Double airlineFare;

    public Double getAirlineFare() {
        return airlineFare;
    }

    public void setAirlineFare(Double airlineFare) {
        this.airlineFare = airlineFare;
    }
    
    private String fareSellKey;

    @Basic
    public String getFareSellKey() {
        return fareSellKey;
    }

    public void setFareSellKey(String fareSellKey) {
        this.fareSellKey = fareSellKey;
    }
    
    @Basic
    private String flightSellKey;

    public String getFlightSellKey() {
        return flightSellKey;
    }

    public void setFlightSellKey(String flightSellKey) {
        this.flightSellKey = flightSellKey;
    }

    @Basic
    private String fareFamily;

    public String getFareFamily() {
        return fareFamily;
    }

    public void setFareFamily(String fareFamily) {
        this.fareFamily = fareFamily;
    }
    
    @Basic
    private String fareDescription;

    public String getFareDescription() {
        return fareDescription;
    }

    public void setFareDescription(String fareDescription) {
        this.fareDescription = fareDescription;
    }
    
    @Basic
    private String fareBasics;

    public String getFareBasics() {
        return fareBasics;
    }

    public void setFareBasics(String fareBasics) {
        this.fareBasics = fareBasics;
    }
    
    @Basic
    private String bookingClass;

    public String getBookingClass() {
        return bookingClass;
    }

    public void setBookingClass(String bookingClass) {
        this.bookingClass = bookingClass;
    }
    
    
    @Transient
    private Total totalFlightFare;

    @Transient
    private Total totalBoardingTax;

    public Total getTotalFlightFare() {
        return totalFlightFare;
    }

    public void setTotalFlightFare(Total totalFlightFare) {
        this.totalFlightFare = totalFlightFare;
    }

    public Total getTotalBoardingTax() {
        return totalBoardingTax;
    }

    public void setTotalBoardingTax(Total totalBoardingTax) {
        this.totalBoardingTax = totalBoardingTax;
    }

    @Basic
    private Integer stops;

    public Integer getStops() {
        return stops;
    }

    public void setStops(Integer stops) {
        this.stops = stops;
    }

    @Basic
    private Integer durationHours;

    public Integer getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(Integer durationHours) {
        this.durationHours = durationHours;
    }
    
    @Basic
    private Integer durationMinutes;

    public Integer getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(Integer durationMinutes) {
        this.durationMinutes = durationMinutes;
    }
    
    @ElementCollection
    @CollectionTable(name = "ChosenFlightSegment_DATTR")
    private List<DynamicAttribute> dynamicAttribute;

    public List<DynamicAttribute> getDynamicAttribute() {
        return dynamicAttribute;
    }

    public void setDynamicAttribute(List<DynamicAttribute> dynamicAttribute) {
        this.dynamicAttribute = dynamicAttribute;
    }
        
    private String sourceFare;
    private String cacheId;
    @Basic
    private Boolean refundable;
    private String cancellationCurrency;
    
    @JoinFetch(JoinFetchType.OUTER)
    @OneToOne(cascade = {PERSIST, MERGE, REMOVE}, mappedBy = "chosenFlightSegment")
    @XmlInverseReference(mappedBy = "chosenFlightSegment")
    @XmlElement
    private PaymentData paymentData;
    
    private String chosenFareCacheId;
    
    private Double conversionRate;
    
    private String negotiatedFareCode;

    private String validatingAirlineCode;
    
    private String airlineCodeContext;

    private Double yieldSM;
    
    @Column(name="YIELDFARE")
    private Double yield;
    
    private Double loadFactor;
    
    @Column(name="CHOOSEDOFFER")
    private Integer opcaoSM;
    
    @Column(name="PUBLICVALUE")
    private Double publishedValue;

    @Transient
    private Double fareValue;
    
    private String legListCost;
    
    private String legListCurrency;
    
	public String getSourceFare() {
		return sourceFare;
	}

	public void setSourceFare(String sourceFare) {
		this.sourceFare = sourceFare;
	}

	public String getCacheId() {
		return cacheId;
	}

	public void setCacheId(String cacheId) {
		this.cacheId = cacheId;
	}

	public Boolean getRefundable() {
		return refundable;
	}

	public void setRefundable(Boolean refundable) {
		this.refundable = refundable;
	}

	public String getCancellationCurrency() {
		return cancellationCurrency;
	}

	public void setCancellationCurrency(String cancellationCurrency) {
		this.cancellationCurrency = cancellationCurrency;
	}

	public PaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}

	public String getChosenFareCacheId() {
		return chosenFareCacheId;
	}

	public void setChosenFareCacheId(String chosenFareCacheId) {
		this.chosenFareCacheId = chosenFareCacheId;
	}
	
	public Double getConversionRate() {
		return conversionRate;
	}

    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }

	public String getNegotiatedFareCode() {
		return negotiatedFareCode;
	}

	public void setNegotiatedFareCode(String negotiatedFareCode) {
		this.negotiatedFareCode = negotiatedFareCode;
	}
    
	
	public void setValidatingAirlineCode(String validatingAirlineCode) {
		this.validatingAirlineCode = validatingAirlineCode;
	}
	public String getValidatingAirlineCode() {
		return validatingAirlineCode;
	}

	public String getAirlineCodeContext() {
		return airlineCodeContext;
	}

	public void setAirlineCodeContext(String airlineCodeContext) {
		this.airlineCodeContext = airlineCodeContext;
	}

	/**
	 * @return the yieldSM
	 */
	public Double getYieldSM() {
		return yieldSM;
	}

	/**
	 * @param yieldSM the yieldSM to set
	 */
	public void setYieldSM(Double yieldSM) {
		this.yieldSM = yieldSM;
	}

	/**
	 * @return the yield
	 */
	public Double getYield() {
		return yield;
	}

	/**
	 * @param yield the yield to set
	 */
	public void setYield(Double yield) {
		this.yield = yield;
	}

	/**
	 * @return the loadFactor
	 */
	public Double getLoadFactor() {
		return loadFactor;
	}

	/**
	 * @param loadFactor the loadFactor to set
	 */
	public void setLoadFactor(Double loadFactor) {
		this.loadFactor = loadFactor;
	}

	/**
	 * @return the opcaoSM
	 */
	public Integer getOpcaoSM() {
		return opcaoSM;
	}

	/**
	 * @param opcaoSM the opcaoSM to set
	 */
	public void setOpcaoSM(Integer opcaoSM) {
		this.opcaoSM = opcaoSM;
	}

	/**
	 * @return the publishedValue
	 */
	public Double getPublishedValue() {
		return publishedValue;
	}

	/**
	 * @param publishedValue the publishedValue to set
	 */
	public void setPublishedValue(Double publishedValue) {
		this.publishedValue = publishedValue;
	}

	/**
	 * @return the fareValue
	 */
	public Double getFareValue() {
		return fareValue;
	}

	/**
	 * @param fareValue the fareValue to set
	 */
	public void setFareValue(Double fareValue) {
		this.fareValue = fareValue;
	}

	/**
	 * @return the legListCost
	 */
	public String getLegListCost() {
		return legListCost;
	}
	/**
	 * @param legListCost the legListCost to set
	 */
	public void setLegListCost(String legListCost) {
		this.legListCost = legListCost;
	}
	/**
	 * @return the legListCurrency
	 */
	public String getLegListCurrency() {
		return legListCurrency;
	}
	/**
	 * @param legListCurrency the legListCurrency to set
	 */
	public void setLegListCurrency(String legListCurrency) {
		this.legListCurrency = legListCurrency;
	}
	
	private Double currencyRate;

	public Double getCurrencyRate() {
		return currencyRate;
	}

	public void setCurrencyRate(Double currencyRate) {
		this.currencyRate = currencyRate;
	}
	
	@JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "chosenFlightSegment")
    @XmlInverseReference(mappedBy = "chosenFlightSegment")
    @XmlElement
    private List<AvailableFare> availableFare;

	public List<AvailableFare> getAvailableFare() {
		return availableFare;
	}

	public void setAvailableFare(List<AvailableFare> availableFare) {
		this.availableFare = availableFare;
	}
	
	private String chosenFareInfo;

	public String getChosenFareInfo() {
		return chosenFareInfo;
	}

	public void setChosenFareInfo(String chosenFareInfo) {
		this.chosenFareInfo = chosenFareInfo;
	}

}
