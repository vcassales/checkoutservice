package br.com.smiles.checkout.entity;

import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Receiver
 *
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Receiver implements Serializable {
	@Id
	@SequenceGenerator( name = "RECEIVER_ID", sequenceName = "RECEIVER_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "RECEIVER_ID" )
	@XmlTransient
	private Long pkId;

	private String memberNumber;
	private String cpf;
	private String name;
	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "receiver")
	@XmlElement
	private Miles miles;
	private Integer smilesClubMilesBonus;
	private Integer milesBonus;

	private static final long serialVersionUID = 1L;

	public Receiver() {
		super();
	}

	public Long getPkId() {
		return pkId;
	}

	public void setPkId(Long pkId) {
		this.pkId = pkId;
	}

	public String getMemberNumber() {
		return this.memberNumber;
	}

	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}

	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Miles getMiles() {
		return miles;
	}

	public void setMiles(Miles miles) {
		this.miles = miles;
	}
	
	public Integer getMilesBonus() {
		return milesBonus;
	}

	public void setMilesBonus(Integer milesBonus) {
		this.milesBonus = milesBonus;
	}
	
	public Integer getSmilesClubMilesBonus() {
		return smilesClubMilesBonus;
	}

	public void setSmilesClubMilesBonus(Integer smilesClubMilesBonus) {
		this.smilesClubMilesBonus = smilesClubMilesBonus;
	}
}
