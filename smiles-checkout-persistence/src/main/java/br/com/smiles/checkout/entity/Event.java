package br.com.smiles.checkout.entity;

import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Event implements Serializable {
	@Id
	@SequenceGenerator( name = "EVENT_ID", sequenceName = "EVENT_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "EVENT_ID" )
	@XmlTransient
	private Long pkId;

	private String id;
	private String description;
	private String memberNumber;
	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "event")
	@XmlElement
	private Miles miles;
	private Integer smilesClubMilesBonus;
	private Integer milesBonus;
	private static final long serialVersionUID = 1L;

	public Event() {
		super();
	}

	public Long getPkId() {
		return pkId;
	}

	public void setPkId(Long pkId) {
		this.pkId = pkId;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMemberNumber() {
		return this.memberNumber;
	}

	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}

	public Miles getMiles() {
		return miles;
	}

	public void setMiles(Miles miles) {
		this.miles = miles;
	}
	
	public Integer getSmilesClubMilesBonus() {
		return smilesClubMilesBonus;
	}

	public void setSmilesClubMilesBonus(Integer smilesClubMilesBonus) {
		this.smilesClubMilesBonus = smilesClubMilesBonus;
	}
	
	public Integer getMilesBonus() {
		return milesBonus;
	}

	public void setMilesBonus(Integer milesBonus) {
		this.milesBonus = milesBonus;
	}
}
