package br.com.smiles.checkout.entity;

import static javax.persistence.FetchType.LAZY;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

@Entity
@Cacheable(false)
@XmlRootElement
public class TravelInsurance {
	
    @Id
    @SequenceGenerator( name = "TRAVELINSURANCE_ID", sequenceName = "TRAVELINSURANCE_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "TRAVELINSURANCE_ID" )
    private Long id;
    
    @Basic
    private String code;
    
    @Basic
    private String name;
    
    @Basic
    private Integer costMiles;
    
    @Basic
    private Double costMoney;
    
    @Basic
    private Long policy;
    
    @Basic
    private String lotteryNumber;
    
    @Basic
    private Integer travellerId;
    
    @Basic
    private Integer orderNumber;
    
    @OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "travelInsurance")
	@XmlElement
	private FlightPassenger flightPassenger;
    
    @ElementCollection
    @CollectionTable(name = "InsuranceCoverage")
    private List<InsuranceCoverage> coverage;
    
    @ElementCollection
    @CollectionTable(name = "InsuranceService")
    private List<InsuranceService> service;
    
    @Basic
    private String chosenCostType;
    
    
    @JoinFetch(JoinFetchType.OUTER)
	@OneToOne
	@XmlInverseReference(mappedBy = "travelInsuranceChange")
	@XmlElement
	private TravelInsurance travelInsuranceOlder;
    

	@JoinFetch(JoinFetchType.OUTER)
	@OneToMany
	@JoinColumn(name = "travelInsuranceOlder_id", columnDefinition = "id")
	@XmlElement
	private List<TravelInsurance> travelInsuranceChange;

	
    
    
	public Integer getCostMiles() {
		return costMiles;
	}

	public void setCostMiles(Integer costMiles) {
		this.costMiles = costMiles;
	}

	public Double getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(Double costMoney) {
		this.costMoney = costMoney;
	}

	public String getChosenCostType() {
		return chosenCostType;
	}

	public void setChosenCostType(String chosenCostType) {
		this.chosenCostType = chosenCostType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPolicy() {
		return policy;
	}

	public void setPolicy(Long policy) {
		this.policy = policy;
	}

	public String getLotteryNumber() {
		return lotteryNumber;
	}

	public void setLotteryNumber(String lotteryNumber) {
		this.lotteryNumber = lotteryNumber;
	}

	public Integer getTravellerId() {
		return travellerId;
	}

	public void setTravellerId(Integer travellerId) {
		this.travellerId = travellerId;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public FlightPassenger getFlightPassenger() {
		return flightPassenger;
	}

	public void setFlightPassenger(FlightPassenger flightPassenger) {
		this.flightPassenger = flightPassenger;
	}

	public List<InsuranceCoverage> getCoverage() {
		return coverage;
	}

	public void setCoverage(List<InsuranceCoverage> coverage) {
		this.coverage = coverage;
	}

	public List<InsuranceService> getService() {
		return service;
	}

	public void setService(List<InsuranceService> service) {
		this.service = service;
	}

	public TravelInsurance getTravelInsuranceOlder() {
		return travelInsuranceOlder;
	}

	public void setTravelInsuranceOlder(TravelInsurance travelInsuranceOlder) {
		this.travelInsuranceOlder = travelInsuranceOlder;
	}

	public List<TravelInsurance> getTravelInsuranceChange() {
		return travelInsuranceChange;
	}

	public void setTravelInsuranceChange(List<TravelInsurance> travelInsuranceChange) {
		this.travelInsuranceChange = travelInsuranceChange;
	}


}
