package br.com.smiles.checkout.entity;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class InstallmentAmount implements Serializable {
	@Id
	@SequenceGenerator( name = "INSTALLMENTAMOUNT_ID", sequenceName = "INSTALLMENTAMOUNT_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "INSTALLMENTAMOUNT_ID" )
	@XmlTransient
	private Long id;

	private String installment;
	private Double amount;

	@ManyToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "installmentAmount")
	@XmlElement
	private PaymentData paymentData;
	
	
	private static final long serialVersionUID = 1L;

	public InstallmentAmount() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInstallment() {
		return installment;
	}

	public void setInstallment(String installment) {
		this.installment = installment;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public PaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}
}
