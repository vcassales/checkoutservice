package br.com.smiles.checkout.entity;

import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Event
 *
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class MemberData implements Serializable {
	@Id
	@SequenceGenerator( name = "MEMBERDATA_ID", sequenceName = "MEMBERDATA_SEQ", allocationSize = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "MEMBERDATA_ID" )
	@XmlTransient
	private Long id;

	private String memberNumber;
	private String documentNumber;
	private String country;
	@OneToOne(fetch = LAZY)
	@XmlInverseReference(mappedBy = "memberData")
	@XmlElement
	private CheckoutOrder checkoutOrder;
	private static final long serialVersionUID = 1L;

	public MemberData() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public CheckoutOrder getCheckoutOrder() {
		return checkoutOrder;
	}

	public void setCheckoutOrder(CheckoutOrder checkoutOrder) {
		this.checkoutOrder = checkoutOrder;
	}

	@Basic
	private String citizenship;

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	@Basic
	private String tier;

	public String getTier() {
		return tier;
	}

	public void setTier(String tier) {
		this.tier = tier;
	}
	
	@Basic
	private Boolean smilesClub;

	public Boolean getSmilesClub() {
		return smilesClub;
	}

	public void setSmilesClub(Boolean smilesClub) {
		this.smilesClub = smilesClub;
	}
}
