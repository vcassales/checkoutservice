package br.com.smiles.checkout.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import static javax.persistence.FetchType.LAZY;

import java.util.Date;
import java.util.List;

/**
 * Created by thiago on 22/07/17.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Address {
	
	 @Id
	 @SequenceGenerator( name = "ADDRESS_ID", sequenceName = "ADDRESS_SEQ", allocationSize = 1 )
	 @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "ADDRESS_ID" )
	 private Long id;

	 @Basic
	 private String streetName;
    
	 @Basic
	 private String addressNumber;
    
	 @Basic
	 private String complement;
    
	 @Basic
	 private String neighborhood;
    
	 @Basic
	 private String city;
    
	 @Basic
	 private String state;
	    
	 @Basic
	 private String zipCode;
    
	 @OneToOne(fetch = LAZY)
	 @XmlInverseReference(mappedBy = "address")
	 @XmlElement
	 private FlightPassenger flightPassenger;

	 
	 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public FlightPassenger getFlightPassenger() {
		return flightPassenger;
	}

	public void setFlightPassenger(FlightPassenger flightPassenger) {
		this.flightPassenger = flightPassenger;
	}
	
}
