package br.com.smiles.checkout.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import br.com.smiles.checkout.domain.Total;

@Entity
@Cacheable(false)
@XmlRootElement
public class Membership extends Item<Total> implements Serializable {
	

		@Basic
		private String planId;
		
		@Basic
		private Integer expirationDay;
		
		@Basic
		private String referralMemberNumber;
		
		@Basic
		private String indicationCode;
		
		@Basic
		private Boolean isRegularization;
		
		@Basic
		private String signatureCode;
		
		@Basic
		private Double money;
	
		@Basic
		private Integer milesQuantity;
		
		@Basic
		private Boolean isSameCard;

		@Basic
		private Boolean saveCard;
		
		@Basic
		private Integer qualifyingMilesQuantity;
		
		@Basic
		private String type;
		
		@Basic
		private String subType;
		
		@Basic
		private Double fullCostMoney;
		
		@Basic
		private Integer validity;
		
		@Basic
		private Integer gracePeriod;
		
		@Basic
		private String installmentOptions;
		
		@Basic
		private Boolean automaticRenewal;
		
		@Basic
		private Boolean isFraud;
		
		@Basic
		private Boolean revertMiles;
		
		@Basic
		private Boolean immediateCancellation;
		
		@JoinFetch(JoinFetchType.OUTER)
	    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "membership")
	    @XmlInverseReference(mappedBy = "membership")
	    @XmlElement
	    private List<SiebelTransaction> siebelTransaction;

		@Basic
		private Double refundMoney;
		
		
		
		public Integer getMilesQuantity() {
			return milesQuantity;
		}
		
		public void setMilesQuantity(Integer milesQuantity) {
			this.milesQuantity = milesQuantity;
		}
		
		public String getPlanId() {
			return planId;
		}
		
		public void setPlanId(String planId) {
			this.planId = planId;
		}
		
		public Integer getExpirationDay() {
			return expirationDay;
		}
		
		public void setExpirationDay(Integer expirationDay) {
			this.expirationDay = expirationDay;
		}
		
		public String getReferralMemberNumber() {
			return referralMemberNumber;
		}
		
		public void setReferralMemberNumber(String referralMemberNumber) {
			this.referralMemberNumber = referralMemberNumber;
		}
		
		public String getIndicationCode() {
			return indicationCode;
		}
		
		public void setIndicationCode(String indicationCode) {
			this.indicationCode = indicationCode;
		}
		
		public Boolean getIsRegularization() {
			return isRegularization;
		}
		
		public void setIsRegularization(Boolean isRegularization) {
			this.isRegularization = isRegularization;
		}
		
		public String getSignatureCode() {
			return signatureCode;
		}
		
		public void setSignatureCode(String signatureCode) {
			this.signatureCode = signatureCode;
		}
		
		public Double getMoney() {
			return money;
		}
		
		public void setMoney(Double money) {
			this.money = money;
		}
		
		public Boolean getIsSameCard() {
			return isSameCard;
		}
		
		public void setIsSameCard(Boolean isSameCard) {
			this.isSameCard = isSameCard;
		}
		
		public Boolean getSaveCard() {
			return saveCard;
		}
		
		public void setSaveCard(Boolean saveCard) {
			this.saveCard = saveCard;
		}
		
		public Integer getQualifyingMilesQuantity() {
			return qualifyingMilesQuantity;
		}
		
		public void setQualifyingMilesQuantity(Integer qualifyingMilesQuantity) {
			this.qualifyingMilesQuantity = qualifyingMilesQuantity;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getSubType() {
			return subType;
		}

		public void setSubType(String subType) {
			this.subType = subType;
		}

		public Double getFullCostMoney() {
			return fullCostMoney;
		}

		public void setFullCostMoney(Double fullCostMoney) {
			this.fullCostMoney = fullCostMoney;
		}

		public Integer getValidity() {
			return validity;
		}

		public void setValidity(Integer validity) {
			this.validity = validity;
		}

		public Integer getGracePeriod() {
			return gracePeriod;
		}

		public void setGracePeriod(Integer gracePeriod) {
			this.gracePeriod = gracePeriod;
		}

		public String getInstallmentOptions() {
			return installmentOptions;
		}

		public void setInstallmentOptions(String installmentOptions) {
			this.installmentOptions = installmentOptions;
		}

		public Boolean getAutomaticRenewal() {
			return automaticRenewal;
		}

		public void setAutomaticRenewal(Boolean automaticRenewal) {
			this.automaticRenewal = automaticRenewal;
		}

		public Boolean getIsFraud() {
			return isFraud;
		}

		public void setIsFraud(Boolean isFraud) {
			this.isFraud = isFraud;
		}
		
		public Boolean getRevertMiles() {
			return revertMiles;
		}

		public void setRevertMiles(Boolean revertMiles) {
			this.revertMiles = revertMiles;
		}

		public Boolean getImmediateCancellation() {
			return immediateCancellation;
		}

		public void setImmediateCancellation(Boolean immediateCancellation) {
			this.immediateCancellation = immediateCancellation;
		}

		public List<SiebelTransaction> getSiebelTransaction() {
	        return siebelTransaction;
	    }

	    public void setSiebelTransaction(List<SiebelTransaction> siebelTransaction) {
	        this.siebelTransaction = siebelTransaction;
	    }

		public Double getRefundMoney() {
			return refundMoney;
		}

		public void setRefundMoney(Double refundMoney) {
			this.refundMoney = refundMoney;
		}
		
}
