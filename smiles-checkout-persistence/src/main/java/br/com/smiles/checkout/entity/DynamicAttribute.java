package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.util.FlightDateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

/**
 * Created by adautomartins on 12/09/16.
 */
@Embeddable
@Cacheable(false)
@XmlRootElement
public class DynamicAttribute {
    @Basic
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    private Boolean isRequired;

    @Basic
    private Boolean isDisplayed;

    public Boolean getRequired() {
        return isRequired;
    }

    public void setRequired(Boolean required) {
        isRequired = required;
    }

    public Boolean getDisplayed() {
        return isDisplayed;
    }

    public void setDisplayed(Boolean displayed) {
        isDisplayed = displayed;
    }
}
