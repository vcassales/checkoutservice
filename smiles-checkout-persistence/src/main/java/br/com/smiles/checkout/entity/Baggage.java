package br.com.smiles.checkout.entity;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

@Entity
@Cacheable(false)
@XmlRootElement
public class Baggage {
	
    @Id
    @SequenceGenerator( name = "BAGGAGE_ID", sequenceName = "BAGGAGE_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "BAGGAGE_ID" )
    private Long id;
    private String passengerId;
    private String code;
    private Integer quantity;
    private Double amount;
    private Double weight;
    private Double maxWeight;
    private String keyValue;
    private Integer costMiles;
    private Double costMoney;
            
    @ManyToOne
    @XmlInverseReference(mappedBy = "baggages")
    @XmlElement
    private ChosenFlightBaggage chosenFlightBaggage;
    
    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "baggage")
    @XmlInverseReference(mappedBy = "baggage")
    @XmlElement
    private List<SiebelTransaction> siebelTransaction;
    
    private String firstName;
    private String lastName;
    
    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "baggage")
    @XmlInverseReference(mappedBy = "baggage")
    @XmlElement
    private List<BaggageUnit> baggageUnit;
    

    private Integer cartQuantity;
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassengerId() {
		return passengerId;
	}

	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(Double maxWeight) {
		this.maxWeight = maxWeight;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public Integer getCostMiles() {
		return costMiles;
	}

	public void setCostMiles(Integer costMiles) {
		this.costMiles = costMiles;
	}

	public Double getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(Double costMoney) {
		this.costMoney = costMoney;
	}

	public ChosenFlightBaggage getChosenFlightBaggage() {
		return chosenFlightBaggage;
	}

	public void setChosenFlightBaggage(ChosenFlightBaggage chosenFlightBaggage) {
		this.chosenFlightBaggage = chosenFlightBaggage;
	}

	public List<SiebelTransaction> getSiebelTransaction() {
		return siebelTransaction;
	}

	public void setSiebelTransaction(List<SiebelTransaction> siebelTransaction) {
		this.siebelTransaction = siebelTransaction;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<BaggageUnit> getBaggageUnit() {
		return baggageUnit;
	}

	public void setBaggageUnit(List<BaggageUnit> baggageUnit) {
		this.baggageUnit = baggageUnit;
	}

	public Integer getCartQuantity() {
		return cartQuantity;
	}

	public void setCartQuantity(Integer cartQuantity) {
		this.cartQuantity = cartQuantity;
	}
	
}
