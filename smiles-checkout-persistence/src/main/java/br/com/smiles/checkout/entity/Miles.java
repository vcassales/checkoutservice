package br.com.smiles.checkout.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.CascadeType.PERSIST;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Entity implementation class for Entity: Miles
 *
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Miles extends Item<Double> implements Serializable {

	private Integer quantity;
	private String optionId;
	private String convenienceFee;
	private String tax;
	private Double unitCost;
	@Transient
	private Double subTotalCost;
	private Double discount;
	@Temporal(value = TemporalType.DATE)
	@XmlSchemaType(name = "date")
	private Date expiryDate;
	private String operation;
	@JoinFetch(JoinFetchType.OUTER)
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "miles")
	@XmlInverseReference(mappedBy = "miles")
	@XmlElement
	private List<SiebelTransaction> siebelTransaction;
	@JoinFetch(JoinFetchType.OUTER)
	@OneToOne(cascade = { PERSIST, MERGE, REMOVE }, mappedBy = "miles")
	@XmlInverseReference(mappedBy = "miles")
	@XmlElement
	private Receiver receiver;
	@JoinFetch(JoinFetchType.OUTER)
	@OneToOne(cascade = { PERSIST, MERGE, REMOVE }, mappedBy = "miles")
	@XmlInverseReference(mappedBy = "miles")
	@XmlElement
	private Event event;
	private Double smilesClubMilesPrice;
	private Integer smilesClubMilesBonus;
	private Integer milesBonus;

	private static final long serialVersionUID = 1L;

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getOptionId() {
		return optionId;
	}

	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}

	public String getConvenienceFee() {
		return convenienceFee;
	}

	public void setConvenienceFee(String convenienceFee) {
		this.convenienceFee = convenienceFee;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public Double getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public List<SiebelTransaction> getSiebelTransaction() {
		return siebelTransaction;
	}

	public void setSiebelTransaction(List<SiebelTransaction> siebelTransaction) {
		this.siebelTransaction = siebelTransaction;
	}

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Double getSubTotalCost() {
		return subTotalCost;
	}

	public void setSubTotalCost(Double subTotalCost) {
		this.subTotalCost = subTotalCost;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	
	public Double getSmilesClubMilesPrice() {
		return smilesClubMilesPrice;
	}

	public void setSmilesClubMilesPrice(Double smilesClubMilesPrice) {
		this.smilesClubMilesPrice = smilesClubMilesPrice;
	}
	
	public Integer getSmilesClubMilesBonus() {
		return smilesClubMilesBonus;
	}

	public void setSmilesClubMilesBonus(Integer smilesClubMilesBonus) {
		this.smilesClubMilesBonus = smilesClubMilesBonus;
	}

	public Integer getMilesBonus() {
		return milesBonus;
	}

	public void setMilesBonus(Integer milesBonus) {
		this.milesBonus = milesBonus;
	}
}
