package br.com.smiles.checkout.entity;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Table;

/**
 * Created by thiagopremoli on 22/04/18.
 */
@Embeddable
public class InsuranceCoverage {
	@Basic
	private Integer code;
	
	@Basic
	private String description;
	
	@Basic
	private Double capital;
	
	@Basic
	private Double premium;

	
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getCapital() {
		return capital;
	}

	public void setCapital(Double capital) {
		this.capital = capital;
	}

	public Double getPremium() {
		return premium;
	}

	public void setPremium(Double premium) {
		this.premium = premium;
	}   
    
}
