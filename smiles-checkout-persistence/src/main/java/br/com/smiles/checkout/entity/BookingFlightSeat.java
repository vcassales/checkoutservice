package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.domain.Total;
import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

/**
 * Created by adautomartins on 15/08/16.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class BookingFlightSeat extends Item<Total> implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5983529744331980335L;


	@JoinFetch(JoinFetchType.INNER)
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "bookingFlightSeat")
    @XmlInverseReference(mappedBy = "bookingFlightSeat")
    @XmlElement
    private List<ChosenFlightSeat> chosenFlightSeats;
	

    public List<ChosenFlightSeat> getChosenFlightSeats() {
		return chosenFlightSeats;
	}

	public void setChosenFlightSeats(List<ChosenFlightSeat> chosenFlightSeats) {
		this.chosenFlightSeats = chosenFlightSeats;
	}

	@Basic
    private String tokenGds;

    public String getTokenGds() {
        return tokenGds;
    }

    public void setTokenGds(String tokenGds) {
        this.tokenGds = tokenGds;
    }

}
