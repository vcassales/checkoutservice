package br.com.smiles.checkout.entity;

import br.com.smiles.checkout.util.FlightDateAdapter;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

import java.util.Date;
import java.util.List;

/**
 * Created by thiagopremoli on 12/09/16.
 */
@Entity
@Cacheable(false)
@XmlRootElement
public class Travel {
	
 	@Id
    @SequenceGenerator( name = "TRAVEL_ID", sequenceName = "TRAVEL_SEQ", allocationSize = 1 )
    @GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "TRAVEL_ID" )
    private Long id;
	
    @Basic
    private String recordLocator;

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    private Date initialDate;

    @XmlJavaTypeAdapter(value= FlightDateAdapter.class, type=Date.class)
    private Date endDate;

    @Basic
    private String origin;

    @Basic
    private String destination;
    
    @ManyToOne
	@JoinFetch(JoinFetchType.OUTER)
	@XmlInverseReference(mappedBy = "travel")
	@XmlElement
	private Product product;
    
    @Basic
    private String tripType;
    
    @Basic
    private String sourceGds;
    
    @Basic
    private String marketingAirlineCode;
    
    @Basic
    private String operationAirlineCode;
    
    @Basic
    private Integer flightNumber;
    
    @Basic
    private String originAirportCode;

    @Basic
    private String destinationAirportCode;
    
    @JoinFetch(JoinFetchType.OUTER)
    @OneToMany(cascade = { PERSIST, MERGE }, mappedBy = "travel")
    @XmlInverseReference(mappedBy = "travel")
    @XmlElement
    private List<FlightPassenger> flightPassenger;
    
    
	
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getRecordLocator() {
		return recordLocator;
	}

	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public List<FlightPassenger> getFlightPassenger() {
		return flightPassenger;
	}

	public void setFlightPassenger(List<FlightPassenger> flightPassenger) {
		this.flightPassenger = flightPassenger;
	}

	public String getSourceGds() {
		return sourceGds;
	}

	public void setSourceGds(String sourceGds) {
		this.sourceGds = sourceGds;
	}

	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}

	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}

	public String getOperationAirlineCode() {
		return operationAirlineCode;
	}

	public void setOperationAirlineCode(String operationAirlineCode) {
		this.operationAirlineCode = operationAirlineCode;
	}

	public Integer getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(Integer flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getOriginAirportCode() {
		return originAirportCode;
	}

	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}
    
}
