#!groovy

 /**
 * Date:  04/04/2017
 * Adicionado path do artefato na notificacao do slack
 * Date:  11/07/2017
 * deletebranch, correcao numero da versao, sonar, junit
 */

 import net.sf.json.JSONArray;
 import net.sf.json.JSONObject;

pipeline {
    agent any
    tools {
        maven 'M3'
        //jdk 'Jdk1.7u80'
        jdk 'Jdk1.8u121'
    }
    environment {
        SONAR_URL = 'http://devops-sonar.smiles.local.br/'
        NEXUS_URL = 'http://devops-nexus.smiles.local.br/'

        NEXUS_URL_SNAP = 'http://devops-nexus.smiles.local.br/repository/smiles-snapshot/'
        NEXUS_ID_SNAP = 'smiles-snapshot'

        NEXUS_URL_CI = 'http://devops-nexus.smiles.local.br/repository/smiles-ci/'
        NEXUS_ID_CI = 'smiles-ci'

        NEXUS_URL_RELEASE = 'http://devops-nexus.smiles.local.br/repository/smiles-release/'
        NEXUS_ID_RELEASE = 'smiles-release'

        // For teste purpose only (need to be set always to TRUE! )
        RUN_TEST = true
        RUN_PRE_BUILD = true
        RUN_SITE = false
        RUN_SONAR = true
        RUN_POST_BUILD = true
        RUN_COMPILE = true
        RUN_CHECKS = true
        RUN_BUILD_CI = true

    }
    options {
        //skipDefaultCheckout()
        // Only keep the 50 most recent builds
        buildDiscarder(logRotator(numToKeepStr: '30'))
        timeout(time: 5, unit: 'MINUTES')
       // disableConcurrentBuilds()
    }


    stages {

        stage('Check commit message') {
          steps {
            script {
              current_commit_message = sh(script: '''
                git rev-list --format=%B --max-count=1 HEAD |head -2 |tail -1
              ''', returnStdout: true).trim()

                    if (current_commit_message != 'Prepare for next Release') {
                        if (BRANCH_NAME.startsWith("master")){
                            delete_branch()
                        }
                    }else{
                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'rundeck-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                  script {
                    sh 'curl -s -c rundeck.cookie "http://devops-rundeck.smiles.local.br/j_security_check?j_username=${USERNAME}&j_password=${PASSWORD}"'
                    env['job_id'] = sh(script: '''
                      curl -s -b rundeck.cookie "http://devops-rundeck.smiles.local.br/api/19/project/git/jobs?jobExactFilter=mergeback" |grep '<job ' |sed -r 's,.*id="([^"]*).*,\\1,' |tr -d '\n'
                    ''', returnStdout: true)
                    env['repo'] = sh(script: '''
                      git remote show -n origin | grep Fetch | sed -r 's,.*:(.*).git,\\1,' |tr -d '\n'
                    ''', returnStdout: true)
                    sh 'curl -s -X POST -b rundeck.cookie "http://devops-rundeck.smiles.local.br/api/19/job/${job_id}/run?option.repo=${repo}"'
                  }
                }
                currentBuild.result = 'ABORTED'
                error('Parando build por ser um commit de CI.')
              }
            }

          }
        }

        stage('Notify') {
            steps {
                echo sh(returnStdout: true, script: 'env')
                notifyBuild('STARTED')
            }
        }

        stage('Pre-Build CheckList') {
            when {
                environment name: 'RUN_CHECKS', value: 'true'
            }
            steps {
                parallel(
                        //"Check Comments": {
                        //    checkComments()
                        //},
                        "Commit Behind": {
                            checkCommitBehind()
                        }
                        //"check tag for PR": {
                        //    checkQATagforPR()
                        //}

                )
            }
        }

        stage('Compile') {
            when {
                environment name: 'RUN_COMPILE', value: 'true'
            }
            steps {
                sh 'mvn --batch-mode -B -V -U -e -Dmaven.test.failure.ignore=true  clean compile'
            }
        }

        stage('Test') {
            when {
                environment name: 'RUN_TEST', value: 'true'
            }
            steps {
                sh 'mvn -B -V -U -e --batch-mode -Dmaven.test.failure.ignore=true  org.jacoco:jacoco-maven-plugin:prepare-agent test'
            }
            post {
                always {
                    echo "Publicando JUnit"
                    junit allowEmptyResults: true, testResults: 'target/surefire-reports/**/*.xml'
                }
                failure {
                    notifyBuild('FAILED')
                }
            }
        }

        stage('Site') {
            when {
                environment name: 'RUN_SITE', value: 'true'
            }
            steps {
                sh 'mvn -B -V -U -e --batch-mode site'
            }
        }

        stage('Pre-Build') {
            when {
                environment name: 'RUN_PRE_BUILD', value: 'true'
            }
            steps {
                //echo sh("echo ${env.BRANCH_NAME}")


                script {
                    //def branch_name = env['BRANCH_NAME']
                    def pom = readMavenPom file: 'pom.xml'
                    env['pom'] = pom
                    env['pom_version'] = pom.version

                    /**** Adicionado msg slack ****/
                    env['pom_groupId'] = pom.groupId
                    if (pom.groupId == null){
                          env['pom_groupId'] = pom.parent.groupId
                    }
                    env['pom_groupId'] = env['pom_groupId'].replace('.','/')
                    env['pom_artifactId'] = pom.artifactId
                    env['pom_packaging'] = pom.packaging
                    /**** Adicionado msg slack ****/

                    env['RUN_BUILD_BRANCH'] = false
                    env['RUN_BUILD_MASTER'] = false

                    if (BRANCH_NAME.startsWith("master")) {
                        echo "***** PERFORMING STEPS ON MASTER *****"
                        env['pom_new_version'] = pom.version.replace("-SNAPSHOT", "")
                        env['RUN_BUILD_MASTER'] = true

                    } else if (BRANCH_NAME.startsWith("release")) {
                        echo "***** PERFORMING STEPS ON RELEASE BRANCH *****"
                        bump_git_tag()
                        env['pom_new_version'] = pom.version.replace("-SNAPSHOT", "-${bumpci_tag}")
                        env['RUN_BUILD_BRANCH'] = true

                    } else if (BRANCH_NAME.startsWith("hotfix")) {
                        echo "***** PERFORMING STEPS ON HOTFIX BRANCH *****"
                        bump_git_tag()
                        env['pom_new_version'] = pom.version.replace("-SNAPSHOT", "-${bumpci_tag}")
                        env['RUN_BUILD_BRANCH'] = true

                    } else {
                        echo "***** BRANCHES MUST START WITH RELEASE OR HOTFIX *****"
                        echo "***** STOPPED BUILD *****"
                        currentBuild.result = 'FAILURE'
                        System.exit(-1)
                    }
                }

                sh 'echo "***** FINISHED PRE-BUILD STEP *****"'
            }
        }


        stage('Sonar publish') {
            when {
                environment name: 'RUN_SONAR', value: 'true'
            }
            steps {
                withEnv(["JAVA_HOME=${tool 'Jdk1.8u121'}", "PATH+MAVEN=${tool 'M3'}/bin:${env.JAVA_HOME}/bin"]) {
                    sh "mvn -B -V -U -e --batch-mode sonar:sonar -Dsonar.host.url='${SONAR_URL}' -Dsonar.branch='${env.BRANCH_NAME}' "
                }
            }
        }


        stage('Build-Snapshot') {
            when {
                environment name: 'RUN_BUILD_BRANCH', value: 'true'
            }
            steps {
                sh 'mvn --batch-mode -B -V -U -e -Dmaven.test.skip=true -Dsurefire.useFile=false deploy -DaltDeploymentRepository=${NEXUS_ID_SNAP}::default::${NEXUS_URL_SNAP}'
            }
            //post {
            //    success {
                    // Jenkins in build artifact registry
            //        archive(includes: 'target/*.jar')
            //        archive(includes: 'target/*.war')
             //      archive(includes: 'target/*.ear')
             //   }
           // }
        }


        stage('Build-Branch') {
            when {
                environment name: 'RUN_BUILD_BRANCH', value: 'true'
            }
            steps {
                sh "mvn --batch-mode -B -V -U -e -DgenerateBackupPoms=false versions:set -Dmaven.test.skip=true -DnewVersion=${env.pom_new_version} -DaltDeploymentRepository=${NEXUS_ID_CI}::default::${NEXUS_URL_CI} "
                sh "mvn --batch-mode -B -V -U -e -Dsurefire.useFile=false clean deploy -Dmaven.test.skip=true -DaltDeploymentRepository=${NEXUS_ID_CI}::default::${NEXUS_URL_CI}"
                // FIXME  javadoc:jar source:jar
                 script {
                   env['NEXUS_URL_NOTIFY'] = env['NEXUS_URL_CI']
                    }

            }
            //post {
            //    success {
                    // Jenkins in build artifact registry
            //        archive(includes: 'target/*.jar')
            //        archive(includes: 'target/*.war')
            //        archive(includes: 'target/*.ear')
            //    }
            //}
        }


       stage('Build-Master') {
            when {
                environment name: 'RUN_BUILD_MASTER', value: 'true'
            }
            steps {
                sh 'echo "***** MASTER STEP (BUMP VERSION) *****"'

                //Ajusta a versão para release
                sh "mvn --batch-mode -B -V -U -e -DgenerateBackupPoms=false versions:set -Dmaven.test.skip=true -DnewVersion=${env.pom_new_version}"
                sh "mvn --batch-mode -B -V -U -e -Dsurefire.useFile=false clean  deploy -Dmaven.test.skip=true -DaltDeploymentRepository=${NEXUS_ID_RELEASE}::default::${NEXUS_URL_RELEASE}"
                //FIXME javadoc:jar source:jar
                script {
                    env['NEXUS_URL_NOTIFY'] = env['NEXUS_URL_RELEASE']

                    sh 'git checkout master'
                    sh 'git commit -m "Version Bump" .'
                    //sh 'git checkout -- pom.xml'
                    def curr_version = env['pom_new_version']

                    def SNAPSHOT_PART = "-SNAPSHOT";
                    def versionParts = curr_version.tokenize('.');
                    def mayorVersionPart = versionParts[0];
                    def minorVersionPart = versionParts[1];
                    def fixVersionPart = versionParts[2];



                    int minorVersion = minorVersionPart.toInteger();
                    int fixVersion = fixVersionPart.toInteger();


                    def newFixVersion = fixVersion + 1
                    def developmentVersion = mayorVersionPart  + "." + minorVersion + "." + newFixVersion + SNAPSHOT_PART;
                    env['new_development_version'] = developmentVersion
                }

                //BUMP DA VERSAO
                sh "mvn versions:set --batch-mode -B -V -U -e  -Dmaven.test.skip=true -DgenerateBackupPoms=false -DnewVersion=${env.new_development_version} -DupdateMatchingVersions=true"

                script {
                  sh "git commit -m 'Prepare for next Release' ."
                  sh "git push"
                }
            }
            post {
                success {
                    // Jenkins in build artifact registry
                    archive(includes: 'target/*.jar')
                    archive(includes: 'target/*.war')
                    archive(includes: 'target/*.ear')
                }
            }

        }



        stage('HTML Reports') {
            when {
                environment name: 'RUN_SITE', value: 'true'
            }
            steps {
                publishHTML(
                        target:
                                [allowMissing         : false,
                                 alwaysLinkToLastBuild: true,
                                 keepAll              : true,
                                 reportDir            : 'target/site',
                                 reportFiles          : 'index.html',
                                 reportName           : 'Project Site'
                                ])
            }

        }

	stage ('Determine if we should deploy') {
          steps {
            script {
              env['CURRENT_COMMIT_FULL_MESSAGE'] = sh(script: '''
                git rev-list --format=%B --max-count=1 HEAD
              ''', returnStdout: true).trim()

              env['should_deploy'] = sh(script: '''
                echo ${CURRENT_COMMIT_FULL_MESSAGE} |grep '#deploy'
              ''', returnStatus: true)

              env['deploy_env'] = sh(script: '''
                echo ${CURRENT_COMMIT_FULL_MESSAGE} |grep '#deploy' |sed -r 's,.*#deploy ([^ ]*),\\1,' |tr -d '\n'
              ''', returnStdout: true)
            }
          }
        }
        stage ('Deploy to UAT') {
          when {
            environment name: 'should_deploy', value: '0'
          }
          steps {
            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'rundeck-credentials',
  usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
              script {
                sh 'curl -s -c rundeck.cookie "http://devops-rundeck.smiles.local.br/j_security_check?j_username=${USERNAME}&j_password=${PASSWORD}"'
                env['job_id'] = sh(script: '''
                  curl -s -b rundeck.cookie "http://devops-rundeck.smiles.local.br/api/19/project/${deploy_env}-Deploy/jobs?jobFilter=java-deploy" |grep '<job ' |sed -r 's,.*id="([^"]*).*,\\1,' |tr -d '\n'
                ''', returnStdout: true)
                env['NEXUS_ARTIFACT_URL'] = "${env.NEXUS_URL_NOTIFY}${env.pom_groupId}/${env.pom_artifactId}/${env.pom_new_version}/${env.pom_artifactId}-${env.pom_new_version}.${env.pom_packaging}"
                sh 'curl -s -X POST -b rundeck.cookie "http://devops-rundeck.smiles.local.br/api/19/job/${job_id}/run?option.allURLs=${NEXUS_ARTIFACT_URL}"'
              }

            }
          }
        }

        stage('Post-Build Actions') {
            when {
                environment name: 'RUN_POST_BUILD', value: 'true'
            }
            steps {
                parallel(
                        //"Nexus Deploy": {
                        //sh 'mvn deploy -DaltDeploymentRepository=${NEXUS_ID_SNAP}::default::${NEXUS_URL_SNAP} -Dmaven.test.skip=true'
                        //},
                        "Get Sonar Status publish": {
                            sh 'echo "Not implemented YET"'
                            // CHECK http://stackoverflow.com/questions/42909439/using-waitforqualitygate-in-a-jenkins-declarative-pipeline
                        }
                )
            }
        }
    }
    post {
        success {
            notifyBuild('SUCCESSFUL')
        }
        failure {
            notifyBuild('FAILED')
        }
        always {
         deleteDir() // Must delete after build, random errors occurs reusing workspace
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus = buildStatus ?: 'SUCCESSFUL'

    // Default values
    def colorCode = '#FF0000'
    String subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
    String summary = "${subject} \n (${env.BUILD_URL})  "
    String details = """<p>${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

    JSONArray attachments = new JSONArray();
    JSONObject attachment = new JSONObject();

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        colorCode = '#FFFF00'
        attachment.put('text','Começando o build. Será que vai dar certo?')
        attachment.put('thumb_url','http://icons.iconarchive.com/icons/jonathan-rey/simpsons/72/Homer-Simpson-03-Beer-icon.png')
    } else if (buildStatus == 'SUCCESSFUL') {
        colorCode = '#00FF00'
        attachment.put('text','Build com sucesso. Parabéns!')
        attachment.put('thumb_url','http://icons.iconarchive.com/icons/jonathan-rey/simpsons/72/Homer-Simpson-04-Happy-icon.png')
        String artifactUrl = "${env.NEXUS_URL_NOTIFY}${env.pom_groupId}/${env.pom_artifactId}/${env.pom_new_version}/${env.pom_artifactId}-${env.pom_new_version}.${env.pom_packaging}"

        JSONArray fields = new JSONArray();
        JSONObject field = new JSONObject();
        field.put('title','URL Nexus');
        field.put('value',artifactUrl);
        fields.add(field);
        attachment.put('fields',fields);

        JSONArray actions = new JSONArray();
        for (i = 1; i <= 6; i++) {
          JSONObject action = new JSONObject();
          action.put('name','deploy');
          String envCode = i==0?'UAT':'UAT'+i;
          action.put('text',envCode);
          action.put('type','button');
          action.put('value',envCode);
          actions.add(action);
        }
        attachment.put('actions',actions);
    } else {
        attachment.put('text','Não foi dessa vez. Talvez você não mereça terminar o seu trabalho!')
        attachment.put('thumb_url','http://imagenes.montevideo.com.uy/imgnoticias/201509/_W420/525104.gif')
        colorCode = '#FF0000'
    }

    String buildUrl = "${env.BUILD_URL}";
    attachment.put('title', subject);
    attachment.put('callback_id', buildUrl);
    attachment.put('title_link', buildUrl);
    attachment.put('fallback', subject);
    attachment.put('color', colorCode);


    attachments.add(attachment);

    // Send notifications
    echo attachments.toString();
    slackSend(attachments: attachments.toString())

    emailext(
            subject: subject,
            body: details,
            recipientProviders: [[$class: 'DevelopersRecipientProvider']]
    )
}

def checkComments() {
    sh 'echo "Verifica se branch está com comentário no codigo."'
    script {
        def checkCommentsStatus = sh(script: '''set +x; set +e;
                                                git --no-pager log -1 --oneline 2>&1 |egrep "[A-Z]{2,}-[0-9]+";
                                                if [ ${?} -eq 0 ];
                                                then
                                                  echo Comentários OK;
                                                else
                                                  echo Comentários não referenciam nenhum Jira!;
                                                  exit 1;
                                                fi''',
                returnStatus: true);
        if (checkCommentsStatus != 0) {
            currentBuild.result = 'UNSTABLE'
        }
    }

}

def checkCommitBehind() {
    sh 'echo "Verifica se branch necessita de merge com master."'
    script {
        sh(script: '''set +x; set +e;
                      git fetch;
                      commitsBehind=$(git rev-list --left-right --count origin/master... |awk '{print $1}');
                      if [ ${commitsBehind} -ne 0 ]
                      then
                        echo "Esta branch está ${commitsBehind} commits atrás da master!"
                        exit 1
                      else
                        echo "Esta branch não tem commits atrás da master."
                      fi''')
    }

}

def checkQATagforPR() {
    sh 'echo "Verifica se o PR está com a TAG de QA."'

    script {
        sh(script: '''set +x; set +e;
                    git tag -l --points-at HEAD |grep UAT''')
    }
}


def bump_git_tag() {
    echo "Bumping Git CI Tag"

    script {
        sh "git fetch --tags"
        env['bumpci_tag'] = sh(script: '''
      current_tag=$(git tag -n9 -l |grep bumpci |awk '{print $1}' |sort -V |tail -1)
      if [[ $current_tag == '' ]]
      then
        echo 1 |tr -d '\n'
      else
        echo "${current_tag} + 1" |bc |tr -d '\n'
      fi
      ''', returnStdout: true)
        echo "${bumpci_tag}"
        sh "git tag -a ${bumpci_tag} -m bumpci && git push origin refs/tags/${bumpci_tag}"
    }
}

def delete_branch(){
              script{
                 def REPO_NAME = sh(script: '''
                                            git remote show -n origin | grep Fetch | sed -r 's,.*:(.*).git,\\1,' |tr -d '\n'
                                            ''', returnStdout: true).trim()

                 def URL_PULL_REQUESTS = "https://api.github.com/repos/${REPO_NAME}/pulls?state=closed"

                 def current_commit = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
                 def response = httpRequest authentication: 'github-user', httpMode: 'GET', url: "${URL_PULL_REQUESTS}"
                 def response_payload = readJSON text: response.content
                 for (pull_request_list in response_payload ){
                   if(current_commit == pull_request_list['merge_commit_sha']){
                     env['pr_branch_name'] = pull_request_list['head']['ref']
                     break
                   }
                   break
                 }

                 if(env['pr_branch_name']!=null && env['pr_branch_name'] != 'master'){
                   def get_branch = sh(script: '''
                                                git ls-remote --heads origin ${pr_branch_name} | grep -c refs/heads/${pr_branch_name}
                                              ''', returnStdout: true).trim()
                   echo "return get branch is ${get_branch}"
                   if(get_branch != "0"){
                     echo "deleting branch ${env.pr_branch_name}"
                     sh(script: '''
                                  git fetch --all
                                  git push origin :${pr_branch_name}
                                ''', returnStdout: false)
                   }
                 }
              }
          }
