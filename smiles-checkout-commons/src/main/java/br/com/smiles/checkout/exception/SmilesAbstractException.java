package br.com.smiles.checkout.exception;

/**
 * Checked abstract exception
 * 
 * Deve ser utilizada para lancamento de exceções de negócio e exeções que necessitam de verificação
 * 
 */
public abstract class SmilesAbstractException extends Exception {

	private String code;

	private static final long serialVersionUID = 1L;
	
	public SmilesAbstractException(String code, String message) {
		super(message);
		this.code = code;
	}

	public SmilesAbstractException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmilesAbstractException(Throwable cause) {
		super(cause);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
