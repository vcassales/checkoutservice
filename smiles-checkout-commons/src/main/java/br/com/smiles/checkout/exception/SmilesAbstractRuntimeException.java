package br.com.smiles.checkout.exception;

/**
 * Non checked abstract exception
 * 
 * Deve ser utilizada para lancamento de exceções necessitam de verificação,
 * como exemplo exceções de falha na carga do arquivo de configurações
 * 
 */
public abstract class SmilesAbstractRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String code;

	public SmilesAbstractRuntimeException() {
		super();
	}

	public SmilesAbstractRuntimeException(String code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}

	public SmilesAbstractRuntimeException(Throwable cause) {
		super(cause);
	}
	
	public SmilesAbstractRuntimeException(String code, String message) {
		super(message);
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
