package br.com.smiles.checkout.exception;

public class InvalidItemMessageException extends SmilesAbstractException {

	private static final long serialVersionUID = 1L;

	public InvalidItemMessageException(String code, String exception){
		super(code, exception);
	}
}
