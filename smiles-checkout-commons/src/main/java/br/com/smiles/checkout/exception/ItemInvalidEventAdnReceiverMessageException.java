package br.com.smiles.checkout.exception;

public class ItemInvalidEventAdnReceiverMessageException extends SmilesAbstractException {

	private static final long serialVersionUID = 1L;

	public ItemInvalidEventAdnReceiverMessageException(String code, String exception){
		super(code, exception);
	}
}
