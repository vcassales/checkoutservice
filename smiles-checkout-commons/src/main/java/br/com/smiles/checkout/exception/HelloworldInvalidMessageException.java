package br.com.smiles.checkout.exception;

/**
 * Exceção de negócio de validação da mensagem de hello world
 *
 */
public class HelloworldInvalidMessageException extends SmilesAbstractException {

	private static final long serialVersionUID = 1L;

	public HelloworldInvalidMessageException(String code, String exception){
		super(code, exception);
	}

}
