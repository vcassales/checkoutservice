package br.com.smiles.checkout.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProperties {

	private static final String PREFIX_KEY = "smiles-checkout";

	private static final String RUNTIME_PROPERTIES_KEY = "smiles.runtime.properties.file";

	private static final Properties properties;

	static {
		properties = new Properties();
		// Load application properties
		loadProperties(properties, PREFIX_KEY + ".properties");

		// Load runtime properties if necessary
		String runtimePropsLocation = System.getenv(RUNTIME_PROPERTIES_KEY);
		if (runtimePropsLocation != null && !runtimePropsLocation.isEmpty()) {
			loadProperties(properties, runtimePropsLocation);
		}
	}

	private static void loadProperties(Properties props, String file) {
		try {
			InputStream resource = ConfigProperties.class.getClassLoader()
					.getResourceAsStream(file);
			if (resource == null) {
				throw new RuntimeException(String.format(
						"Can not load properties from  %s",
						file));
			}
			try {
				props.load(resource);
			} finally {
				if (resource != null) {
					resource.close();
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(String.format(
					"Can not load properties from  %s", file),
					e);
		}
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
}
