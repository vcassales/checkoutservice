package br.com.smiles.checkout.exception;

public class CheckoutOrderInvalidStatusMessageException extends SmilesAbstractException {

	private static final long serialVersionUID = 1L;

	public CheckoutOrderInvalidStatusMessageException(String code, String exception){
		super(code, exception);
	}
}
