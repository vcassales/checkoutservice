package br.com.smiles.checkout.exception;

/**
 * Exceção que caracteriza um problema na carga do arquivo de configuração
 *
 */
public class SmilesConfigurationException extends SmilesAbstractRuntimeException {

	private static final long serialVersionUID = 1L;
	
	public SmilesConfigurationException(String code, String message, Throwable cause) {
		super(code, message, cause);
	}

	public SmilesConfigurationException(Throwable cause) {
		super(cause);
	}
	
	public SmilesConfigurationException(String code, String message) {
		super(code, message);
	}

}
