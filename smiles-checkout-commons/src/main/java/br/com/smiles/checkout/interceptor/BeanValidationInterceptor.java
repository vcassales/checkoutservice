package br.com.smiles.checkout.interceptor;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class BeanValidationInterceptor {

	@AroundInvoke
	public Object intercept(InvocationContext ctx) throws Exception {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		
		Annotation params[][] = ctx.getMethod().getParameterAnnotations();
		int index = 0;
		for (Annotation[] annotations : params) {
			for (Annotation param : annotations) {
				if (param.annotationType().isAssignableFrom(Valid.class)) {
					Set<ConstraintViolation<Object>> violations = validator.validate(ctx.getParameters()[index]);
					if (violations.size() > 0) {
						throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(violations));
					}
				}
			}
			index++;
		}
		
		return ctx.proceed();
	}
}
