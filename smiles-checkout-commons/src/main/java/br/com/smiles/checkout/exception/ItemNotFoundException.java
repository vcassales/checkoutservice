package br.com.smiles.checkout.exception;

public class ItemNotFoundException extends SmilesAbstractException {

	private static final long serialVersionUID = 1L;

	public ItemNotFoundException(String code, String exception){
		super(code, exception);
	}

}
