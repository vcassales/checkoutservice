package br.com.smiles.checkout.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by adautomartins on 13/09/16.
 */
public class FlightDateAdapter extends XmlAdapter<String, Date> {
    // the desired format
    private String pattern = "yyyy-MM-dd'T'HH:mm:ss";

    public String marshal(Date date) throws Exception {
        return new SimpleDateFormat(pattern).format(date);
    }

    public Date unmarshal(String dateString) throws Exception {
        return new SimpleDateFormat(pattern).parse(dateString);
    }
}
