package br.com.smiles.checkout.exception;

public class ItemParametersInvalid extends SmilesAbstractException{
	
	private static final long serialVersionUID = 1L;

	public ItemParametersInvalid(String code, String exception){
		super(code, exception);
	}

}
