package br.com.smiles.checkout.exception;

public class ItemInvalidStatusMessageException extends SmilesAbstractException {

	private static final long serialVersionUID = 1L;

	public ItemInvalidStatusMessageException(String code, String exception){
		super(code, exception);
	}
}
