---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------PASSENGERDOCUMENTS---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


ALTER TABLE "CHECKOUT_OW"."FLIGHTPASSENGER"
ADD "MEMBERNUMBER" VARCHAR2(255);



CREATE TABLE "CHECKOUT_OW"."DOCUMENT"
   (	"ID" NUMBER(19,0) NOT NULL ENABLE,
      "DOCUMENTNUMBER" VARCHAR2(255) NOT NULL ENABLE,
      "DOCUMENTTYPE" VARCHAR2(255) NOT NULL ENABLE,
      "ISSUERORGANIZATION" VARCHAR2(255),
      "ISSUERSTATE" VARCHAR2(255),
      "EXPEDITIONDATE" DATE,
      "VALIDITYDATE" DATE,
      "FLIGHTPASSENGER_ID" NUMBER(19,0),
	 PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBS_CHECKOUT_OW"  ENABLE, 
	 CONSTRAINT "DOCUMENTFLGHTPSSENGERID" FOREIGN KEY ("FLIGHTPASSENGER_ID")
	  REFERENCES "CHECKOUT_OW"."FLIGHTPASSENGER" ("ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBS_CHECKOUT_OW";




CREATE SEQUENCE  "CHECKOUT_OW"."DOCUMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOPARTITION;



CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."DOCUMENT" FOR "CHECKOUT_OW"."DOCUMENT";
CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."DOCUMENT_SEQ" FOR "CHECKOUT_OW"."DOCUMENT_SEQ";



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------BOOKINGFLIGHTBAGGAGE---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------
--  DDL for Table BOOKINGFLIGHTBAGGAGE
--------------------------------------------------------
    CREATE TABLE CHECKOUT_OW."BOOKINGFLIGHTBAGGAGE" 
   (  "ID" NUMBER(19,0), 
      "TOKENGDS" VARCHAR2(4000 BYTE)
   ) ;

--------------------------------------------------------
--  DDL for Index SYS_C009453
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C009453" ON CHECKOUT_OW."BOOKINGFLIGHTBAGGAGE" ("ID");
--------------------------------------------------------
--  Constraints for Table BOOKINGFLIGHTBAGGAGE
--------------------------------------------------------
  ALTER TABLE CHECKOUT_OW."BOOKINGFLIGHTBAGGAGE" ADD PRIMARY KEY ("ID") USING INDEX  ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table BOOKINGFLIGHTBAGGAGE
--------------------------------------------------------
  ALTER TABLE CHECKOUT_OW."BOOKINGFLIGHTBAGGAGE" ADD CONSTRAINT "FK_BOOKINGFLIGHTBAGGAGE_ID" FOREIGN KEY ("ID")
  REFERENCES CHECKOUT_OW."ITEM" ("ID") ENABLE;


--------------------------------------------------------
--  Ref Synonym for Table BOOKINGFLIGHTBAGGAGE
--------------------------------------------------------

  CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."BOOKINGFLIGHTBAGGAGE" FOR "CHECKOUT_OW"."BOOKINGFLIGHTBAGGAGE";

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------CHOSENFLIGHTBAGGAGE----------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------
--  DDL for Table CHOSENFLIGHTBAGGAGE
--------------------------------------------------------
    CREATE TABLE CHECKOUT_OW."CHOSENFLIGHTBAGGAGE" 
   (  
       "ID" NUMBER(19, 0) NOT NULL , 
       "RECORDLOCATOR" VARCHAR2(255 BYTE),    
       "FLIGHTNUMBER" VARCHAR2(255 BYTE),
       "DEPARTUREDATE" TIMESTAMP(6),
       "DEPARTUREAIRPORTCODE" VARCHAR2(255 BYTE), 
       "ARRIVALDATE" TIMESTAMP(6),
       "ARRIVALAIRPORTCODE" VARCHAR2(255 BYTE), 
       "BOOKINGFLIGHTBAGGAGE_ID" NUMBER(19, 0),
       "FLIGHTSELLKEY"  VARCHAR2(255 BYTE)        
   ) ;


--------------------------------------------------------
--  DDL for Index SYS_C009454
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C009454" ON CHECKOUT_OW."CHOSENFLIGHTBAGGAGE" ("ID");
--------------------------------------------------------
--  Constraints for Table CHOSENFLIGHTBAGGAGE
--------------------------------------------------------
  ALTER TABLE CHECKOUT_OW."CHOSENFLIGHTBAGGAGE" ADD PRIMARY KEY ("ID") USING INDEX  ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table CHOSENFLIGHTBAGGAGE
--------------------------------------------------------
  ALTER TABLE CHECKOUT_OW."CHOSENFLIGHTBAGGAGE" ADD CONSTRAINT "FK_BOOKFLIGHTBAGGAGECHOSEN_ID" FOREIGN KEY ("BOOKINGFLIGHTBAGGAGE_ID")
  REFERENCES CHECKOUT_OW."ITEM" ("ID") ENABLE;

--------------------------------------------------------
--  Ref Sequence for Table CHOSENFLIGHTBAGGAGE
--------------------------------------------------------

   CREATE SEQUENCE  "CHECKOUT_OW"."CHOSENFLIGHTBAGGAGE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 3 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

--------------------------------------------------------
--  Ref Synonym for Table and Synonym CHOSENFLIGHTBAGGAGE
--------------------------------------------------------

  CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."CHOSENFLIGHTBAGGAGE" FOR "CHECKOUT_OW"."CHOSENFLIGHTBAGGAGE";
  CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."CHOSENFLIGHTBAGGAGE_SEQ" FOR "CHECKOUT_OW"."CHOSENFLIGHTBAGGAGE_SEQ";



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------BAGGAGE----------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------
--  DDL for Table BAGGAGE
--------------------------------------------------------
    CREATE TABLE CHECKOUT_OW."BAGGAGE" 
   (  
       "ID" NUMBER(19, 0) NOT NULL , 
       "PASSENGERID" VARCHAR2(255 BYTE),    
       "CODE" VARCHAR2(255 BYTE),
       "QUANTITY" NUMBER(5,0),
       "AMOUNT" NUMBER(19, 4), 
       "WEIGHT" NUMBER(19, 4), 
       "MAXWEIGHT" NUMBER(19, 4), 
       "KEYVALUE" VARCHAR2(255 BYTE),
       "COSTMILES" NUMBER(10, 0), 
       "COSTMONEY" NUMBER(19, 4), 
       "CHOSENFLIGHTBAGGAGE_ID" NUMBER(19, 0),
       "FIRSTNAME" VARCHAR2(255 BYTE),
       "LASTNAME" VARCHAR2(255 BYTE)
   ) ;

--------------------------------------------------------
--  DDL for Index SYS_C009457
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C009457" ON CHECKOUT_OW."BAGGAGE" ("ID");
--------------------------------------------------------
--  Constraints for Table BAGGAGE
--------------------------------------------------------
  ALTER TABLE CHECKOUT_OW."BAGGAGE" ADD PRIMARY KEY ("ID") USING INDEX  ENABLE;


--------------------------------------------------------
--  Ref Constraints for Table BAGGAGE -> SIEBELTRANSACTION
--------------------------------------------------------

  ALTER TABLE CHECKOUT_OW."SIEBELTRANSACTION"  
  ADD (BAGGAGE_ID  NUMBER(19,4));

  ALTER TABLE CHECKOUT_OW."SIEBELTRANSACTION"
  ADD CONSTRAINT "SBLTRANSACTIONBAGGAGEID" FOREIGN KEY ("BAGGAGE_ID")
	  REFERENCES CHECKOUT_OW."BAGGAGE" ("ID") ENABLE;

--------------------------------------------------------
--  Ref Sequence for Table BAGGAGE
--------------------------------------------------------

   CREATE SEQUENCE  "CHECKOUT_OW"."BAGGAGE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 3 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
   
   
--------------------------------------------------------
--  Ref Constraints for Table BAGGAGE
--------------------------------------------------------
  ALTER TABLE CHECKOUT_OW."BAGGAGE" ADD CONSTRAINT "FK_CHOSENFLIGHTBAGGAGE_ID" FOREIGN KEY ("CHOSENFLIGHTBAGGAGE_ID")
  REFERENCES CHECKOUT_OW."CHOSENFLIGHTBAGGAGE" ("ID") ENABLE;
  

--------------------------------------------------------
--  Ref Synonym for Table and Synonym BAGGAGE
--------------------------------------------------------

  CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."BAGGAGE" FOR "CHECKOUT_OW"."BAGGAGE";  
  CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."BAGGAGE_SEQ" FOR "CHECKOUT_OW"."BAGGAGE_SEQ"; 



--------------------------------------------------------
--  ADD COLUMN TABLE FLIGHTPASSENGER
--------------------------------------------------------

  ALTER TABLE CHECKOUT_OW."FLIGHTPASSENGER"  
  ADD (TIER  VARCHAR2(255 BYTE));


--------------------------------------------------------
--  ADD COLUMN TABLE BOOKINGFLIGHT
--------------------------------------------------------

  ALTER TABLE CHECKOUT_OW."BOOKINGFLIGHT"  
  ADD (ISHASVOUCHER  NUMBER(1, 0) DEFAULT 0);





---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------BAGGAGEUNIT---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE SEQUENCE  "CHECKOUT_OW"."BAGGAGEUNIT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOPARTITION ;




CREATE TABLE "CHECKOUT_OW"."BAGGAGEUNIT" 
   (  "ID" VARCHAR2(255) NOT NULL ENABLE, 
  "UNITNUMBER" NUMBER(10,0) NOT NULL ENABLE, 
  "COSTMILES" NUMBER(10,0), 
  "COSTMONEY" NUMBER(19,4), 
  "BAGGAGE_ID" NUMBER(19,4), 
   PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBS_CHECKOUT_OW"  ENABLE, 
   CONSTRAINT "BAGGEUNITBAGGAGEID" FOREIGN KEY ("BAGGAGE_ID")
    REFERENCES "CHECKOUT_OW"."BAGGAGE" ("ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBS_CHECKOUT_OW";



ALTER TABLE "CHECKOUT_OW"."BAGGAGE"
ADD "CARTQUANTITY" NUMBER(5,0);



CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."BAGGAGEUNIT" FOR "CHECKOUT_OW"."BAGGAGEUNIT";  
CREATE OR REPLACE SYNONYM "CHECKOUT_WLS"."BAGGAGEUNIT_SEQ" FOR "CHECKOUT_OW"."BAGGAGEUNIT_SEQ"; 


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




