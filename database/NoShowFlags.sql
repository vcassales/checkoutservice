--------------------------------------------------------
--  Add Colunm Table ITEM
--------------------------------------------------------
ALTER TABLE "CHECKOUT_OW"."ITEM"
  ADD (
    "ISNOSHOW" NUMBER(1,0),
    "REFUNDBYNOSHOW" NUMBER(1,0)
  );
