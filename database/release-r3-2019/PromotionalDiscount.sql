--------------------------------------------------------
---  ALTER TABLE CHECKOUTORDER
--------------------------------------------------------
ALTER TABLE "CHECKOUT_OW"."CHECKOUTORDER"
ADD ("AVAILABLESMILESCLUBPLANID" VARCHAR2(255 BYTE));



--------------------------------------------------------
---  ALTER TABLE BOOKINGFLIGHT
--------------------------------------------------------
ALTER TABLE "CHECKOUT_OW"."BOOKINGFLIGHT"
ADD ("AVAILABLEDISCOUNTMILES" NUMBER(10,0));