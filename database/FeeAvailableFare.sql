--------------------------------------------------------
--  CREATE TABLE FEEAVAILABLEFARE
--------------------------------------------------------
CREATE TABLE CHECKOUT_OW.FEEAVAILABLEFARE
(
  ID NUMBER(19, 0) NOT NULL
, CACHEID VARCHAR2(255 BYTE)
, OFFER NUMBER(1, 0)
, MILES NUMBER(10, 0)
, MONEY NUMBER(19, 4)
, FEE_ID NUMBER(19, 0) 
);



--------------------------------------------------------
--  CREATE CONSTRAINTS ON FEEAVAILABLEFARE
--------------------------------------------------------
ALTER TABLE CHECKOUT_OW.FEEAVAILABLEFARE
ADD CONSTRAINT FEEAVLBLFARE_PK PRIMARY KEY (ID)
ENABLE;


ALTER TABLE CHECKOUT_OW.FEEAVAILABLEFARE
ADD CONSTRAINT FEEAVLBLFAREFEEID FOREIGN KEY
(
  FEE_ID 
)
REFERENCES CHECKOUT_OW.FEE
(
  ID 
)
ENABLE;



--------------------------------------------------------
--  CREATE SEQUENCE ON FEEAVAILABLEFARE
--------------------------------------------------------
CREATE SEQUENCE CHECKOUT_OW.FEEAVAILABLEFARE_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;



--------------------------------------------------------
--  CREATE INDEX ON FEEAVAILABLEFARE
--------------------------------------------------------
CREATE INDEX CHECKOUT_OW.IDX_CACHEID_FEEAVLBLFARE ON CHECKOUT_OW.FEEAVAILABLEFARE (CACHEID ASC);



--------------------------------------------------------
--  GRANT USER CHECKOUT_WLS
--------------------------------------------------------
GRANT ALL ON CHECKOUT_OW.FEEAVAILABLEFARE TO CHECKOUT_WLS;
GRANT SELECT ON CHECKOUT_OW.FEEAVAILABLEFARE_SEQ TO CHECKOUT_WLS;



--------------------------------------------------------
--  GRANT USER monitoracao
--------------------------------------------------------
GRANT SELECT ON CHECKOUT_OW.FEEAVAILABLEFARE TO monitoracao;
GRANT SELECT ON CHECKOUT_OW.FEEAVAILABLEFARE_SEQ TO monitoracao;



--------------------------------------------------------
--  CREATE SYNONYM CHECKOUT_WLS
--------------------------------------------------------
CREATE OR REPLACE SYNONYM CHECKOUT_WLS.FEEAVAILABLEFARE FOR CHECKOUT_OW.FEEAVAILABLEFARE;
CREATE OR REPLACE SYNONYM CHECKOUT_WLS.FEEAVAILABLEFARE_SEQ FOR CHECKOUT_OW.FEEAVAILABLEFARE_SEQ;

