ALTER TABLE "CHECKOUT_OW"."CHECKOUTORDER"
ADD ("REGION" VARCHAR2(255 BYTE));

ALTER TABLE "CHECKOUT_OW"."CHOSENFLIGHTSEGMENT"
ADD ("CURRENCYRATE" NUMBER(19,4));

ALTER TABLE "CHECKOUT_OW"."FLIGHTPASSENGER"
ADD ("NATIONALITY" VARCHAR2(255 BYTE));

