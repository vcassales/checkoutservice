--------------------------------------------------------
--  CREATE COLUMN
--------------------------------------------------------
ALTER TABLE CHECKOUT_OW.SIEBELTRANSACTION ADD (PRODUCT_ID  NUMBER(19,4));

--------------------------------------------------------
--  CREATE INDEX SIEBELTRANSACTION
--------------------------------------------------------
CREATE INDEX CHECKOUT_OW.IDX_PROD_ID_SIEBELTRANS ON CHECKOUT_OW.SIEBELTRANSACTION (PRODUCT_ID ASC); 

--------------------------------------------------------
--  TABLE PRODUCT
--------------------------------------------------------
CREATE TABLE CHECKOUT_OW.PRODUCT 
(  
	ID                   NUMBER(19, 0) NOT NULL, 
	TYPE                 VARCHAR2(255 BYTE),    
        SUBTYPE              VARCHAR2(255 BYTE), 
        CREDITVALUE          NUMBER(19, 4),
        SENDAPPROVALDATE     TIMESTAMP (6),
        RETURNANALYSISDATE   TIMESTAMP (6),
        GIFT                 VARCHAR2(255 BYTE), 
        GIFTDATE             TIMESTAMP (6),
        TRANSACTIONID        VARCHAR2(255 BYTE),
        MONEY                NUMBER(19,4), 
        MILES                NUMBER(10,0), 
        BONUSMILES           NUMBER(10,0), 
        CANCELDATE           TIMESTAMP (6),
        PRIMARY KEY (ID)
);

--------------------------------------------------------
--  FK  PRODUCT -> ITEM
--------------------------------------------------------
ALTER TABLE CHECKOUT_OW.PRODUCT ADD CONSTRAINT FK_PRODUCT_ID FOREIGN KEY (ID) REFERENCES CHECKOUT_OW.ITEM (ID) ENABLE;

--------------------------------------------------------
--  FK  SIEBELTRANSACTION -> PRODUCT
--------------------------------------------------------
 ALTER TABLE CHECKOUT_OW.SIEBELTRANSACTION ADD CONSTRAINT SBLTRANSACTIONPRODUCTID FOREIGN KEY ("PRODUCT_ID") REFERENCES CHECKOUT_OW.PRODUCT (ID) ENABLE;

--------------------------------------------------------
--  GRANT USER  CHECKOUT_WLS
--------------------------------------------------------
GRANT ALL ON CHECKOUT_OW.PRODUCT TO CHECKOUT_WLS;

--------------------------------------------------------
--  GRANT USER  monitoracao
--------------------------------------------------------
GRANT SELECT ON CHECKOUT_OW.PRODUCT TO monitoracao;

--------------------------------------------------------
--  GRANT SYNONYM  CHECKOUT_WLS
--------------------------------------------------------
CREATE OR REPLACE SYNONYM CHECKOUT_WLS.PRODUCT FOR CHECKOUT_OW.PRODUCT;

