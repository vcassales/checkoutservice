
package br.com.smiles.checkout.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SayHelloResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SayHelloResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://smiles.com.br/Helloworld/schemas/v1}HelloWorld"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SayHelloResponse", namespace = "http://smiles.com.br/Helloworld/wsdl/v1", propOrder = {
    "helloWorld"
})
public class SayHelloResponse {

    @XmlElement(name = "HelloWorld", namespace = "http://smiles.com.br/Helloworld/schemas/v1", required = true)
    protected HelloWorldType helloWorld;

    /**
     * Gets the value of the helloWorld property.
     * 
     * @return
     *     possible object is
     *     {@link HelloWorldType }
     *     
     */
    public HelloWorldType getHelloWorld() {
        return helloWorld;
    }

    /**
     * Sets the value of the helloWorld property.
     * 
     * @param value
     *     allowed object is
     *     {@link HelloWorldType }
     *     
     */
    public void setHelloWorld(HelloWorldType value) {
        this.helloWorld = value;
    }

}
