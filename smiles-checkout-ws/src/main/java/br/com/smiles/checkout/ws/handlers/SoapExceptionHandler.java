package br.com.smiles.checkout.ws.handlers;

import br.com.smiles.checkout.ws.SmilesFault;
import br.com.smiles.checkout.ws.SmilesFaultType;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Set;

/**
 * Created by adautomartins on 26/04/16.
 */
public class SoapExceptionHandler implements SOAPHandler<SOAPMessageContext> {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void close(MessageContext context) { }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        try {
            boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

            if (outbound) {
                logger.info("Processing " + context + " for exceptions");
                SOAPMessage msg = ((SOAPMessageContext) context).getMessage();
                SOAPFault fault = msg.getSOAPBody().getFault();
                // Retrives the exception from the context
                Exception ex = (Exception) context.get("exception");
                if (ex != null) {
                    // Get my exception
                    int indexOfType = ExceptionUtils.indexOfType(ex, SmilesFault.class);
                    if (indexOfType != -1) {
                        ex = (SmilesFault)ExceptionUtils.getThrowableList(ex).get(indexOfType);
                        SmilesFault myEx = (SmilesFault) ex;
                        // Add a fault to the body if not there already
                        if (fault == null) {
                            fault = msg.getSOAPBody().addFault();
                            fault.setFaultString(myEx.getMessage());
                        }
                        try {
                            JAXBContext jaxContext = JAXBContext.newInstance(SmilesFaultType.class);
                            Marshaller marshaller = jaxContext.createMarshaller();
                            //Add the UserMessage xml as a fault detail. Detail interface extends Node
                            marshaller.marshal(myEx.getFaultInfo(), fault.addDetail());
                        } catch (JAXBException e) {
                            throw new RuntimeException("Can't marshall the user message ", e);
                        }
                    }else {
                        logger.info("This is not an AmsException");
                    }
                }else {
                    logger.warn("No exception found in the webServiceContext");
                }
            }

        } catch (SOAPException e) {
            logger.warn("Error when trying to access the soap message", e);
        }
        return true;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

}