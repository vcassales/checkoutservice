package br.com.smiles.checkout.ws.impl;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.WebServiceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.smiles.checkout.domain.HelloWorld;
import br.com.smiles.checkout.business.HelloWorldBeanLocal;
import br.com.smiles.checkout.ws.SmilesFault;
import br.com.smiles.checkout.ws.EventArrayType;
import br.com.smiles.checkout.ws.HelloWorldType;
import br.com.smiles.checkout.ws.ObjectFactory;
import br.com.smiles.checkout.ws.SayHelloRequest;
import br.com.smiles.checkout.ws.SayHelloResponse;
import br.com.smiles.checkout.ws.handlers.ColetaTrilhaAuditoria;
import br.com.smiles.checkout.ws.handlers.HandleWSException;


/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.2.9-b14002 Generated
 * source version: 2.1
 * 
 */
@WebService(serviceName = "Helloworld", name = "Helloworld", targetNamespace = "http://smiles.com.br/Helloworld/wsdl/v1", wsdlLocation = "WEB-INF/wsdl/helloworld/Helloworld.wsdl")
@HandlerChain(file = "../Helloworld_handler.xml")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@javax.xml.ws.BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@XmlSeeAlso({
    ObjectFactory.class
})
@Stateless
public class HelloworldImpl {

	private Logger logger = LoggerFactory.getLogger("AUDIT");

	@Resource
	private WebServiceContext wsContext;
	
	@EJB
	private HelloWorldBeanLocal helloWorldBeanLocal;

	/**
	 * 
	 * @param trilhaAuditoriaHeader
	 * @param parameters
	 * @return returns br.com.smiles.checkout.ws.SayHelloResponse
	 * @throws SmilesFault
	 */
	@WebMethod(action = "http://smiles.com.br/Helloworld/wsdl/v1/sayHello")
	@WebResult(name = "sayHelloResponse", targetNamespace = "http://smiles.com.br/Helloworld/wsdl/v1", partName = "parameters")
	@HandleWSException
	@ColetaTrilhaAuditoria
	public SayHelloResponse sayHello(
			@WebParam(name = "sayHelloRequest", targetNamespace = "http://smiles.com.br/Helloworld/wsdl/v1", partName = "parameters") SayHelloRequest parameters,
			@WebParam(name = "Events", targetNamespace = "http://smiles.com.br/TrilhaAuditoria/schemas/v1", header = true, partName = "trilhaAuditoriaHeader") EventArrayType trilhaAuditoriaHeader)
			throws SmilesFault {
		SayHelloResponse response = new SayHelloResponse();
		response.setHelloWorld(new HelloWorldType());
		logger.info("Trilhando webservice...");
		HelloWorld helloWorld = new HelloWorld();
		if (parameters != null) {
			helloWorld.setName(parameters.getPerson().getName());
		}

		response.getHelloWorld().setMessage(
				helloWorldBeanLocal.sayHelloWorld(helloWorld));
		
		return response;
	}

}
