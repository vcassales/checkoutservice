
package br.com.smiles.checkout.ws;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b14002
 * Generated source version: 2.1
 * 
 */
@WebService(name = "Helloworld", targetNamespace = "http://smiles.com.br/Helloworld/wsdl/v1")
@HandlerChain(file = "Helloworld_handler.xml")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface Helloworld {


    /**
     * 
     * @param trilhaAuditoriaHeader
     * @param parameters
     * @return
     *     returns br.com.smiles.ws.SayHelloResponse
     * @throws SmilesFault
     */
    @WebMethod(action = "http://smiles.com.br/Helloworld/wsdl/v1/sayHello")
    @WebResult(name = "sayHelloResponse", targetNamespace = "http://smiles.com.br/Helloworld/wsdl/v1", partName = "parameters")
    public SayHelloResponse sayHello(
        @WebParam(name = "sayHelloRequest", targetNamespace = "http://smiles.com.br/Helloworld/wsdl/v1", partName = "parameters")
        SayHelloRequest parameters,
        @WebParam(name = "Events", targetNamespace = "http://smiles.com.br/TrilhaAuditoria/schemas/v1", header = true, partName = "trilhaAuditoriaHeader")
        EventArrayType trilhaAuditoriaHeader)
        throws SmilesFault
    ;

}
