
package br.com.smiles.checkout.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.smiles.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SmilesFault_QNAME = new QName("http://smiles.com.br/SmilesFault/schemas/v1", "SmilesFault");
    private final static QName _Person_QNAME = new QName("http://smiles.com.br/Helloworld/schemas/v1", "Person");
    private final static QName _Events_QNAME = new QName("http://smiles.com.br/TrilhaAuditoria/schemas/v1", "Events");
    private final static QName _HelloWorld_QNAME = new QName("http://smiles.com.br/Helloworld/schemas/v1", "HelloWorld");
    private final static QName _SayHelloRequest_QNAME = new QName("http://smiles.com.br/SendHelloworld/wsdl/v1", "sayHelloRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.smiles.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SayHelloRequest }
     * 
     */
    public SayHelloRequest createSayHelloRequest() {
        return new SayHelloRequest();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link HelloWorldType }
     * 
     */
    public HelloWorldType createHelloWorldType() {
        return new HelloWorldType();
    }

    /**
     * Create an instance of {@link EventArrayType }
     * 
     */
    public EventArrayType createEventArrayType() {
        return new EventArrayType();
    }

    /**
     * Create an instance of {@link EventType }
     * 
     */
    public EventType createEventType() {
        return new EventType();
    }

    /**
     * Create an instance of {@link CorrelationDataType }
     * 
     */
    public CorrelationDataType createCorrelationDataType() {
        return new CorrelationDataType();
    }

    /**
     * Create an instance of {@link PayloadItemType }
     * 
     */
    public PayloadItemType createPayloadItemType() {
        return new PayloadItemType();
    }

    /**
     * Create an instance of {@link MetadataType }
     * 
     */
    public MetadataType createMetadataType() {
        return new MetadataType();
    }

    /**
     * Create an instance of {@link PayloadType }
     * 
     */
    public PayloadType createPayloadType() {
        return new PayloadType();
    }

    /**
     * Create an instance of {@link SmilesFaultType }
     * 
     */
    public SmilesFaultType createSmilesFaultType() {
        return new SmilesFaultType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SmilesFaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://smiles.com.br/SmilesFault/schemas/v1", name = "SmilesFault")
    public JAXBElement<SmilesFaultType> createSmilesFault(SmilesFaultType value) {
        return new JAXBElement<SmilesFaultType>(_SmilesFault_QNAME, SmilesFaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://smiles.com.br/Helloworld/schemas/v1", name = "Person")
    public JAXBElement<PersonType> createPerson(PersonType value) {
        return new JAXBElement<PersonType>(_Person_QNAME, PersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventArrayType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://smiles.com.br/TrilhaAuditoria/schemas/v1", name = "Events")
    public JAXBElement<EventArrayType> createEvents(EventArrayType value) {
        return new JAXBElement<EventArrayType>(_Events_QNAME, EventArrayType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HelloWorldType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://smiles.com.br/Helloworld/schemas/v1", name = "HelloWorld")
    public JAXBElement<HelloWorldType> createHelloWorld(HelloWorldType value) {
        return new JAXBElement<HelloWorldType>(_HelloWorld_QNAME, HelloWorldType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayHelloRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://smiles.com.br/SendHelloworld/wsdl/v1", name = "sayHelloRequest")
    public JAXBElement<SayHelloRequest> createSayHelloRequest(SayHelloRequest value) {
        return new JAXBElement<SayHelloRequest>(_SayHelloRequest_QNAME, SayHelloRequest.class, null, value);
    }

}
