package br.com.smiles.checkout.ws.impl;

import br.com.smiles.checkout.business.CheckoutOrderBeanLocal;
import br.com.smiles.checkout.domain.CheckoutOrderSearch;
import br.com.smiles.checkout.entity.CheckoutOrder;
import br.com.smiles.checkout.entity.Item;
import br.com.smiles.checkout.ws.handlers.ColetaTrilhaAuditoria;
import br.com.smiles.checkout.ws.handlers.HandleWSException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * Session Bean implementation class CheckoutServiceImpl
 */
@Stateless
@WebService(serviceName = "CheckoutService", targetNamespace = "http://smiles.com.br/CheckoutService/wsdl/v1")
@HandlerChain(file = "../Helloworld_handler.xml")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@javax.xml.ws.BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP11HTTP_BINDING)
@TransactionAttribute(TransactionAttributeType.NEVER)
public class CheckoutServiceImpl {

	@EJB
	protected CheckoutOrderBeanLocal checkoutOrderBean;
	
	/**
	 * Default constructor.
	 */
	public CheckoutServiceImpl() {
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder create(CheckoutOrder checkout) throws Exception {
		return checkoutOrderBean.create(checkout);
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder findOrder(CheckoutOrder checkoutOrder) {
		return checkoutOrderBean.getOrder(checkoutOrder);
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder update(CheckoutOrder checkout) throws Exception {
			checkoutOrderBean.update(checkout);
			return checkoutOrderBean.getOrder(checkout);
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public List<CheckoutOrder> search(CheckoutOrderSearch checkoutSearch) throws Exception {
		return checkoutOrderBean.search(checkoutSearch);
	}
}
