package br.com.smiles.checkout.ws.handlers;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.MDC;

import br.com.smiles.checkout.ws.EventArrayType;
import br.com.smiles.checkout.ws.EventType;

@Interceptor
@ColetaTrilhaAuditoria
public class TrilhaAuditoriaInterceptor {

	@AroundInvoke
	public Object interceptWithReturn(InvocationContext ctx) throws Exception {
		for (Object param : ctx.getParameters()) {
			if (param instanceof EventArrayType) {
				EventArrayType trilhaAuditoria = (EventArrayType) param;

				for (EventType event : trilhaAuditoria.getEvent()) {
					MDC.put("version", trilhaAuditoria.getVersion());
					if (event.getMeta() != null) {
						MDC.put("sourceAddr", event.getMeta().getSourceAddr());
					}
					
					break; // Get only the first item
				}
			}
		}
		
		return ctx.proceed();
	}
}
