package br.com.smiles.checkout.ws.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import br.com.smiles.checkout.business.CheckoutOrderBeanLocal;
import br.com.smiles.checkout.business.ItemBeanLocal;
import br.com.smiles.checkout.domain.ChosenBaggageSearch;
import br.com.smiles.checkout.domain.ChosenSeatSearch;
import br.com.smiles.checkout.entity.Benefit;
import br.com.smiles.checkout.entity.BookingFlight;
import br.com.smiles.checkout.entity.BookingFlightBaggage;
import br.com.smiles.checkout.entity.BookingFlightSeat;
import br.com.smiles.checkout.entity.CheckoutOrder;
import br.com.smiles.checkout.entity.ChosenFlightBaggage;
import br.com.smiles.checkout.entity.ChosenFlightSeat;
import br.com.smiles.checkout.entity.ChosenFlightSegment;
import br.com.smiles.checkout.entity.FamilyAccount;
import br.com.smiles.checkout.entity.FlightPassenger;
import br.com.smiles.checkout.entity.Item;
import br.com.smiles.checkout.entity.Membership;
import br.com.smiles.checkout.entity.PaymentData;
import br.com.smiles.checkout.entity.RefundPayment;
import br.com.smiles.checkout.entity.SiebelTransaction;
import br.com.smiles.checkout.ws.handlers.ColetaTrilhaAuditoria;
import br.com.smiles.checkout.ws.handlers.HandleWSException;

/**
 * Session Bean implementation class ItemServiceImpl
 */
@Stateless
@WebService(serviceName = "ItemService", targetNamespace = "http://smiles.com.br/ItemService/wsdl/v1")
@HandlerChain(file = "../Helloworld_handler.xml")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@javax.xml.ws.BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP11HTTP_BINDING)
@TransactionAttribute(TransactionAttributeType.NEVER)
public class ItemServiceImpl {

	@EJB
	protected CheckoutOrderBeanLocal checkoutOrderBean;
	
	@EJB
	protected ItemBeanLocal itemBean;

	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder remove(Item item) throws Exception {
		itemBean.remove(item);
		return checkoutOrderBean.getOrder(item.getCheckoutOrder());
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder insert(Item item) throws Exception {
		itemBean.create(item);
		return checkoutOrderBean.getOrder(item.getCheckoutOrder());
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder updateStatus(Item item) throws Exception {
		itemBean.updateStatus(item);
		return checkoutOrderBean.getOrder(item.getCheckoutOrder());
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item findById(Item item) throws Exception {
		return itemBean.findById(item);
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item addFlightBookingSeat(BookingFlightSeat bookingFlightSeat) throws Exception {
		return itemBean.addBookingFlightSeat(bookingFlightSeat);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item updateChosenSeat(ChosenFlightSeat chosenFlightSeat) throws Exception {
		return itemBean.UpdateChosenSeat(chosenFlightSeat);
		 
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item addChosenSeat(ChosenFlightSeat chosenFlightSeat) throws Exception {
		return itemBean.addChosenFlightSeat(chosenFlightSeat);
		 
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item deleteChosenSeat(ChosenFlightSeat chosenFlightSeat) throws Exception {
		return itemBean.removeChosenSeat(chosenFlightSeat);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public List<ChosenFlightSeat> searchChosenSeat(ChosenSeatSearch search) throws Exception {
		return itemBean.SearchChosenSeat(search);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public PaymentData addRefund(RefundPayment refundPayment) throws Exception {
		return itemBean.addRefundPayment(refundPayment);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public SiebelTransaction addSiebelTransaction(SiebelTransaction siebelTransaction) throws Exception {
		return itemBean.addSiebelTransaction(siebelTransaction);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public SiebelTransaction cancelSiebelTransaction(SiebelTransaction siebelTransaction) throws Exception {
		return itemBean.cancelSiebelTransaction(siebelTransaction);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item addPaymentData(PaymentData paymentData) throws Exception {
		return itemBean.addPaymentData(paymentData);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public BookingFlight addChosenFlightSegment(ChosenFlightSegment chosenFlightSegment) throws Exception {
		return itemBean.addChosenFlightSegment(chosenFlightSegment);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item addBookingFlight(BookingFlight bookingFlight) throws Exception {
		return itemBean.addBookingFlight(bookingFlight);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public BookingFlight updateBookingFlight(BookingFlight bookingFlight) throws Exception {
		return itemBean.updateBookingFlight(bookingFlight);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public List<ChosenFlightSegment> addFlightPassengers(List<FlightPassenger> flightPassenger) throws Exception {
		return itemBean.addFlightPassengers(flightPassenger);
	}

	@HandleWSException
	@ColetaTrilhaAuditoria
	public void removePayment(PaymentData paymentData) throws Exception {
		itemBean.deletePaymentData(paymentData);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public void updateMembership(Membership membership) throws Exception {
		itemBean.updateMembership(membership);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public void updateFamilyAccount(FamilyAccount familyAccount) throws Exception {
		itemBean.updateFamilyAccount(familyAccount);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public Item addFlightBookingBaggage(BookingFlightBaggage bookingFlightBaggage) throws Exception {
		return itemBean.addBookingFlightBaggage(bookingFlightBaggage);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public BookingFlightBaggage updateBookingFlightBaggage(BookingFlightBaggage bookingFlightBaggage) throws Exception {
		return itemBean.updateBookingFlightBaggage(bookingFlightBaggage);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public List<ChosenFlightBaggage> searchChosenBaggage(ChosenBaggageSearch search) throws Exception {
		return itemBean.searchChosenBaggage(search);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder insertBenefit(Benefit benefit) throws Exception {
		benefit = itemBean.insertBenefit(benefit);
		CheckoutOrder order = null;
		
		if (benefit.getItem()!=null && benefit.getItem().getId()!=null){
			order =  itemBean.findByItemId(benefit.getItem()).getCheckoutOrder();
		}
		else if (benefit.getCheckoutOrder()!=null && benefit.getCheckoutOrder().getId()!=null){
			order = benefit.getCheckoutOrder();
		}

		return checkoutOrderBean.getOrder(order);
	}
	
	@HandleWSException
	@ColetaTrilhaAuditoria
	public CheckoutOrder updateChosenFare(BookingFlight bookingFlight) throws Exception {
		bookingFlight = itemBean.updateChosenFare(bookingFlight);
		return checkoutOrderBean.getOrder(bookingFlight.getCheckoutOrder());
	}
	
}
