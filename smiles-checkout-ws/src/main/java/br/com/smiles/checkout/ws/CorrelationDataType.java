
package br.com.smiles.checkout.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CorrelationDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorrelationDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="userId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="correlationId" type="{http://smiles.com.br/TrilhaAuditoria/schemas/v1}CorrelationIDType" />
 *       &lt;attribute name="clientAddr" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="clientCorrelationId" type="{http://smiles.com.br/TrilhaAuditoria/schemas/v1}CorrelationIDType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorrelationDataType")
public class CorrelationDataType {

    @XmlAttribute(name = "userId")
    protected String userId;
    @XmlAttribute(name = "correlationId")
    protected String correlationId;
    @XmlAttribute(name = "clientAddr")
    protected String clientAddr;
    @XmlAttribute(name = "clientCorrelationId")
    protected String clientCorrelationId;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the correlationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrelationId() {
        return correlationId;
    }

    /**
     * Sets the value of the correlationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrelationId(String value) {
        this.correlationId = value;
    }

    /**
     * Gets the value of the clientAddr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientAddr() {
        return clientAddr;
    }

    /**
     * Sets the value of the clientAddr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientAddr(String value) {
        this.clientAddr = value;
    }

    /**
     * Gets the value of the clientCorrelationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientCorrelationId() {
        return clientCorrelationId;
    }

    /**
     * Sets the value of the clientCorrelationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientCorrelationId(String value) {
        this.clientCorrelationId = value;
    }

}
