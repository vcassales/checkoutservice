package br.com.smiles.checkout.ws.handlers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import br.com.smiles.checkout.ws.SmilesFaultType;
import br.com.smiles.checkout.ws.EventArrayType;
import br.com.smiles.checkout.ws.EventType;
import br.com.smiles.checkout.ws.ObjectFactory;

public class TrilhaAuditoriaHandler implements SOAPHandler<SOAPMessageContext> {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void close(MessageContext arg0) {

	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		Boolean isRequest = (Boolean) context
				.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		String request = "";
		String response = "";

		// Treat only inbound messages
		if (!isRequest) {

			try {
				SOAPMessage soapMsg = context.getMessage();
				SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
				SOAPHeader soapHeader = soapEnv.getHeader();

				// if no header, add one
				if (soapHeader == null) {
					soapHeader = soapEnv.addHeader();
					// throw exception
					generateSOAPErrMessage(soapMsg, "Nenhum header SOAP foi associado a mensagem.");
				}

				// Obtém os dados de auditoria
				Iterator<Node> it = soapHeader
						.getChildElements(new QName("http://smiles.com.br/TrilhaAuditoria/schemas/v1", "Events"));

				// if no header block for next actor found? throw exception
				if (it == null || !it.hasNext()) {
					generateSOAPErrMessage(soapMsg,
							"Não existe nenhum Header para a trilha de auditoria.");
				}

				final Unmarshaller unmarshaller = JAXBContext.newInstance(
						ObjectFactory.class).createUnmarshaller();
				// marshalling instance (appending) to SOAP header's xml node
				EventArrayType trilhaAuditoria = ((JAXBElement<EventArrayType>) unmarshaller
						.unmarshal(it.next())).getValue();

				if (trilhaAuditoria != null && trilhaAuditoria.getEvent() != null && trilhaAuditoria.getEvent().size() > 0) {
					EventType event = trilhaAuditoria.getEvent().get(0);
					if (event.getXref() != null) {
						MDC.put("correlationId", event.getXref().getCorrelationId());
					}
					if (event.getMeta() != null) {
						MDC.put("serviceId", event.getMeta().getServiceId());
						MDC.put("sourceAddr", event.getMeta().getSourceAddr());
						MDC.put("sourceId", event.getMeta().getSourceId());
					}
				}

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				soapMsg.writeTo(baos);
				request = baos.toString();
				baos.close();
			} catch (IOException e) {
				logger.error("Um erro ocorreu durante a leitura de headers da mensagem SOAP.", e);
			} catch (SOAPException e) {
				logger.error("Um erro ocorreu durante a leitura de headers da mensagem SOAP.", e);
			} catch (JAXBException e) {
				logger.error("Um erro ocorreu durante a conversão de headers da mensagem SOAP.", e);
			}

		} else {
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				SOAPMessage soapMsg = context.getMessage();
				soapMsg.writeTo(baos);
				response = baos.toString();
				baos.close();
			} catch (Exception e) {
				logger.error("Um erro ocorreu durante a leitura de headers da mensagem SOAP.", e);
			}
		}

		logger.debug("Logging TrilhaAuditoria " + (!request.isEmpty() ? "REQUEST: " + request.replaceAll("[\n\r]", "") : " RESPONSE: " + response.replaceAll("[\n\r]", "")));

		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

	private void generateSOAPErrMessage(SOAPMessage msg, String reason) {
		try {
			SOAPBody soapBody = msg.getSOAPPart().getEnvelope().getBody();
			SOAPFault soapFault = soapBody.addFault();
			soapFault.setFaultString(reason);

			// Generate a SmilesFault exception node
			SmilesFaultType fault = new SmilesFaultType();
			fault.setCodigo("400");
			fault.setDescricao(reason);

			// Effectively append the fault on the SOAPFault element
			final Marshaller marshaller = JAXBContext.newInstance(
					ObjectFactory.class).createMarshaller();
			marshaller.marshal(fault, soapFault.addDetail());

			throw new SOAPFaultException(soapFault);
		} catch (SOAPException e) {
			logger.error("Um erro ocorreu durante a geração de um elemento Fault para a mensagem SOAP.", e);
		} catch (JAXBException e) {
			logger.error("Um erro ocorreu durante a a conversão do objeto SmilesFault para elemento XML da mensagem SOAP.", e);
		}
	}
}
