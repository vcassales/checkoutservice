package br.com.smiles.checkout.ws.handlers;

import javax.annotation.Resource;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.xml.ws.WebServiceContext;

import br.com.smiles.checkout.exception.SmilesAbstractException;
import br.com.smiles.checkout.exception.SmilesAbstractRuntimeException;
import br.com.smiles.checkout.util.ExceptionUtil;
import br.com.smiles.checkout.ws.SmilesFault;
import br.com.smiles.checkout.ws.SmilesFaultType;

@Interceptor
@HandleWSException
public class HandleWSExceptionInterceptor {

	@Resource
	private WebServiceContext context;

	@AroundInvoke
	public Object intercept(InvocationContext ctx) throws Exception {
		try {
			return ctx.proceed();
		} catch (SmilesAbstractException ex) {
			throw convertException(ex.getCode(), ex.getMessage(), ex);
		} catch (SmilesAbstractRuntimeException ex) {
			throw convertException(ex.getCode(), ex.getMessage(), ex);
		} catch (Exception ex) {
			throw convertException(ex.getClass().getName(), ex.getMessage(), ex);
		}
	}

	private SmilesFault convertException(String code, String message, Exception ex) {
		SmilesFaultType fault = new SmilesFaultType();
		fault.setCodigo(code);
		fault.setDescricao(message);
		fault.setStacktrace(ExceptionUtil.getStackTrace(ex));

		SmilesFault faultEx = new SmilesFault("Erro ao processar requisição.", fault);

		if (context != null && context.getMessageContext() != null) {
			context.getMessageContext().put("exception", faultEx);
		}

		return faultEx;
	}
}
