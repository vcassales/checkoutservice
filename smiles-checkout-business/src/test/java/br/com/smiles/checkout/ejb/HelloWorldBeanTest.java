/**
 * 
 */
package br.com.smiles.checkout.ejb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @author adautomartins
 *
 */
@RunWith(PowerMockRunner.class)
public class HelloWorldBeanTest {
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test method for {@link br.com.smiles.checkout.ejb.HelloWorldBean#sendHelloWorld(java.lang.String)}.
	 */
	@Test
	public final void testSendHelloWorld() {
		assertEquals("Esta correto", "", "");
	}

	/**
	 * Test method for {@link br.com.smiles.checkout.ejb.HelloWorldBean#sayHelloWorld(java.lang.String)}.
	 */
	@Test
	public final void testSayHelloWorld() {
		assertEquals("Esta correto", "", "");
	}

}
