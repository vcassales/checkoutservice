package br.com.smiles.checkout.business;

import java.util.List;

import br.com.smiles.checkout.domain.ChosenBaggageSearch;
import br.com.smiles.checkout.domain.ChosenSeatSearch;
import br.com.smiles.checkout.entity.Benefit;
import br.com.smiles.checkout.entity.BookingFlight;
import br.com.smiles.checkout.entity.BookingFlightBaggage;
import br.com.smiles.checkout.entity.BookingFlightSeat;
import br.com.smiles.checkout.entity.ChosenFlightBaggage;
import br.com.smiles.checkout.entity.ChosenFlightSeat;
import br.com.smiles.checkout.entity.ChosenFlightSegment;
import br.com.smiles.checkout.entity.FamilyAccount;
import br.com.smiles.checkout.entity.FlightPassenger;
import br.com.smiles.checkout.entity.Item;
import br.com.smiles.checkout.entity.Membership;
import br.com.smiles.checkout.entity.PaymentData;
import br.com.smiles.checkout.entity.RefundPayment;
import br.com.smiles.checkout.entity.SiebelTransaction;
import br.com.smiles.checkout.exception.InvalidItemMessageException;
import br.com.smiles.checkout.exception.ItemInvalidEventAdnReceiverMessageException;
import br.com.smiles.checkout.exception.ItemInvalidStatusMessageException;
import br.com.smiles.checkout.exception.ItemNotFoundException;
import br.com.smiles.checkout.exception.ItemParametersInvalid;

public interface ItemBeanLocal {

	Item create(Item item) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;

	Item addBookingFlightSeat(BookingFlightSeat bookingFlightSeat) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;
	
	Item addChosenFlightSeat(ChosenFlightSeat chosenFlightSeat) throws ItemInvalidEventAdnReceiverMessageException, ItemInvalidStatusMessageException, ItemNotFoundException;
	
	Item removeChosenSeat(ChosenFlightSeat ChosenFlightSeat) throws ItemInvalidStatusMessageException,ItemNotFoundException;
	
	void remove(Item item) throws ItemInvalidStatusMessageException,ItemNotFoundException;
	
	ChosenFlightSeat findChosenFlightId(ChosenFlightSeat chosenFlightSeat) throws ItemNotFoundException;
	
	Item findById(Item item) throws ItemNotFoundException;
	
	List<ChosenFlightSeat> SearchChosenSeat(ChosenSeatSearch search)throws ItemParametersInvalid;
	
	void updateStatus(Item item) throws ItemInvalidStatusMessageException,ItemNotFoundException;
	
	SiebelTransaction addSiebelTransaction(SiebelTransaction siebelTransaction) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;

	SiebelTransaction cancelSiebelTransaction(SiebelTransaction siebelTransaction) throws ItemInvalidStatusMessageException,ItemNotFoundException;
	
	Item UpdateChosenSeat(ChosenFlightSeat chosenFlightSeat)throws ItemInvalidStatusMessageException,ItemNotFoundException;

	PaymentData addRefundPayment(RefundPayment refundPayment) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;

	Item addPaymentData(PaymentData paymentData) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException,ItemNotFoundException;

	BookingFlight addChosenFlightSegment(ChosenFlightSegment chosenFlightSegment) throws InvalidItemMessageException;
	
	Item addBookingFlight(BookingFlight bookingFlight) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;

	BookingFlight updateBookingFlight(BookingFlight bookingFlight) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;

	List<ChosenFlightSegment> addFlightPassengers(List<FlightPassenger> flightPassengerList) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;

	void deletePaymentData(PaymentData paymentData) throws ItemInvalidStatusMessageException, InvalidItemMessageException,ItemNotFoundException;
	
	Item updateMembership(Membership membership) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException,ItemNotFoundException;
	
	Item updateFamilyAccount(FamilyAccount familyAccount) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException,ItemNotFoundException;
	
	Item addBookingFlightBaggage(BookingFlightBaggage bookingFlightBaggage) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;
	
	BookingFlightBaggage updateBookingFlightBaggage(BookingFlightBaggage bookingFlightBaggage) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException;
	
	List<ChosenFlightBaggage> searchChosenBaggage(ChosenBaggageSearch search) throws ItemParametersInvalid;
	
	Benefit insertBenefit(Benefit benefit) throws InvalidItemMessageException;
	
	Item findByItemId(Item item) throws ItemNotFoundException;
	
	BookingFlight updateChosenFare(BookingFlight bookingFlight) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException,ItemNotFoundException;
	
}
