package br.com.smiles.checkout.business;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.smiles.checkout.domain.HelloWorld;
import br.com.smiles.checkout.interceptor.BeanValidationInterceptor;

/**
 * Session Bean implementation class HelloWorldEJB
 */
@Stateless
@Local(HelloWorldBeanLocal.class)
@LocalBean
@Interceptors(BeanValidationInterceptor.class)
public class HelloWorldBean implements HelloWorldBeanLocal {
	
	private Logger logger = LoggerFactory.getLogger("AUDIT");

	/**
	 * Default constructor.
	 */
	public HelloWorldBean() {
	}

	public void sendHelloWorld(HelloWorld helloWorld) {
		
	}
	
	public String sayHelloWorld(@Valid HelloWorld helloWorld) {
		logger.info("Trilhando EJB...");
		return String.format("Say Hellow World (without cache), %s", helloWorld.getName());
	}

}
