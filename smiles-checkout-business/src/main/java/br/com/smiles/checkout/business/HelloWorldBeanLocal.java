package br.com.smiles.checkout.business;

import javax.validation.Valid;

import br.com.smiles.checkout.domain.HelloWorld;

public interface HelloWorldBeanLocal {

	void sendHelloWorld(HelloWorld helloWorld);

	String sayHelloWorld(@Valid HelloWorld helloWorld);
}
