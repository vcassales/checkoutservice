package br.com.smiles.checkout.business;

import java.util.List;

import br.com.smiles.checkout.entity.CheckoutOrder;
import br.com.smiles.checkout.exception.CheckoutOrderInvalidStatusMessageException;
import br.com.smiles.checkout.exception.ItemInvalidEventAdnReceiverMessageException;
import br.com.smiles.checkout.exception.ItemInvalidStatusMessageException;
import br.com.smiles.checkout.domain.CheckoutOrderSearch;

public interface CheckoutOrderBeanLocal {

	CheckoutOrder create(CheckoutOrder checkout);

	void update(CheckoutOrder checkoutOrder) throws CheckoutOrderInvalidStatusMessageException,ItemInvalidStatusMessageException,ItemInvalidEventAdnReceiverMessageException;

	CheckoutOrder getOrder(CheckoutOrder checkoutOrder);

	List<CheckoutOrder> search(CheckoutOrderSearch checkoutOrder);
	
}
