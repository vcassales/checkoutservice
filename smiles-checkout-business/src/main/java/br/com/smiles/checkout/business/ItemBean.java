package br.com.smiles.checkout.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.smiles.checkout.domain.ChosenBaggageSearch;
import br.com.smiles.checkout.domain.ChosenSeatSearch;
import br.com.smiles.checkout.domain.Status;
import br.com.smiles.checkout.entity.AvailableFare;
import br.com.smiles.checkout.entity.Benefit;
import br.com.smiles.checkout.entity.BookingFlight;
import br.com.smiles.checkout.entity.BookingFlightBaggage;
import br.com.smiles.checkout.entity.BookingFlightSeat;
import br.com.smiles.checkout.entity.CheckoutOrder;
import br.com.smiles.checkout.entity.ChosenFlightBaggage;
import br.com.smiles.checkout.entity.ChosenFlightSeat;
import br.com.smiles.checkout.entity.ChosenFlightSegment;
import br.com.smiles.checkout.entity.FamilyAccount;
import br.com.smiles.checkout.entity.Fee;
import br.com.smiles.checkout.entity.FeeAvailableFare;
import br.com.smiles.checkout.entity.FeeDetail;
import br.com.smiles.checkout.entity.FlightPassenger;
import br.com.smiles.checkout.entity.Item;
import br.com.smiles.checkout.entity.Membership;
import br.com.smiles.checkout.entity.Miles;
import br.com.smiles.checkout.entity.PaymentData;
import br.com.smiles.checkout.entity.Product;
import br.com.smiles.checkout.entity.RefundPayment;
import br.com.smiles.checkout.entity.SiebelTransaction;
import br.com.smiles.checkout.entity.Travel;
import br.com.smiles.checkout.entity.TravelInsurance;
import br.com.smiles.checkout.exception.InvalidItemMessageException;
import br.com.smiles.checkout.exception.ItemInvalidEventAdnReceiverMessageException;
import br.com.smiles.checkout.exception.ItemInvalidStatusMessageException;
import br.com.smiles.checkout.exception.ItemNotFoundException;
import br.com.smiles.checkout.exception.ItemParametersInvalid;
import br.com.smiles.checkout.interceptor.BeanValidationInterceptor;
import br.com.smiles.checkout.util.ConfigProperties;

/**
 * Session Bean implementation class HelloWorldEJB
 */
@Stateless
@Local(ItemBeanLocal.class)
@LocalBean
@Interceptors(BeanValidationInterceptor.class)
public class ItemBean implements ItemBeanLocal {

    private Logger logger = LoggerFactory.getLogger("AUDIT");

    @PersistenceContext
    protected EntityManager em;

    @EJB
    protected CheckoutOrderBeanLocal checkoutBean;

    @EJB
    protected ItemBeanLocal itemBeanLocal;

    /**
     * Default constructor.
     */
    public ItemBean() {
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item create(Item item) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("Creating an item...");
        if (item.getCheckoutOrder() == null || (item.getCheckoutOrder().getId() == null && (item.getCheckoutOrder().getMemberData() == null || item.getCheckoutOrder().getMemberData().getMemberNumber() == null || item.getCheckoutOrder().getMemberData().getMemberNumber().isEmpty()))) {
            throw new InvalidItemMessageException("ITEM_WITHOUT_RELATED_MEMBER", ConfigProperties.getProperty("smiles.item.Validation.withoutMember"));
        }
        logger.debug("1 - getId " + item.getId());
        // Cria uma nova order se necessario
        if (item.getCheckoutOrder().getId() == null) {
            item.getCheckoutOrder().setDate(new Date());
            CheckoutOrder checkoutOrder = item.getCheckoutOrder();
            logger.debug("2 - getId " + item.getId());
            CheckoutOrder checkoutOrderCreated = checkoutBean.create(checkoutOrder);
            logger.debug("3 - getId " + item.getId());
            item.setCheckoutOrder(checkoutOrderCreated);
        }
        logger.debug("4 - getId " + item.getId());
        // Define sempre a data da inclusao
        item.setDate(new Date());

        if (item instanceof Miles) {
            Miles miles = (Miles) item;
            if (miles.getEvent() != null && miles.getReceiver() != null) {
                throw new ItemInvalidEventAdnReceiverMessageException("ITEM_WITH_RECEIVER_AND_MILES", ConfigProperties.getProperty("smiles.helloworld.Validation.itemNotFound"));
            }
            miles.setStatus(Status.NEW.getStatus());
        }

        if (item.getId() != null) {

            Item persisteItem = em.find(Item.class, item.getId());

            persisteItem.setVoucherNumber(item.getVoucherNumber());

            if (persisteItem instanceof BookingFlight) {
                BookingFlight bookingFlight = (BookingFlight) persisteItem;
                BookingFlight bookingFlightRequest = (BookingFlight) item;

                if (bookingFlightRequest.getVoucherType() != null) {
                    bookingFlight.setVoucherType(bookingFlightRequest.getVoucherType());
                }

                if (bookingFlightRequest.getDiscount() != null) {
                    bookingFlight.setDiscount(bookingFlightRequest.getDiscount());
                }

                if (bookingFlightRequest.getDiscountMiles() != null) {
                    bookingFlight.setDiscountMiles(bookingFlightRequest.getDiscountMiles());
                }

                if (bookingFlightRequest.getChosenFlightSegment() != null) {
                    for (ChosenFlightSegment chosenFlightSegment : bookingFlight.getChosenFlightSegment()) {
                        chosenFlightSegment.setChosenTaxType(bookingFlightRequest.getChosenFlightSegment().get(0).getChosenTaxType());
                    }
                }

                if (bookingFlightRequest.getIsHasVoucher() != null) {
                    bookingFlight.setIsHasVoucher(bookingFlightRequest.getIsHasVoucher());
                }

                if (bookingFlightRequest.getTokenGds() != null) {
                    bookingFlight.setTokenGds(bookingFlightRequest.getTokenGds());
                }
            }

            if (persisteItem instanceof Fee) {
                Fee fee = (Fee) persisteItem;
                Fee feeRequest = (Fee) item;

                if (feeRequest.getMiles() != null) {
                    fee.setMiles(feeRequest.getMiles());
                }
                if (feeRequest.getMoney() != null) {
                    fee.setMoney(feeRequest.getMoney());
                }
                if (feeRequest.getStatus() != null) {
                    fee.setStatus(feeRequest.getStatus());
                }
                if (feeRequest.getBaseMiles() != null) {
                    fee.setBaseMiles(feeRequest.getBaseMiles());
                }

                if (feeRequest.getBaseMoney() != null) {
                    fee.setBaseMoney(feeRequest.getBaseMoney());
                }

                logger.debug("request " + feeRequest.getFeeAvailableFare());
                logger.debug("tamanho do persistido " + fee.getFeeAvailableFare().size());
                if (feeRequest.getFeeAvailableFare() != null) {
                    logger.debug("não nulo");
                    fee.setFeeAvailableFare(feeRequest.getFeeAvailableFare());
                } else {
                    logger.debug("nulo");
                }

                logger.debug("tamanho do persistido apos" + fee.getFeeAvailableFare().size());

            }

            if (persisteItem instanceof Product) {
                Product product = (Product) persisteItem;
                Product productRequest = (Product) item;

                if (productRequest.getSubStatus() != null) {
                    product.setSubStatus(productRequest.getSubStatus());
                }

                if (productRequest.getSendApprovalDate() != null) {
                    product.setSendApprovalDate(productRequest.getSendApprovalDate());
                }

                if (productRequest.getGift() != null) {
                    product.setGift(productRequest.getGift());
                }

                if (productRequest.getGiftDate() != null) {
                    product.setGiftDate(productRequest.getGiftDate());
                }

                if (productRequest.getCancelDate() != null) {
                    product.setCancelDate(productRequest.getCancelDate());
                }

                if (productRequest.getTransactionId() != null) {
                    product.setTransactionId(productRequest.getTransactionId());
                }

                if (productRequest.getReturnAnalysisDate() != null) {
                    product.setReturnAnalysisDate(productRequest.getReturnAnalysisDate());
                }
                if (productRequest.getBonusMiles() != null) {
                    product.setBonusMiles(productRequest.getBonusMiles());
                }
                if (productRequest.getMiles() != null) {
                    product.setMiles(productRequest.getMiles());
                }

                if (productRequest.getTravel() != null) {
                    List<Travel> listTravelRequest = new ArrayList<>();
                    for (Travel travelNew : productRequest.getTravel()) {
                        for (Travel travel : product.getTravel()) {
                            if (travel.getId().equals(travelNew.getId())) {

                                if (travelNew.getFlightPassenger() != null) {

                                    List<FlightPassenger> listPassengerRequest = new ArrayList<>();
                                    for (FlightPassenger passengerNew : travelNew.getFlightPassenger()) {
                                        for (FlightPassenger passenger : travel.getFlightPassenger()) {
                                            if (passenger.getId().equals(passengerNew.getId())) {

                                                TravelInsurance trvInsurance = new TravelInsurance();
                                                trvInsurance = passenger.getTravelInsurance();

                                                if (passengerNew.getTravelInsurance().getPolicy() != null) {
                                                    trvInsurance.setPolicy(passengerNew.getTravelInsurance().getPolicy());
                                                }
                                                if (passengerNew.getTravelInsurance().getLotteryNumber() != null) {
                                                    trvInsurance.setLotteryNumber(passengerNew.getTravelInsurance().getLotteryNumber());

                                                }
                                                if (passengerNew.getTravelInsurance().getTravellerId() != null) {
                                                    trvInsurance.setTravellerId(passengerNew.getTravelInsurance().getTravellerId());

                                                }
                                                if (passengerNew.getTravelInsurance().getOrderNumber() != null) {
                                                    trvInsurance.setOrderNumber(passengerNew.getTravelInsurance().getOrderNumber());

                                                }
                                                passenger.setTravelInsurance(trvInsurance);
                                                listPassengerRequest.add(passenger);
                                            }
                                        }
                                    }
                                    travel.setFlightPassenger(listPassengerRequest);
                                }
                                listTravelRequest.add(travel);
                            }
                        }

                    }

                    product.setTravel(listTravelRequest);
                }
            }

            em.merge(persisteItem);
        } else {
            em.persist(item);
        }

        return item;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item addBookingFlightSeat(BookingFlightSeat bookingFlightSeat) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("Creating an BookingFlightSeat...");

        if (!bookingFlightSeat.getStatus().equals(Status.NEW.getStatus()) && !bookingFlightSeat.getStatus().equals(Status.IN_PROGRESS.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        if (bookingFlightSeat.getCheckoutOrder() == null || (bookingFlightSeat.getCheckoutOrder().getId() == null && (bookingFlightSeat.getCheckoutOrder().getMemberData() == null || bookingFlightSeat.getCheckoutOrder().getMemberData().getMemberNumber() == null || bookingFlightSeat.getCheckoutOrder().getMemberData().getMemberNumber().isEmpty()))) {
            throw new InvalidItemMessageException("ITEM_WITHOUT_RELATED_MEMBER", ConfigProperties.getProperty("smiles.item.Validation.withoutMember"));
        }

        // Cria uma nova order se necessario
        if (bookingFlightSeat.getCheckoutOrder().getId() == null) {
            bookingFlightSeat.getCheckoutOrder().setDate(new Date());
            bookingFlightSeat.setCheckoutOrder(checkoutBean.create(bookingFlightSeat.getCheckoutOrder()));
        } else {
            List<Item> items = findByOrderId(bookingFlightSeat.getCheckoutOrder().getId());
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i) instanceof BookingFlightSeat) {
                    throw new InvalidItemMessageException("BOOKING_FLIGHT_SEAT_ALREADY_EXISTS", ConfigProperties.getProperty("smiles.item.Validation.bookingFlightSeatAlreadyExists"));
                }
            }
        }

        // Define sempre a data da inclusao
        bookingFlightSeat.setDate(new Date());

        em.persist(bookingFlightSeat);
        return bookingFlightSeat;
    }

    public ChosenFlightSeat findChosenFlightId(ChosenFlightSeat chosenFlightSeat) throws ItemNotFoundException {
        try {
            return (ChosenFlightSeat) em.createNamedQuery("findChosenFlightSeatId").setParameter("chosenId", chosenFlightSeat.getId()).getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            throw new ItemNotFoundException("ITEM_NOT_FOUND", ConfigProperties.getProperty("smiles.item.Validation.itemNotFound"));
        }
    }

    public SiebelTransaction findSiebelTransactionId(SiebelTransaction siebelTransaction) throws ItemNotFoundException {
        try {
            return (SiebelTransaction) em.createNamedQuery("findSiebelTransactionById").setParameter("siebelId", siebelTransaction.getId()).getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            throw new ItemNotFoundException("ITEM_NOT_FOUND", ConfigProperties.getProperty("smiles.item.Validation.itemNotFound"));
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item addChosenFlightSeat(ChosenFlightSeat chosenFlightSeat) throws ItemInvalidEventAdnReceiverMessageException, ItemInvalidStatusMessageException, ItemNotFoundException {
        logger.info("Creating an chosenFlightSeat...");
        verifyChosenStatus(this.findByItemId(chosenFlightSeat.getBookingFlightSeat()));
        em.persist(chosenFlightSeat);
        return this.findChosenFlightId(chosenFlightSeat).getBookingFlightSeat();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void remove(Item item) throws ItemInvalidStatusMessageException, ItemNotFoundException {
        logger.info("Deleting an item...");
        item = findById(item);
        verifyItemStatus(item);
        em.remove(item);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item removeChosenSeat(ChosenFlightSeat chosenFlightSeat) throws ItemInvalidStatusMessageException, ItemNotFoundException {
        logger.info("Deleting an item...");
        ChosenFlightSeat persistedItem = this.findChosenFlightId(chosenFlightSeat);
        verifyChosenStatus(persistedItem.getBookingFlightSeat());
        em.remove(persistedItem);
        return this.findById(persistedItem.getBookingFlightSeat());
    }

    public Item findById(Item item) throws ItemNotFoundException {
        try {
            return (Item) em.createNamedQuery("findById").setParameter("checkoutOrder", item.getCheckoutOrder().getId())
                    .setParameter("itemId", item.getId()).getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            throw new ItemNotFoundException("ITEM_NOT_FOUND", ConfigProperties.getProperty("smiles.item.Validation.itemNotFound"));
        }
    }

    public Item findByItemId(Item item) throws ItemNotFoundException {
        try {
            return em.find(Item.class, item.getId());
        } catch (javax.persistence.NoResultException e) {
            throw new ItemNotFoundException("ITEM_NOT_FOUND", ConfigProperties.getProperty("smiles.item.Validation.itemNotFound"));
        }
    }

    private List<Item> findByOrderId(Long orderId) {
        try {
            return em.createNamedQuery("findByOrderId").setParameter("checkoutOrder", orderId).getResultList();
        } catch (javax.persistence.NoResultException e) {
            return new ArrayList<>();
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void update(Item item) throws ItemInvalidStatusMessageException, ItemNotFoundException {
        Item persistedItem = this.findById(item);
        // avoid that order be replaced
        item.setCheckoutOrder(persistedItem.getCheckoutOrder());
        verifyItemStatus(persistedItem);
        em.merge(item);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item UpdateChosenSeat(ChosenFlightSeat chosenFlightSeat) throws ItemInvalidStatusMessageException, ItemNotFoundException {
        ChosenFlightSeat persistedItem = this.findChosenFlightId(chosenFlightSeat);
        verifyChosenStatus(persistedItem.getBookingFlightSeat());
        chosenFlightSeat.setBookingFlightSeat(persistedItem.getBookingFlightSeat());
        em.merge(chosenFlightSeat);
        return this.findChosenFlightId(chosenFlightSeat).getBookingFlightSeat();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateStatus(Item item) throws ItemInvalidStatusMessageException, ItemNotFoundException {
        Item persistedItem = this.findById(item);
        verifyItemStatus(persistedItem);

        if (item.getStatus() == null) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        persistedItem.setStatus(item.getStatus());
        persistedItem.setSubStatus(item.getSubStatus());
        em.merge(persistedItem);
    }

    private void verifyItemStatus(Item persistedItem) throws ItemInvalidStatusMessageException {
        if (!persistedItem.getStatus().equals(Status.NEW.getStatus()) && !persistedItem.getStatus().equals(Status.IN_PROGRESS.getStatus()) && !persistedItem.getStatus().equals(Status.PENDING_PAYMENT.getStatus()) && !persistedItem.getStatus().equals(Status.PROCESSED.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }
    }

    private void verifyChosenStatus(Item persistedItem) throws ItemInvalidStatusMessageException {
        if (!Status.NEW.getStatus().equals(persistedItem.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<ChosenFlightSeat> SearchChosenSeat(ChosenSeatSearch search) throws ItemParametersInvalid {
        if ((search.getMemberNumber() == null || search.getMemberNumber().isEmpty()) && (search.getOrderid() == null)) {
            throw new ItemParametersInvalid("Item wrong number parameters", ConfigProperties.getProperty("smiles.item.Validation.numberParameters"));
        }

        final List<ChosenFlightSeat> ordersChosen = new ArrayList<ChosenFlightSeat>();
        final StringBuilder findChosenFlightSeatStatus = new StringBuilder("select o FROM ChosenFlightSeat o WHERE ");
        boolean andAppend = false;

        if (search.getMemberNumber() != null) {
            findChosenFlightSeatStatus.append(" o.bookingFlightSeat.checkoutOrder.memberData.memberNumber = :memberNumber");
            andAppend = true;
        }

        if (search.getStatus() != null) {
            if (andAppend) {
                findChosenFlightSeatStatus.append(" AND ");
            } else {
                andAppend = true;
            }
            findChosenFlightSeatStatus.append(" o.bookingFlightSeat.checkoutOrder.status in :orderStatus");
        }
        if (search.getOrderid() != null) {
            if (andAppend) {
                findChosenFlightSeatStatus.append(" AND ");
            } else {
                andAppend = true;
            }
            findChosenFlightSeatStatus.append(" o.bookingFlightSeat.checkoutOrder.id = :orderId");
        }
        if (search.getTokenGds() != null) {
            if (andAppend) {
                findChosenFlightSeatStatus.append(" AND ");
            } else {
                andAppend = true;
            }
            findChosenFlightSeatStatus.append(" o.bookingFlightSeat.tokenGds = :tokenGds");

        }
        if (search.getRecordLocator() != null) {
            if (andAppend) {
                findChosenFlightSeatStatus.append(" AND ");
            }

            findChosenFlightSeatStatus.append(" o.recordLocator = :recordLocator");
        }

        final Query query = em.createQuery(findChosenFlightSeatStatus.toString());
        query.setHint(QueryHints.BIND_PARAMETERS, HintValues.TRUE);
        if (search.getMemberNumber() != null) {
            query.setParameter("memberNumber", search.getMemberNumber());
        }
        if (search.getStatus() != null) {
            query.setParameter("orderStatus", search.getStatus());
        }
        if (search.getOrderid() != null) {
            query.setParameter("orderId", search.getOrderid());
        }
        if (search.getTokenGds() != null) {
            query.setParameter("tokenGds", search.getTokenGds());
        }
        if (search.getRecordLocator() != null) {
            query.setParameter("recordLocator", search.getRecordLocator());
        }
        ordersChosen.addAll(query.getResultList());

        return ordersChosen;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public PaymentData addRefundPayment(RefundPayment refundPayment) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("Creating an refundPayment...");

        PaymentData payment = em.find(PaymentData.class, refundPayment.getPaymentData().getId());
        Double refundTotals = (double) 0;
        for (RefundPayment refundItem : payment.getRefundPayment()) {
            refundTotals = refundTotals + refundItem.getAmount();
        }
        if (Double.parseDouble(payment.getAmount()) < refundTotals + refundPayment.getAmount()) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        em.persist(refundPayment);
        return refundPayment.getPaymentData();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SiebelTransaction addSiebelTransaction(SiebelTransaction siebelTransaction) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("Creating an refundPayment...");
        em.persist(siebelTransaction);
        return siebelTransaction;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SiebelTransaction cancelSiebelTransaction(SiebelTransaction siebelTransaction) throws ItemInvalidStatusMessageException, ItemNotFoundException {
        logger.info("cancel an item...");
        siebelTransaction = em.find(SiebelTransaction.class, siebelTransaction.getId());
        siebelTransaction.setCancelled(true);
        em.merge(siebelTransaction);
        return siebelTransaction;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item addPaymentData(PaymentData paymentData) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException, ItemNotFoundException {
        logger.info("Creating an paymentData...");

        Long paymentDataId = null;

        if (paymentData.getItem() != null && paymentData.getItem().getId() != null) {
            Item item = em.find(Item.class, paymentData.getItem().getId());
            if (item.getPaymentData() != null && item.getPaymentData().getId() != null) {
                paymentDataId = item.getPaymentData().getId();
            }
        } else if (paymentData.getChosenFlightSegment() != null && paymentData.getChosenFlightSegment().getId() != null) {
            ChosenFlightSegment chosenFlightSegment = em.find(ChosenFlightSegment.class, paymentData.getChosenFlightSegment().getId());
            if (chosenFlightSegment.getPaymentData() != null && chosenFlightSegment.getPaymentData().getId() != null) {
                paymentDataId = chosenFlightSegment.getPaymentData().getId();
            }
        } else if (paymentData.getFeeDetail() != null && paymentData.getFeeDetail().getId() != null) {
            FeeDetail feeDetail = em.find(FeeDetail.class, paymentData.getFeeDetail().getId());
            if (feeDetail.getPaymentData() != null && feeDetail.getPaymentData().getId() != null) {
                paymentDataId = feeDetail.getPaymentData().getId();
            }
        } else {
            throw new InvalidItemMessageException("PAYMENTDATA_NO_RELATION_WITH_ITEM", ConfigProperties.getProperty("smiles.item.Validation.paymentDataNoRelationWithItem"));
        }

        //Adicionando dados de pagamento.
        if (paymentDataId != null) {
            paymentData.setId(paymentDataId);
            em.merge(paymentData);
        } else {
            em.persist(paymentData);
        }

        return paymentData.getItem();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public BookingFlight addChosenFlightSegment(ChosenFlightSegment chosenFlightSegment) throws InvalidItemMessageException {
        logger.info("Creating an chosenFlightSegment...");

        if (chosenFlightSegment.getChosenFlightSegmentOlder() == null || chosenFlightSegment.getChosenFlightSegmentOlder().getId() == null) {
            throw new InvalidItemMessageException("ITEM_WITHOUT_RELATED_MEMBER", ConfigProperties.getProperty("smiles.item.Validation.withoutMember"));
        }

        em.persist(chosenFlightSegment);

        return chosenFlightSegment.getBookingFlight();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item addBookingFlight(BookingFlight bookingFlight) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("Creating an BookingFlightSeat...");

        if (!bookingFlight.getStatus().equals(Status.NEW.getStatus()) && !bookingFlight.getStatus().equals(Status.IN_PROGRESS.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        if (bookingFlight.getCheckoutOrder() == null || (bookingFlight.getCheckoutOrder().getId() == null && (bookingFlight.getCheckoutOrder().getMemberData() == null || bookingFlight.getCheckoutOrder().getMemberData().getMemberNumber() == null || bookingFlight.getCheckoutOrder().getMemberData().getMemberNumber().isEmpty()))) {
            throw new InvalidItemMessageException("ITEM_WITHOUT_RELATED_MEMBER", ConfigProperties.getProperty("smiles.item.Validation.withoutMember"));
        }

        // Cria uma nova order se necessario
        if (bookingFlight.getCheckoutOrder().getId() == null) {
            bookingFlight.getCheckoutOrder().setDate(new Date());
            bookingFlight.setCheckoutOrder(checkoutBean.create(bookingFlight.getCheckoutOrder()));
        } else {
            List<Item> items = findByOrderId(bookingFlight.getCheckoutOrder().getId());
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i) instanceof BookingFlight) {
                    throw new InvalidItemMessageException("BOOKING_FLIGHT_ALREADY_EXISTS", ConfigProperties.getProperty("smiles.item.Validation.bookingFlightAlreadyExists"));
                }
            }
        }

        // Define sempre a data da inclusao
        bookingFlight.setDate(new Date());

        em.persist(bookingFlight);
        return bookingFlight;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public BookingFlight updateBookingFlight(BookingFlight bookingFlight) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("update an bookingFlight...");

        BookingFlight persistedItem = em.find(BookingFlight.class, bookingFlight.getId());

        if (!persistedItem.getCheckoutOrder().getStatus().equals(Status.NEW.getStatus()) && !persistedItem.getCheckoutOrder().getStatus().equals(Status.IN_PROGRESS.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        bookingFlight.setDate(new Date());
        bookingFlight.setCheckoutOrder(persistedItem.getCheckoutOrder());

        em.merge(bookingFlight);
        return bookingFlight;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ChosenFlightSegment> addFlightPassengers(List<FlightPassenger> flightPassengerList) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("Creating an addFlightPassengers...");

        for (FlightPassenger flightPassenger : flightPassengerList) {
            FlightPassenger flightPassengerItem = em.find(FlightPassenger.class, flightPassenger.getId());
            flightPassenger.setChosenFlightSegment(flightPassengerItem.getChosenFlightSegment());
            em.merge(flightPassenger);
        }
        FlightPassenger flightPassenger = em.find(FlightPassenger.class, flightPassengerList.get(0).getId());
        BookingFlight bookingFlight = em.find(BookingFlight.class, flightPassenger.getChosenFlightSegment().getBookingFlight().getId());

        return bookingFlight.getChosenFlightSegment();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void deletePaymentData(PaymentData paymentData) throws ItemInvalidStatusMessageException, InvalidItemMessageException, ItemNotFoundException {
        if (paymentData != null && paymentData.getId() != null) {
            PaymentData paymentPersisted = em.find(PaymentData.class, paymentData.getId());
            if (paymentPersisted != null && paymentPersisted.getItem() != null) {
                if ("PROCESSED".equals(paymentPersisted.getItem().getStatus())) {
                    throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
                }
                // Remove all installments
                for (int i = 0; i < paymentPersisted.getInstallmentAmount().size(); i++) {
                    em.remove(paymentPersisted.getInstallmentAmount().get(i));
                }
                // Remove the payment data
                em.remove(paymentPersisted);
            }
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item updateMembership(Membership membership) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException, ItemNotFoundException {
        logger.info("update an Membership...");
        Membership persistedItem = em.find(Membership.class, membership.getId());

        if (membership.getPlanId() != null) {
            persistedItem.setPlanId(membership.getPlanId());
        }
        if (membership.getExpirationDay() != null) {
            persistedItem.setExpirationDay(membership.getExpirationDay());
        }
        if (membership.getReferralMemberNumber() != null) {
            persistedItem.setReferralMemberNumber(membership.getReferralMemberNumber());
        }
        if (membership.getIndicationCode() != null) {
            persistedItem.setIndicationCode(membership.getIndicationCode());
        }
        if (membership.getIsRegularization() != null) {
            persistedItem.setIsRegularization(membership.getIsRegularization());
        }
        if (membership.getSignatureCode() != null) {
            persistedItem.setSignatureCode(membership.getSignatureCode());
        }
        if (membership.getMoney() != null) {
            persistedItem.setMoney(membership.getMoney());
        }
        if (membership.getMilesQuantity() != null) {
            persistedItem.setMilesQuantity(membership.getMilesQuantity());
        }
        if (membership.getIsSameCard() != null) {
            persistedItem.setIsSameCard(membership.getIsSameCard());
        }
        if (membership.getSaveCard() != null) {
            persistedItem.setSaveCard(membership.getSaveCard());
        }
        if (membership.getQualifyingMilesQuantity() != null) {
            persistedItem.setQualifyingMilesQuantity(membership.getQualifyingMilesQuantity());
        }
        if (membership.getType() != null) {
            persistedItem.setType(membership.getType());
        }
        if (membership.getSubType() != null) {
            persistedItem.setSubType(membership.getSubType());
        }
        if (membership.getFullCostMoney() != null) {
            persistedItem.setFullCostMoney(membership.getFullCostMoney());
        }
        if (membership.getValidity() != null) {
            persistedItem.setValidity(membership.getValidity());
        }
        if (membership.getGracePeriod() != null) {
            persistedItem.setGracePeriod(membership.getGracePeriod());
        }
        if (membership.getInstallmentOptions() != null) {
            persistedItem.setInstallmentOptions(membership.getInstallmentOptions());
        }
        if (membership.getAutomaticRenewal() != null) {
            persistedItem.setAutomaticRenewal(membership.getAutomaticRenewal());
        }
        if (membership.getIsFraud() != null) {
            persistedItem.setIsFraud(membership.getIsFraud());
        }
        if (membership.getRevertMiles() != null) {
            persistedItem.setRevertMiles(membership.getRevertMiles());
        }
        if (membership.getImmediateCancellation() != null) {
            persistedItem.setImmediateCancellation(membership.getImmediateCancellation());
        }
        if (membership.getRefundMoney() != null) {
            persistedItem.setRefundMoney(membership.getRefundMoney());
        }

        em.merge(persistedItem);

        return em.find(Item.class, persistedItem.getId());
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item updateFamilyAccount(FamilyAccount familyAccount) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException, ItemNotFoundException {
        logger.info("update an FamilyAccount...");
        FamilyAccount persistedItem = em.find(FamilyAccount.class, familyAccount.getId());

        if (familyAccount.getFamilyAccountId() != null) {
            persistedItem.setFamilyAccountId(familyAccount.getFamilyAccountId());
        }
        if (familyAccount.getIntegrantType() != null) {
            persistedItem.setIntegrantType(familyAccount.getIntegrantType());
        }

        if (familyAccount.getInvitationCode() != null) {
            persistedItem.setInvitationCode(familyAccount.getInvitationCode());
        }

        if (familyAccount.getInvitationResponse() != null) {
            persistedItem.setInvitationResponse(familyAccount.getInvitationResponse());
        }

        if (familyAccount.getEligibilityId() != null) {
            persistedItem.setEligibilityId(familyAccount.getEligibilityId());
        }

        if (familyAccount.getMoney() != null) {
            persistedItem.setMoney(familyAccount.getMoney());
        }

        em.merge(persistedItem);

        return em.find(Item.class, persistedItem.getId());
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Item addBookingFlightBaggage(BookingFlightBaggage bookingFlightBaggage) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException {
        logger.info("Creating an BookingFlightSeat...");

        if (!bookingFlightBaggage.getStatus().equals(Status.NEW.getStatus()) && !bookingFlightBaggage.getStatus().equals(Status.IN_PROGRESS.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        if (bookingFlightBaggage.getCheckoutOrder() == null || (bookingFlightBaggage.getCheckoutOrder().getId() == null && (bookingFlightBaggage.getCheckoutOrder().getMemberData() == null || bookingFlightBaggage.getCheckoutOrder().getMemberData().getMemberNumber() == null || bookingFlightBaggage.getCheckoutOrder().getMemberData().getMemberNumber().isEmpty()))) {
            throw new InvalidItemMessageException("ITEM_WITHOUT_RELATED_MEMBER", ConfigProperties.getProperty("smiles.item.Validation.withoutMember"));
        }

        // Cria uma nova order se necessario
        if (bookingFlightBaggage.getCheckoutOrder().getId() == null) {
            bookingFlightBaggage.getCheckoutOrder().setDate(new Date());
            bookingFlightBaggage.setCheckoutOrder(checkoutBean.create(bookingFlightBaggage.getCheckoutOrder()));
        } else {
            List<Item> items = findByOrderId(bookingFlightBaggage.getCheckoutOrder().getId());
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i) instanceof BookingFlightBaggage) {
                    throw new InvalidItemMessageException("BOOKING_FLIGHT_BAGGAGE_ALREADY_EXISTS", ConfigProperties.getProperty("bookingFlightBaggageAlreadyExists"));
                }
            }
        }

        // Define sempre a data da inclusao
        bookingFlightBaggage.setDate(new Date());

        em.persist(bookingFlightBaggage);

        return bookingFlightBaggage;
    }

    @Override
    public BookingFlightBaggage updateBookingFlightBaggage(BookingFlightBaggage bookingFlightBaggage)
            throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException,
            InvalidItemMessageException {
        logger.info("update an bookingFlightBaggage...");

        BookingFlightBaggage persistedItem = em.find(BookingFlightBaggage.class, bookingFlightBaggage.getId());

        if (!persistedItem.getCheckoutOrder().getStatus().equals(Status.NEW.getStatus()) && !persistedItem.getCheckoutOrder().getStatus().equals(Status.IN_PROGRESS.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        bookingFlightBaggage.setDate(new Date());
        bookingFlightBaggage.setCheckoutOrder(persistedItem.getCheckoutOrder());

        em.merge(bookingFlightBaggage);
        return bookingFlightBaggage;
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<ChosenFlightBaggage> searchChosenBaggage(ChosenBaggageSearch search) throws ItemParametersInvalid {
        if ((search.getMemberNumber() == null || search.getMemberNumber().isEmpty()) && (search.getOrderid() == null)) {
            throw new ItemParametersInvalid("Item wrong number parameters", ConfigProperties.getProperty("smiles.item.Validation.numberParameters"));
        }

        List<ChosenFlightBaggage> ordersChosen = new ArrayList<ChosenFlightBaggage>();
        if (search.getStatus() != null) {
            ordersChosen.addAll(em.createNamedQuery("findChosenFlightBaggageStatus").setParameter("orderStatus", search.getStatus())
                    .setParameter("memberNumber", search.getMemberNumber()).setParameter("orderId", search.getOrderid())
                    .setParameter("tokenGds", search.getTokenGds()).setParameter("recordLocator", search.getRecordLocator()).getResultList());

        } else {
            ordersChosen.addAll(em.createNamedQuery("findChosenFlightBaggage").setParameter("orderStatus", search.getStatus())
                    .setParameter("memberNumber", search.getMemberNumber()).setParameter("orderId", search.getOrderid())
                    .setParameter("tokenGds", search.getTokenGds()).setParameter("recordLocator", search.getRecordLocator()).getResultList());
        }
        return ordersChosen;
    }

    @SuppressWarnings("unchecked")
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Benefit insertBenefit(Benefit benefit) throws InvalidItemMessageException {

        Long benefitId = null;
        if (benefit.getItem() != null && benefit.getItem().getId() != null) {
            Item itemCurrent = em.find(Item.class, benefit.getItem().getId());
            if (itemCurrent != null) {
                if (itemCurrent.getBenefit() != null && itemCurrent.getBenefit().getId() != null) {
                    benefitId = itemCurrent.getBenefit().getId();
                }
            } else {
                throw new InvalidItemMessageException("ITEM_NOT_FOUND",
                        ConfigProperties.getProperty("smiles.item.Validation.itemNotFound"));
            }
        } else if (benefit.getCheckoutOrder() != null && benefit.getCheckoutOrder().getId() != null) {
            CheckoutOrder orderCurrent = em.find(CheckoutOrder.class, benefit.getCheckoutOrder().getId());
            if (orderCurrent != null) {
                if (orderCurrent.getBenefit() != null && orderCurrent.getBenefit().getId() != null) {
                    benefitId = orderCurrent.getBenefit().getId();
                }
            } else {
                throw new InvalidItemMessageException("ORDER_NOT_FOUND",
                        ConfigProperties.getProperty("smiles.item.Validation.orderNotFound"));
            }
        } else {
            throw new InvalidItemMessageException("BENEFIT_NO_RELATION_WITH_ITEM",
                    ConfigProperties.getProperty("smiles.item.Validation.benefitNoRelationWithItem"));
        }

        if (benefitId != null) {
            benefit.setId(benefitId);
            em.merge(benefit);
        } else {
            em.persist(benefit);
        }
        return benefit;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public BookingFlight updateChosenFare(BookingFlight bookingFlight) throws ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException, InvalidItemMessageException, ItemNotFoundException {
        logger.info("update an chosenFare...");

        BookingFlight persistedItem = em.find(BookingFlight.class, bookingFlight.getId());

        if (!persistedItem.getCheckoutOrder().getStatus().equals(Status.NEW.getStatus())) {
            throw new ItemInvalidStatusMessageException("Item wrong status", ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
        }

        if (bookingFlight.getChosenFlightSegment() != null) {

            List<ChosenFlightSegment> listFlightSegment = new ArrayList<>();
            for (ChosenFlightSegment persistedSegment : persistedItem.getChosenFlightSegment()) {

                for (ChosenFlightSegment segment : bookingFlight.getChosenFlightSegment()) {

                    if (segment.getType().equals(persistedSegment.getType())) {

                        if (segment.getChosenFareCacheId() != null) {
                            String chosenFareCacheId = segment.getChosenFareCacheId();
                            AvailableFare chosenfare = new AvailableFare();
                            for (AvailableFare fare : persistedSegment.getAvailableFare()) {
                                if (fare.getCacheId().equals(chosenFareCacheId)) {
                                    chosenfare = fare;
                                    break;
                                }
                            }

                            if (chosenfare.getCacheId() != null) {
                                persistedSegment.setChosenFareCacheId(chosenfare.getCacheId());

                                if (chosenfare.getType() != null) {
                                    persistedSegment.setChosenFareType(chosenfare.getType());
                                } else {
                                    persistedSegment.setChosenFareType(null);
                                }

                                if (chosenfare.getMiles() != null) {
                                    persistedSegment.setChosenFareMiles(chosenfare.getMiles());
                                } else {
                                    persistedSegment.setChosenFareMiles(null);
                                }

                                if (chosenfare.getMoney() != null) {
                                    persistedSegment.setChosenFareMoney(chosenfare.getMoney());
                                } else {
                                    persistedSegment.setChosenFareMoney(null);
                                }

                                if (chosenfare.getHighlightText() != null) {
                                    persistedSegment.setChosenFareHighlightText(chosenfare.getHighlightText());
                                } else {
                                    persistedSegment.setChosenFareHighlightText(null);
                                }

                                if (chosenfare.getBaseMiles() != null) {
                                    persistedSegment.setChosenFareBaseMiles(chosenfare.getBaseMiles());
                                } else {
                                    persistedSegment.setChosenFareBaseMiles(null);
                                }

                                if (chosenfare.getAirlineFareAmount() != null) {
                                    persistedSegment.setAirlineFareAmount(chosenfare.getAirlineFareAmount());
                                } else {
                                    persistedSegment.setAirlineFareAmount(null);
                                }

                                if (chosenfare.getAirlineFare() != null) {
                                    persistedSegment.setAirlineFare(chosenfare.getAirlineFare());
                                } else {
                                    persistedSegment.setAirlineFare(null);
                                }

                                if (chosenfare.getSellKey() != null) {
                                    persistedSegment.setFareSellKey(chosenfare.getSellKey());
                                } else {
                                    persistedSegment.setFareSellKey(null);
                                }

                                if (chosenfare.getFamily() != null) {
                                    persistedSegment.setFareFamily(chosenfare.getFamily());
                                } else {
                                    persistedSegment.setFareFamily(null);
                                }

                                if (chosenfare.getDescription() != null) {
                                    persistedSegment.setFareDescription(chosenfare.getDescription());
                                } else {
                                    persistedSegment.setFareDescription(null);
                                }

                                if (chosenfare.getBasics() != null) {
                                    persistedSegment.setFareBasics(chosenfare.getBasics());
                                } else {
                                    persistedSegment.setFareBasics(null);
                                }

                                if (chosenfare.getBookingClass() != null) {
                                    persistedSegment.setBookingClass(chosenfare.getBookingClass());
                                } else {
                                    persistedSegment.setBookingClass(null);
                                }

                                if (chosenfare.getYieldSM() != null) {
                                    persistedSegment.setYieldSM(chosenfare.getYieldSM());
                                } else {
                                    persistedSegment.setYieldSM(null);
                                }

                                if (chosenfare.getYield() != null) {
                                    persistedSegment.setYield(chosenfare.getYield());
                                } else {
                                    persistedSegment.setYield(null);
                                }

                                if (chosenfare.getLoadFactor() != null) {
                                    persistedSegment.setLoadFactor(chosenfare.getLoadFactor());
                                } else {
                                    persistedSegment.setLoadFactor(null);
                                }

                                if (chosenfare.getChoosedOffer() != null) {
                                    persistedSegment.setOpcaoSM(chosenfare.getChoosedOffer());
                                } else {
                                    persistedSegment.setOpcaoSM(null);
                                }

                                if (chosenfare.getPublicValue() != null) {
                                    persistedSegment.setPublishedValue(chosenfare.getPublicValue());
                                } else {
                                    persistedSegment.setPublishedValue(null);
                                }

                                if (chosenfare.getLegListCost() != null) {
                                    persistedSegment.setLegListCost(chosenfare.getLegListCost());
                                } else {
                                    persistedSegment.setLegListCost(null);
                                }

                                if (chosenfare.getLegListCurrency() != null) {
                                    persistedSegment.setLegListCurrency(chosenfare.getLegListCurrency());
                                } else {
                                    persistedSegment.setLegListCurrency(null);
                                }

                                if (chosenfare.getNegotiatedFareCode() != null) {
                                    persistedSegment.setNegotiatedFareCode(chosenfare.getNegotiatedFareCode());
                                } else {
                                    persistedSegment.setNegotiatedFareCode(null);
                                }

                                if (chosenfare.getFareInfo() != null) {
                                    persistedSegment.setChosenFareInfo(chosenfare.getFareInfo());
                                } else {
                                    persistedSegment.setChosenFareInfo(null);
                                }

                                break;
                            }
                        }
                    }
                }
                listFlightSegment.add(persistedSegment);
            }
            persistedItem.setChosenFlightSegment(listFlightSegment);
            persistedItem.setDate(new Date());
            em.merge(persistedItem);
        }

        return em.find(BookingFlight.class, persistedItem.getId());
    }

}
