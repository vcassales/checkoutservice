package br.com.smiles.checkout.business;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.smiles.checkout.domain.CheckoutOrderSearch;
import br.com.smiles.checkout.domain.Status;
import br.com.smiles.checkout.domain.Total;
import br.com.smiles.checkout.entity.Baggage;
import br.com.smiles.checkout.entity.BookingFlight;
import br.com.smiles.checkout.entity.BookingFlightBaggage;
import br.com.smiles.checkout.entity.BookingFlightSeat;
import br.com.smiles.checkout.entity.CheckoutOrder;
import br.com.smiles.checkout.entity.ChosenFlightBaggage;
import br.com.smiles.checkout.entity.ChosenFlightSeat;
import br.com.smiles.checkout.entity.ChosenFlightSegment;
import br.com.smiles.checkout.entity.FamilyAccount;
import br.com.smiles.checkout.entity.Fee;
import br.com.smiles.checkout.entity.FlightPassenger;
import br.com.smiles.checkout.entity.Item;
import br.com.smiles.checkout.entity.Membership;
import br.com.smiles.checkout.entity.Miles;
import br.com.smiles.checkout.entity.Product;
import br.com.smiles.checkout.entity.SiebelTransaction;
import br.com.smiles.checkout.entity.Travel;
import br.com.smiles.checkout.exception.CheckoutOrderInvalidStatusMessageException;
import br.com.smiles.checkout.exception.ItemInvalidEventAdnReceiverMessageException;
import br.com.smiles.checkout.exception.ItemInvalidStatusMessageException;
import br.com.smiles.checkout.interceptor.BeanValidationInterceptor;
import br.com.smiles.checkout.util.ConfigProperties;
import br.com.smiles.checkout.util.DoubleUtil;
import br.com.smiles.checkout.util.IntegerUtil;

/**
 * Session Bean implementation class HelloWorldEJB
 */
@Stateless
@Local(CheckoutOrderBeanLocal.class)
@LocalBean
@Interceptors(BeanValidationInterceptor.class)
public class CheckoutOrderBean implements CheckoutOrderBeanLocal {

	private Logger logger = LoggerFactory.getLogger("AUDIT");

	@PersistenceContext
	protected EntityManager em;

	@EJB
	protected ItemBeanLocal itemBean;

	/**
	 * Default constructor.
	 */
	public CheckoutOrderBean() {
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public CheckoutOrder create(CheckoutOrder checkout) {
		logger.info("Creating an order...");
		checkout.setStatus(Status.NEW.getStatus());
		em.persist(checkout);
		calculateTotalCostCheckout(checkout);
		return checkout;
	}

	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public CheckoutOrder getOrder(CheckoutOrder checkoutOrder) {
		CheckoutOrder checkout = em.find(CheckoutOrder.class, checkoutOrder.getId());
		calculateTotalCostCheckout(checkout);
		return checkout;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void update(CheckoutOrder checkoutOrder) throws CheckoutOrderInvalidStatusMessageException,
			ItemInvalidStatusMessageException, ItemInvalidEventAdnReceiverMessageException {
		logger.info("Updating an order...");
		CheckoutOrder orderStatus = this.getOrder(checkoutOrder);
		if (!orderStatus.getStatus().equals(Status.NEW.getStatus())
				&& !orderStatus.getStatus().equals(Status.IN_PROGRESS.getStatus())
				&& !orderStatus.getStatus().equals(Status.PROCESSED.getStatus())) {
			throw new CheckoutOrderInvalidStatusMessageException("CHECKOUTORDER_INVALID_STATUS",
					ConfigProperties.getProperty("smiles.checkoutorder.Validation.checkoutorderInvalidStatus"));
		} else if (checkoutOrder.getItems() != null) {
			List<Item> listItensRequest = new ArrayList<>();
			for (Item itemNew : checkoutOrder.getItems()) {

				for (Item item : orderStatus.getItems()) {

					if (item.getId().equals(itemNew.getId())) {

						if (!item.getStatus().equals(Status.NEW.getStatus())
								&& !item.getStatus().equals(Status.IN_PROGRESS.getStatus())
								&& !item.getStatus().equals(Status.PROCESSED.getStatus())
								&& !item.getStatus().equals(Status.PENDING_PAYMENT.getStatus())
								&& !item.getStatus().equals(Status.PENDING_APPROVAL.getStatus())) {
							throw new ItemInvalidStatusMessageException("ITEM_INVALID_STATUS",
									ConfigProperties.getProperty("smiles.item.Validation.itemInvalidStatus"));
						} else {
							if (itemNew.getStatus() != null) {
								item.setStatus(itemNew.getStatus());
							}

							if (itemNew.getSubStatus() != null) {
								item.setSubStatus(itemNew.getSubStatus());
							}

							if (itemNew.getMaxInstallments() != null) {
								item.setMaxInstallments(itemNew.getMaxInstallments());
							}

							if (itemNew.getPaymentData() != null) {
								item.setPaymentData(itemNew.getPaymentData());
							}

							// Apenas para suportar o processo de compra de
							// milhas atual, pois o correto é ter uma operação
							// no
							// Webservice exclusivamente para associar
							// transações Siebel a um item
							if ((itemNew instanceof Miles)) {
								Miles milesNew = (Miles) itemNew;
								Miles miles = (Miles) item;
								if (milesNew.getSiebelTransaction() != null) {
									if (miles.getSiebelTransaction() == null) {
										miles.setSiebelTransaction(milesNew.getSiebelTransaction());
									} else {
										List<SiebelTransaction> siebelList = new ArrayList<>();
										for (SiebelTransaction transactionNew : milesNew.getSiebelTransaction()) {
											String ok = "";
											for (SiebelTransaction transaction : miles.getSiebelTransaction()) {
												if ((transaction.getTransactionId() != null)
														&& (transaction.getTransactionId()
																.equals(transactionNew.getTransactionId()))) {
													ok = "ok";
													break;
												} else {
													ok = "";
												}
											}

											if (!ok.equals("ok")) {
												siebelList.add(transactionNew);
											}
										}

										miles.setSiebelTransaction(siebelList);
									}
								}
							}
						}

						listItensRequest.add(item);
						break;
					}
				}
			}

			orderStatus.setItems(listItensRequest);
		}

		if (checkoutOrder.getDate() != null) {
			orderStatus.setDate(checkoutOrder.getDate());
		}

		if (checkoutOrder.getStatus() != null) {
			orderStatus.setStatus(checkoutOrder.getStatus());
		}

		if (checkoutOrder.getSubStatus() != null) {
			orderStatus.setSubStatus(checkoutOrder.getSubStatus());
		}
		
		if (checkoutOrder.getPromotionalChannel() != null) {
			orderStatus.setPromotionalChannel(checkoutOrder.getPromotionalChannel());
		}
		
		if (checkoutOrder.getPartnerAlias() != null) {
			orderStatus.setPartnerAlias(checkoutOrder.getPartnerAlias());
		}

		if (checkoutOrder.getMemberData() != null && checkoutOrder.getMemberData().getCitizenship() != null) {
			orderStatus.getMemberData().setCitizenship(checkoutOrder.getMemberData().getCitizenship());
		}

		if (checkoutOrder.getMemberData() != null && checkoutOrder.getMemberData().getCountry() != null) {
			orderStatus.getMemberData().setCountry(checkoutOrder.getMemberData().getCountry());
		}

		if (checkoutOrder.getMemberData() != null && checkoutOrder.getMemberData().getSmilesClub() != null) {
			orderStatus.getMemberData().setSmilesClub(checkoutOrder.getMemberData().getSmilesClub());
		}

		if (checkoutOrder.getMemberData() != null && checkoutOrder.getMemberData().getTier() != null) {
			orderStatus.getMemberData().setTier(checkoutOrder.getMemberData().getTier());
		}
		
		if (checkoutOrder.getMemberData() != null && checkoutOrder.getMemberData().getDocumentNumber() != null) {
			orderStatus.getMemberData().setDocumentNumber(checkoutOrder.getMemberData().getDocumentNumber());
		}
		
		if (checkoutOrder.getAvailableSmilesClubPlanId() != null) {
			orderStatus.setAvailableSmilesClubPlanId(checkoutOrder.getAvailableSmilesClubPlanId());
		}

		em.merge(orderStatus);

	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<CheckoutOrder> search(CheckoutOrderSearch search) {
		List<CheckoutOrder> ordersFound = new ArrayList<CheckoutOrder>();
		if (search.getId() != null) {
			CheckoutOrder order = new CheckoutOrder();
			order.setId(search.getId());
			order = this.getOrder(order);
			if (order!=null && order.getId()!=null){
				ordersFound.add(order);
			}
		} 
		else if (search.getProductType() != null && search.getProductType().length()>0) {
			if (search.getStatus()!=null && search.getStatus().size()>0) {
			ordersFound.addAll(em.createNamedQuery("findOrderProductTypeStatus")
					.setParameter("statusList", search.getStatus())
					.setParameter("dateIni", search.getDateInit())
					.setParameter("dateEnd", search.getDateEnd())
					.setParameter("memberID", search.getMemberId())
					.setParameter("productType", search.getProductType())
					.setParameter("cancellationItem", search.getCancellationItem())
					.getResultList());
			
			}else{	
			ordersFound.addAll(em.createNamedQuery("findOrderProductType")
					.setParameter("dateIni", search.getDateInit())
					.setParameter("dateEnd", search.getDateEnd())
					.setParameter("memberID", search.getMemberId())
					.setParameter("productType", search.getProductType())
					.setParameter("cancellationItem", search.getCancellationItem())
					.getResultList());
			}
		} else if (search.getSiebelTransactionId() != null && search.getSiebelTransactionId().length()>0) {
			ordersFound.addAll(em.createNamedQuery("findOrderSiebeltransaction")
					.setParameter("cancellationItem", search.getCancellationItem())
					.setParameter("siebelTransactionId", search.getSiebelTransactionId())
					.setParameter("memberID", search.getMemberId())
					.getResultList()); 
			
		} else {
			if ((search.getRecordLocator() != null && search.getRecordLocator().length()>0) && (search.getStatus() != null)) {
				ordersFound.addAll(em.createNamedQuery("findOrderRecordLocatorStatus")
						.setParameter("statusList", search.getStatus()).setParameter("memberID", search.getMemberId())
						.setParameter("recordLocator", search.getRecordLocator()).getResultList());
			}
			else if (search.getRecordLocator() != null && search.getRecordLocator().length()>0) {
				ordersFound.addAll(em.createNamedQuery("findOrderRecordLocator")
						.setParameter("memberID", search.getMemberId())
						.setParameter("recordLocator", search.getRecordLocator()).getResultList());
			} else {
				if (search.getStatus() != null) {
					ordersFound.addAll(em.createNamedQuery("findOrderStatus")
							.setParameter("statusList", search.getStatus())
							.setParameter("dateIni", search.getDateInit()).setParameter("dateEnd", search.getDateEnd())
							.setParameter("memberID", search.getMemberId()).getResultList());
				} else {
					ordersFound.addAll(em.createNamedQuery("findOrder").setParameter("dateIni", search.getDateInit())
							.setParameter("dateEnd", search.getDateEnd()).setParameter("memberID", search.getMemberId())
							.getResultList());
				}
			}
		}
		for (CheckoutOrder checkoutOrder : ordersFound) {
			// Para evitar cyclic reference
			if (checkoutOrder.getItems() != null) {
				for (Item item : checkoutOrder.getItems()) {
					item.setCheckoutOrder(null);
				}
			}
			calculateTotalCostCheckout(checkoutOrder);

		}
		return ordersFound;
	}

	public int totalPaxPagante(List<ChosenFlightSegment> segments) {

		int total = 0;

		if (segments != null && segments.size() > 0) {
			total = (int) segments.stream().findAny().get().getFlightPassenger().stream()
					.filter(p -> !p.getType().equals("INF")).mapToLong(p -> p.getId()).count();
		}

		return total;

	}

	private Total getTotalTax(List<Item> items) {
		Total totalCost = new Total();
                totalCost.setMiles(0);
                totalCost.setMoney((double) 0);
		for (Item item : items) {
			if (item instanceof Fee) {
				Fee fee = (Fee) item;
				if ("BOARDING".equals(fee.getType())) {
					totalCost.setMiles(fee.getMiles());
					totalCost.setMoney(fee.getMoney());
					return totalCost;
				}
			}
		}
		return totalCost;
	}

	public void calculateTotalCostCheckout(CheckoutOrder checkout) { 
		if (checkout != null) {
			checkout.setTotalExpiredMiles(0);
			checkout.setTotalCost(0D);
			checkout.setTotalCostMiles(0);
			checkout.setTotalRefund(new Total());
			checkout.getTotalRefund().setMoney(0.0d);
			checkout.getTotalRefund().setMiles(0);
			for (Item item : checkout.getItems()) {
				if (item instanceof Miles) {
					DecimalFormat df = new DecimalFormat("#.########");
					DecimalFormat totalFormat = new DecimalFormat("#0.00");

					if (item.getVoucherType() != null) {
						if (item.getVoucherType().equals("MILEPURCHASE")) {
							df = new DecimalFormat("#0.00000000");
						} else if (item.getVoucherType().equals("MILESBENEFIT")) {
							df = new DecimalFormat("#0.000");
						}
					}
					Miles miles = (Miles) item;

					BigDecimal unitcost = new BigDecimal(miles.getUnitCost().toString());
					BigDecimal quantity = new BigDecimal(miles.getQuantity().toString());

					// TODO Rever isto aqui para efetuar arredondamento de
					// valores corretamente
					miles.setSubTotalCost(0.0d);
					miles.setSubTotalCost(
							Double.parseDouble(totalFormat.format(unitcost.multiply(quantity)).toString()));

					if ((miles.getDiscount() != null) && (miles.getDiscount() > 0.0)) {

						BigDecimal discountInit = new BigDecimal(miles.getDiscount().toString());

						BigDecimal discountDiv100 = new BigDecimal(
								discountInit.divide(new BigDecimal("100")).toString());

						BigDecimal discountMultUnit = new BigDecimal(df.format(unitcost.multiply(discountDiv100)));

						BigDecimal discountPerMile = new BigDecimal(df.format(unitcost.subtract(discountMultUnit)));

						BigDecimal discountMultQtdy = new BigDecimal(discountPerMile.multiply(quantity).toString());

						BigDecimal discountMilesPrice = new BigDecimal(
								new BigDecimal(miles.getSubTotalCost().toString()).subtract(discountMultQtdy)
										.toString());

						BigDecimal valueTotal = new BigDecimal(new BigDecimal(miles.getSubTotalCost().toString())
								.subtract(discountMilesPrice).toString());

						miles.setTotalCost(Double.parseDouble(totalFormat.format(valueTotal).toString()));
					} else {
						miles.setDiscount(0.0);
						// TODO Rever isto aqui para efetuar arredondamento de
						// valores corretamente
						miles.setTotalCost(0.0d);
						miles.setTotalCost(
								Double.parseDouble(totalFormat.format(unitcost.multiply(quantity)).toString()));
					}
					if (!miles.getStatus().equals(Status.CANCELLED.getStatus()))
						checkout.setTotalCost(checkout.getTotalCost() + miles.getTotalCost());
				}

				if (item instanceof BookingFlightSeat) {
					BookingFlightSeat bookingFlightSeat = (BookingFlightSeat) item;
					Total totalCost = new Total();
					totalCost.setMiles(0);
					totalCost.setMoney((double) 0);
					if (!bookingFlightSeat.getStatus().equals(Status.CANCELLED.getStatus())) {
						for (ChosenFlightSeat chosenFlightSeat : bookingFlightSeat.getChosenFlightSeats()) {
							if (chosenFlightSeat.getCostMiles() != null
									&& chosenFlightSeat.getChosenCost().equals("miles")) {
								  final Double percentDiscountBenefit = this.getBenefitDiscount(checkout,item);
								  if (percentDiscountBenefit>0){
									  final Integer milesBenefitDiscount = this.calcMilesDiscountPercent(chosenFlightSeat.getCostMiles(),percentDiscountBenefit);
									  totalCost.setMiles(totalCost.getMiles() + (chosenFlightSeat.getCostMiles() - milesBenefitDiscount));
								  }
								  else{
									  totalCost.setMiles(totalCost.getMiles() + (chosenFlightSeat.getCostMiles()));  
								  }	
							}
							if (chosenFlightSeat.getCostMoney() != null
									&& chosenFlightSeat.getChosenCost().equals("money")) {
								totalCost.setMoney(totalCost.getMoney() + chosenFlightSeat.getCostMoney());
							}

							if (chosenFlightSeat.getChosenFlightSeatOlder() != null
									&& bookingFlightSeat.getCancellation().toString().equals("false")) {

								if (chosenFlightSeat.getChosenFlightSeatOlder().getCostMiles() != null
										&& chosenFlightSeat.getChosenFlightSeatOlder().getChosenCost()
												.equals("miles")) {
									checkout.getTotalRefund().setMiles(checkout.getTotalRefund().getMiles()
											+ (chosenFlightSeat.getChosenFlightSeatOlder().getCostMiles()));
								}
								if (chosenFlightSeat.getChosenFlightSeatOlder().getCostMoney() != null
										&& chosenFlightSeat.getChosenFlightSeatOlder().getChosenCost()
												.equals("money")) {
									checkout.getTotalRefund().setMoney(checkout.getTotalRefund().getMoney()
											+ chosenFlightSeat.getChosenFlightSeatOlder().getCostMoney());
								}

							}

						}

						bookingFlightSeat.setTotalCost(totalCost);
					}
					

					if (bookingFlightSeat.getCancellation() != null
							&& bookingFlightSeat.getCancellation().toString().equals("false")) {
						checkout.setTotalCost(checkout.getTotalCost() + bookingFlightSeat.getTotalCost().getMoney());
						checkout.setTotalCostMiles(
								checkout.getTotalCostMiles() + bookingFlightSeat.getTotalCost().getMiles());

					} else {

						bookingFlightSeat.setTotalExpiredMiles(0);

						for (ChosenFlightSeat chosenFlightSeat : bookingFlightSeat.getChosenFlightSeats()) {
							if (chosenFlightSeat.getSiebelTransaction() != null
									&& chosenFlightSeat.getSiebelTransaction().getExpiredMiles() != null) {
								bookingFlightSeat.setTotalExpiredMiles(bookingFlightSeat.getTotalExpiredMiles()
										+ chosenFlightSeat.getSiebelTransaction().getExpiredMiles());
							}
						}
						checkout.getTotalRefund().setMoney(
								checkout.getTotalRefund().getMoney() + bookingFlightSeat.getTotalCost().getMoney());
						checkout.getTotalRefund().setMiles(
								checkout.getTotalRefund().getMiles() + bookingFlightSeat.getTotalCost().getMiles());
						checkout.setTotalExpiredMiles(
								checkout.getTotalExpiredMiles() + bookingFlightSeat.getTotalExpiredMiles());
					}
				}
				
				//Item BookingFlightBaggage
				if (item instanceof BookingFlightBaggage) {
					BookingFlightBaggage bookingFlightBaggage = (BookingFlightBaggage) item;
					Total totalCost = new Total();
					totalCost.setMiles(0);
					totalCost.setMoney((double) 0);
					if (!bookingFlightBaggage.getStatus().equals(Status.CANCELLED.getStatus()) ) {
						if (bookingFlightBaggage.getChosenFlightBaggages()!=null){
							for (ChosenFlightBaggage chosenFlightBaggage : bookingFlightBaggage.getChosenFlightBaggages()) {
								for (Baggage baggage : chosenFlightBaggage.getBaggages()){
									if (baggage!=null && baggage.getCostMiles()!=null){
									totalCost.setMiles(totalCost.getMiles() + baggage.getCostMiles());
									} 
									if (baggage!=null && baggage.getCostMoney()!=null){
									totalCost.setMoney(totalCost.getMoney() + baggage.getCostMoney());
									}  
								}
							}	
						}
						bookingFlightBaggage.setTotalCost(totalCost);
					}
					

					if (bookingFlightBaggage.getCancellation() != null && bookingFlightBaggage.getCancellation().toString().equals("false")) {
						if (bookingFlightBaggage.getTotalCost()!=null && bookingFlightBaggage.getTotalCost().getMoney()!=null) {
						   checkout.setTotalCost(checkout.getTotalCost() + bookingFlightBaggage.getTotalCost().getMoney());
						}  
						if (bookingFlightBaggage.getTotalCost()!=null && bookingFlightBaggage.getTotalCost().getMiles()!=null) {
							  final Double percentDiscount = this.getBenefitDiscount(checkout,item);
							  if (percentDiscount>0){
								  final Integer milesBenefitDiscount = this.calcMilesDiscountPercent(bookingFlightBaggage.getTotalCost().getMiles(),percentDiscount); 
								  checkout.setTotalCostMiles(checkout.getTotalCostMiles() + (bookingFlightBaggage.getTotalCost().getMiles()-milesBenefitDiscount));  
							  }
							  else{
								  checkout.setTotalCostMiles(checkout.getTotalCostMiles() + bookingFlightBaggage.getTotalCost().getMiles());  
							  }		
						}   

					} else {
						bookingFlightBaggage.setTotalExpiredMiles(0);
						for (ChosenFlightBaggage chosenFlightBaggage : bookingFlightBaggage.getChosenFlightBaggages()) {
							for (Baggage baggage : chosenFlightBaggage.getBaggages()){
									if (baggage.getSiebelTransaction() != null){
										for (SiebelTransaction siebelTransactionBaggage : baggage.getSiebelTransaction()) {
											if (siebelTransactionBaggage.getExpiredMiles() != null){
											  bookingFlightBaggage.setTotalExpiredMiles(bookingFlightBaggage.getTotalExpiredMiles() + siebelTransactionBaggage.getExpiredMiles());
											}  
										}
										
									}
							}		
						}
						checkout.getTotalRefund().setMoney(
								checkout.getTotalRefund().getMoney() + bookingFlightBaggage.getTotalCost().getMoney());
						checkout.getTotalRefund().setMiles(
								checkout.getTotalRefund().getMiles() + bookingFlightBaggage.getTotalCost().getMiles());
						checkout.setTotalExpiredMiles(
								checkout.getTotalExpiredMiles() + bookingFlightBaggage.getTotalExpiredMiles());
					}
				}

				if (item instanceof BookingFlight) {

					BookingFlight bookingFlight = (BookingFlight) item;
					Total totalCost = new Total();
					Total totalCostFare = new Total();
					Total totalAirlineTax = new Total();
					totalCost.setMiles(0);
					totalCost.setMoney((double) 0);
					totalCostFare.setMiles(0);
					totalCostFare.setMoney((double) 0);
					totalAirlineTax.setMiles(0);
					totalAirlineTax.setMoney((double) 0);
					DecimalFormat totalFormat = new DecimalFormat("#0.00");

					if (!bookingFlight.getStatus().equals(Status.CANCELLED.getStatus())) {

						if (bookingFlight.getCancellation().toString().equals("true")
								|| (bookingFlight.getSourceGds() != null
										&& bookingFlight.getSourceGds().toUpperCase().equals("AMADEUS"))) {
							totalCost = getTotalTax(checkout.getItems());
						} else {
							totalCost.setMiles((bookingFlight.getChosenFlightSegment().stream()
									.mapToInt(p -> IntegerUtil.parseSafe(p.getChosenTaxMiles())).sum())
									* (totalPaxPagante(bookingFlight.getChosenFlightSegment().stream()
											.collect(Collectors.toList()))));

							BigDecimal totalCostFormated = new BigDecimal((bookingFlight.getChosenFlightSegment()
									.stream().mapToDouble(p -> DoubleUtil.parseSafe(p.getChosenTaxMoney())).sum())
									* (totalPaxPagante(bookingFlight.getChosenFlightSegment().stream()
											.collect(Collectors.toList()))));

							totalCost.setMoney(Double.parseDouble(totalFormat.format(totalCostFormated).toString()));

						}

						BigDecimal totalCostFormated = new BigDecimal((bookingFlight.getChosenFlightSegment().stream()
								.filter(p -> p.getChosenTaxType().equals("money"))
								.mapToDouble(p -> DoubleUtil.parseSafe(p.getChosenAirlineTax())).sum())
								* (totalPaxPagante(bookingFlight.getChosenFlightSegment().stream()
										.filter(p -> p.getChosenTaxType().equals("money"))
										.collect(Collectors.toList()))));

						totalAirlineTax.setMoney(Double.parseDouble(totalFormat.format(totalCostFormated).toString()));

						totalCostFare.setMiles((bookingFlight.getChosenFlightSegment().stream()
								.mapToInt(p -> IntegerUtil.parseSafe(p.getChosenFareMiles())).sum())
								* (totalPaxPagante(
										bookingFlight.getChosenFlightSegment().stream().collect(Collectors.toList()))));

						totalCostFormated = new BigDecimal((bookingFlight.getChosenFlightSegment().stream()
								.mapToDouble(p -> DoubleUtil.parseSafe(p.getChosenFareMoney())).sum())
								* (totalPaxPagante(
										bookingFlight.getChosenFlightSegment().stream().collect(Collectors.toList()))));

						totalCostFare.setMoney(Double.parseDouble(totalFormat.format(totalCostFormated).toString()));
					}

					bookingFlight.setTotalFlightFare(totalCostFare);
					bookingFlight.setTotalBoardingTax(totalCost);
					bookingFlight.setTotalAirlineTax(totalAirlineTax);

					int milesDiscount = 0;
					int milesBenefitDiscount = 0;

					if (bookingFlight.getDiscount() != null && bookingFlight.getDiscount() > 0) {
						milesDiscount = (int) (bookingFlight.getTotalFlightFare().getMiles()
								* (bookingFlight.getDiscount() / 100));
					}

					if (bookingFlight.getDiscountMiles() != null && bookingFlight.getDiscountMiles() > 0) {
						milesDiscount = bookingFlight.getDiscountMiles();
					}
					
					final Double percentDiscount = this.getBenefitDiscount(checkout,item);
					if (percentDiscount>0){
					   milesBenefitDiscount = this.calcMilesDiscountPercent(bookingFlight.getTotalFlightFare().getMiles() - milesDiscount,percentDiscount);
					}

					bookingFlight.getTotalFlightFare()
							.setMiles((bookingFlight.getTotalFlightFare().getMiles() - milesDiscount) - milesBenefitDiscount);

					if (bookingFlight.getTotalFlightFare().getMiles() < 0) {
						bookingFlight.getTotalFlightFare().setMiles(0);
					}

					if (bookingFlight.getCancellation().toString().equals("true")) {

						bookingFlight.setTotalExpiredMiles(0);
						
						for(ChosenFlightSegment chosenFlightSegment : bookingFlight.getChosenFlightSegment()){
							if(chosenFlightSegment.getFlightPassenger() != null){
								for(FlightPassenger flightPassenger : chosenFlightSegment.getFlightPassenger()){
									if(flightPassenger.getSiebelTransaction() != null){
										for(SiebelTransaction siebelTransaction : flightPassenger.getSiebelTransaction()){
											if(siebelTransaction.getExpiredMiles() != null){
												bookingFlight.setTotalExpiredMiles(bookingFlight.getTotalExpiredMiles() + siebelTransaction.getExpiredMiles());												
											}
										} 
																				
									}
									
								}
							}
							
						}
						
						if (bookingFlight.getChosenFlightSegment().get(0).getChosenTaxType().equals("money")) {
							checkout.getTotalRefund()
									.setMoney(checkout.getTotalRefund().getMoney()
											+ bookingFlight.getTotalBoardingTax().getMoney()
											+ bookingFlight.getTotalFlightFare().getMoney());
							checkout.getTotalRefund().setMiles(checkout.getTotalRefund().getMiles()
									+ bookingFlight.getTotalFlightFare().getMiles());
						} else {
							if (bookingFlight.getChosenFlightSegment().get(0).getChosenTaxType().equals("miles")) {
								checkout.getTotalRefund().setMoney(checkout.getTotalRefund().getMoney()
										+ bookingFlight.getTotalFlightFare().getMoney() );
								checkout.getTotalRefund()
										.setMiles(checkout.getTotalRefund().getMiles()
												
												+ bookingFlight.getTotalBoardingTax().getMiles()
												+ bookingFlight.getTotalFlightFare().getMiles());
							} else {
								checkout.getTotalRefund()
										.setMoney(checkout.getTotalRefund().getMoney()
												 
												+ bookingFlight.getTotalBoardingTax().getMoney()
												+ bookingFlight.getTotalFlightFare().getMoney());
								checkout.getTotalRefund()
										.setMiles(checkout.getTotalRefund().getMiles()
												
												+ bookingFlight.getTotalBoardingTax().getMiles()
												+ bookingFlight.getTotalFlightFare().getMiles());
							}
						}

						checkout.setTotalExpiredMiles(
								checkout.getTotalExpiredMiles() + bookingFlight.getTotalExpiredMiles());
					} else {

						if (bookingFlight.getChosenFlightSegment().get(0).getChosenTaxType().equals("both")) {

							checkout.setTotalCost(
									checkout.getTotalCost() + bookingFlight.getTotalBoardingTax().getMoney()
											+ bookingFlight.getTotalFlightFare().getMoney()
											);
							checkout.setTotalCostMiles(
									checkout.getTotalCostMiles() + bookingFlight.getTotalBoardingTax().getMiles()
											+ bookingFlight.getTotalFlightFare().getMiles()
											);

						} else {
							if (bookingFlight.getChosenFlightSegment().get(0).getChosenTaxType().equals("money")) {

								checkout.setTotalCost(
										checkout.getTotalCost() + bookingFlight.getTotalBoardingTax().getMoney()
												+ bookingFlight.getTotalFlightFare().getMoney()
												);

								checkout.setTotalCostMiles(
										checkout.getTotalCostMiles() + bookingFlight.getTotalFlightFare().getMiles());

							} else {

								checkout.setTotalCost(
										checkout.getTotalCost() + bookingFlight.getTotalFlightFare().getMoney());
								checkout.setTotalCostMiles(
										checkout.getTotalCostMiles() + bookingFlight.getTotalBoardingTax().getMiles()
												+ bookingFlight.getTotalFlightFare().getMiles());

							}
						}
					}

					bookingFlight.setTotalCost(new Total());
					if (bookingFlight.getChosenFlightSegment().get(0).getChosenTaxType().equals("miles")) {
						bookingFlight.getTotalCost().setMoney(totalCostFare.getMoney());
						bookingFlight.getTotalCost().setMiles(totalCost.getMiles() + totalCostFare.getMiles());
					} else {
						bookingFlight.getTotalCost()
								.setMoney(totalCost.getMoney() + totalCostFare.getMoney() + totalAirlineTax.getMoney());
						bookingFlight.getTotalCost().setMiles(totalCostFare.getMiles());
					}
					if (bookingFlight.getDiscount() != null && bookingFlight.getDiscount() > 0) {
						bookingFlight.setSubTotalCostMiles(bookingFlight.getTotalCost().getMiles());
					}
					
					if (bookingFlight.getDiscount() != null && bookingFlight.getDiscount() > 0) {

						for (ChosenFlightSegment chosenFlightSegment : bookingFlight.getChosenFlightSegment()) {
							for (FlightPassenger passenger : chosenFlightSegment.getFlightPassenger()) {

								if (!passenger.getType().equals("INF")) {
									passenger.setRealFareMiles(
											IntegerUtil.parseSafe(chosenFlightSegment.getChosenFareMiles()
													- (int) (chosenFlightSegment.getChosenFareMiles()
															* (bookingFlight.getDiscount() / 100))));
									
									final Double percentDiscountBenefit = this.getBenefitDiscount(checkout,item);
									if (percentDiscountBenefit>0){
										  final Integer milesBenefitDiscountPassengerSeg = this.calcMilesDiscountPercent(passenger.getRealFareMiles(),percentDiscountBenefit);
										  passenger.setRealFareMiles(passenger.getRealFareMiles() - milesBenefitDiscountPassengerSeg);  
									}
								}
							}
						}

					} else {
					
						for (ChosenFlightSegment chosenFlightSegment : bookingFlight.getChosenFlightSegment()) {

							double percentSegment = ((double) (IntegerUtil
									.parseSafe(chosenFlightSegment.getChosenFareMiles())
									* bookingFlight.getChosenFlightSegment().stream()
											.mapToInt(p -> p.getFlightPassenger().size()).sum())
									/ (totalCostFare.getMiles() + milesDiscount) * 100);

							int milesPerPassenger = (int) ((milesDiscount / bookingFlight.getChosenFlightSegment().stream()
									.mapToInt(p -> p.getFlightPassenger().size()).sum()) * (percentSegment / 100));

							for (FlightPassenger passenger : chosenFlightSegment.getFlightPassenger()) {

								if (!passenger.getType().equals("INF")) {
									passenger.setRealFareMiles(
											IntegerUtil.parseSafe(chosenFlightSegment.getChosenFareMiles())
													- milesPerPassenger);
									
								    final Double percentDiscountBenefit = this.getBenefitDiscount(checkout,item);
									if (percentDiscountBenefit>0){
										  final Integer milesBenefitDiscountPassengerSeg = this.calcMilesDiscountPercent(passenger.getRealFareMiles(),percentDiscountBenefit);
										  passenger.setRealFareMiles(passenger.getRealFareMiles() - milesBenefitDiscountPassengerSeg);  
									}
									
								}
							}
						}

						int milesAdicionais = bookingFlight.getChosenFlightSegment().stream()
								.mapToInt(p -> p.getFlightPassenger().stream()
										.mapToInt(q -> IntegerUtil.parseSafe(q.getRealFareMiles())).sum())
								.sum() - totalCostFare.getMiles();

						for (int i = 0; i < milesAdicionais;) {
							for (ChosenFlightSegment chosenFlightSegment : bookingFlight.getChosenFlightSegment()) {

								for (FlightPassenger passenger : chosenFlightSegment.getFlightPassenger()) {

									if (!passenger.getType().equals("INF") && milesAdicionais > 0) {
										passenger.setRealFareMiles(IntegerUtil.parseSafe(passenger.getRealFareMiles()) - 1);
										milesAdicionais = milesAdicionais - 1;
									}
								}
							}
						}
					}
				}

				if (item instanceof Membership) {
					Membership membership = (Membership) item;
					DecimalFormat totalFormat = new DecimalFormat("#0.00");
					membership.setTotalCost(new Total());
					membership.getTotalCost().setMiles(0);
					membership.getTotalCost().setMoney(membership.getMoney());

					if (membership.getCancellation() != null && membership.getCancellation().toString().equals("false")) {
						BigDecimal totalCostFormated = new BigDecimal(checkout.getTotalCost() + membership.getMoney());
						checkout.setTotalCost(Double.parseDouble(totalFormat.format(totalCostFormated).toString()));
						
						if (membership.getRefundMoney() != null && membership.getRefundMoney() > 0) {
							BigDecimal totalRefundFormated = new BigDecimal(checkout.getTotalRefund().getMoney() + membership.getRefundMoney());
							checkout.getTotalRefund().setMoney(Double.parseDouble(totalFormat.format(totalRefundFormated).toString()));
						}
					}
					else {
						BigDecimal totalCostFormated = new BigDecimal(checkout.getTotalRefund().getMoney() + membership.getTotalCost().getMoney());
						checkout.getTotalRefund().setMoney(Double.parseDouble(totalFormat.format(totalCostFormated).toString()));
					}
				}

				if (item instanceof Fee) {
					Fee fee = (Fee) item;
					DecimalFormat totalFormat = new DecimalFormat("#0.00");
					fee.setTotalCost(new Total());
					fee.getTotalCost().setMiles(fee.getMiles());
					fee.getTotalCost().setMoney(fee.getMoney());
					if (!fee.getType().equals("BOARDING")) {
						if (fee.getCancellation().toString().equals("false")) {
							if (!(fee.getStatus().equals("PROCESSED") && fee.getCheckoutOrder() != null
									&& fee.getCheckoutOrder().getStatus().equals("PROCESSED")
									&& fee.getCheckoutOrder().getSubStatus().equals("PENDING_PAYMENT"))) {
								BigDecimal totalCostFormated = new BigDecimal(checkout.getTotalCost() + fee.getMoney());
								checkout.setTotalCost(
										Double.parseDouble(totalFormat.format(totalCostFormated).toString()));
								checkout.setTotalCostMiles(checkout.getTotalCostMiles() + fee.getMiles());
							}
						} else {
							fee.setTotalExpiredMiles(0);
							if (!fee.getType().equals("NO_MILES_BOOKING")) {
								checkout.getTotalRefund()
										.setMoney(checkout.getTotalRefund().getMoney() + fee.getMoney());
								checkout.getTotalRefund()
										.setMiles(checkout.getTotalRefund().getMiles() + fee.getMiles());
								for (SiebelTransaction transaction : fee.getSiebelTransaction()) {
									if (transaction.getExpiredMiles() != null)
										fee.setTotalExpiredMiles(
												fee.getTotalExpiredMiles() + transaction.getExpiredMiles());
									checkout.setTotalExpiredMiles(
											checkout.getTotalExpiredMiles() + fee.getTotalExpiredMiles());
								}
							}
						}
					}
				}

				if (item instanceof FamilyAccount) {
					FamilyAccount familyAccount = (FamilyAccount) item;
					DecimalFormat totalFormat = new DecimalFormat("#0.00");
					familyAccount.setTotalCost(new Total());
					familyAccount.getTotalCost().setMiles(0);
					if (familyAccount.getMoney() == null) {
						familyAccount.setMoney(0.0);
					}
					familyAccount.getTotalCost().setMoney(familyAccount.getMoney());
					BigDecimal totalCostFormated = new BigDecimal(checkout.getTotalCost() + familyAccount.getMoney());
					checkout.setTotalCost(Double.parseDouble(totalFormat.format(totalCostFormated).toString()));
				}
				
				
				if (item instanceof Product) {
					final Product product = (Product) item;
					final DecimalFormat totalFormat = new DecimalFormat("#0.00");
					product.setTotalCost(new Total());
					product.getTotalCost().setMiles(product.getMiles());
					product.getTotalCost().setMoney(product.getMoney());
					
					if (product.getCancellation().toString().equals("true")) {
					    product.setTotalExpiredMiles(0);	
					    checkout.getTotalRefund().setMoney(checkout.getTotalRefund().getMoney() + product.getMoney());
					    checkout.getTotalRefund().setMiles(checkout.getTotalRefund().getMiles() + product.getMiles());
					    if (product.getType().toString().equals("TRAVEL_INSURANCE")) {
					    	for (Travel travel : product.getTravel()) {
						    	for (FlightPassenger passageiro : travel.getFlightPassenger()) {
						    		for (SiebelTransaction transaction : passageiro.getSiebelTransaction()) {
						    			if (transaction.getExpiredMiles() != null)
											product.setTotalExpiredMiles(product.getTotalExpiredMiles() + transaction.getExpiredMiles());
										    checkout.setTotalExpiredMiles(checkout.getTotalExpiredMiles() + product.getTotalExpiredMiles());
						    			
						    		}
								}
					    	}
					    }else {
					    	for (SiebelTransaction transaction : product.getSiebelTransaction()) {
								if (transaction.getExpiredMiles() != null)
									product.setTotalExpiredMiles(product.getTotalExpiredMiles() + transaction.getExpiredMiles());
								    checkout.setTotalExpiredMiles(checkout.getTotalExpiredMiles() + product.getTotalExpiredMiles());
							}
					    }
						
					}
					else{
						final BigDecimal totalCostFormated = new BigDecimal(checkout.getTotalCost() + product.getMoney());
						checkout.setTotalCost(Double.parseDouble(totalFormat.format(totalCostFormated).toString()));
						checkout.setTotalCostMiles(checkout.getTotalCostMiles()+product.getMiles());
					}
				}
			}
		}
	}
	
	private Double getBenefitDiscount(CheckoutOrder checkout, Item item) {
		Double discountPercent = 0.0;
		if (checkout.getBenefit()!=null && checkout.getBenefit().getDiscount()!=null) {
			discountPercent = checkout.getBenefit().getDiscount();
		}
		else if (item.getBenefit()!=null && item.getBenefit().getDiscount()!=null) {
			discountPercent = item.getBenefit().getDiscount();
		}
		return discountPercent;
	}

	private Integer calcMilesDiscountPercent(final Integer value,final Double percent){
		Integer returnValue = 0;
		if (value != null && percent!=null && percent >0){
			returnValue = (int) ((value * percent)/100);
		} 
		return returnValue;
	}
}