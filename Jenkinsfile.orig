﻿#!groovy

pipeline {
    agent any
    tools {
        maven 'M3'
        //jdk 'Jdk1.7u80'
        jdk 'Jdk1.8u121'
    }
    environment {
        SONAR_URL = 'http://devops-sonar.smiles.local.br/'
        NEXUS_URL = 'http://devops-nexus.smiles.local.br/'

        NEXUS_URL_SNAP = 'http://devops-nexus.smiles.local.br/repository/smiles-snapshot/'
        NEXUS_ID_SNAP = 'smiles-snapshot'

        NEXUS_URL_CI = 'http://devops-nexus.smiles.local.br/repository/smiles-ci/'
        NEXUS_ID_CI = 'smiles-ci'

        NEXUS_URL_RELEASE = 'http://devops-nexus.smiles.local.br/repository/smiles-release/'
        NEXUS_ID_RELEASE = 'smiles-release'

        // For teste purpose only (need to be set always to TRUE! )
        RUN_TEST = false
        RUN_PRE_BUILD = true
        RUN_SITE = false
        RUN_SONAR = true
        RUN_POST_BUILD = true
        RUN_COMPILE = true
        RUN_CHECKS = false
        RUN_BUILD_CI = true

    }
    options {
        //skipDefaultCheckout()
        // Only keep the 50 most recent builds
        buildDiscarder(logRotator(numToKeepStr: '50'))
        timeout(time: 5, unit: 'MINUTES')
       // disableConcurrentBuilds()
    }


    stages {

        stage('Check commit message') {
          steps {
            script {
              current_commit_message = sh(script: '''
                git rev-list --format=%B --max-count=1 HEAD |head -2 |tail -1
              ''', returnStdout: true).trim()

              if (current_commit_message == 'Prepare for next Release') {
                currentBuild.result = 'ABORTED'
                error('Parando build por ser um commit de CI.')
              }
            }

          }
        }

        stage('Notify') {
            steps {
                echo sh(returnStdout: true, script: 'env')
                notifyBuild('STARTED')
            }
        }

        stage('Pre-Build CheckList') {
            when {
                environment name: 'RUN_CHECKS', value: 'true'
            }
            steps {
                parallel(
                        "Check Comments": {
                            checkComments()
                        },
                        "Commit Behind": {
                            checkCommitBehind()
                        },
                        "check tag for PR": {
                            checkQATagforPR()
                        }

                )
            }
        }

        stage('Compile') {
            when {
                environment name: 'RUN_COMPILE', value: 'true'
            }
            steps {
                sh 'mvn --batch-mode -B -V -U -e -Dmaven.test.failure.ignore=true  clean compile'
            }
        }

        stage('Test') {
            when {
                environment name: 'RUN_TEST', value: 'true'
            }
            steps {
                sh 'mvn -B -V -U -e --batch-mode -Dmaven.test.failure.ignore=true test'
            }
            post {
                always {
                    sh 'echo "publicando JUnit"'
                    junit 'target/surefire-reports/**/*.xml'
                }
                failure {
                    notifyBuild('FAILED')
                }
            }
        }

        stage('Site') {
            when {
                environment name: 'RUN_SITE', value: 'true'
            }
            steps {
                sh 'mvn -B -V -U -e --batch-mode site'
            }
        }

        stage('Sonar publish') {
            when {
                environment name: 'RUN_SONAR', value: 'true'
            }
            steps {
                withEnv(["JAVA_HOME=${tool 'Jdk1.8u121'}", "PATH+MAVEN=${tool 'M3'}/bin:${env.JAVA_HOME}/bin"]) {
                    sh "mvn -B -V -U -e --batch-mode sonar:sonar -Dsonar.host.url='${SONAR_URL}'"
                }
            }
        }


        stage('Pre-Build') {
            when {
                environment name: 'RUN_PRE_BUILD', value: 'true'
            }
            steps {
                //echo sh("echo ${env.BRANCH_NAME}")


                script {
                    //def branch_name = env['BRANCH_NAME']
                    def pom = readMavenPom file: 'pom.xml'
                    env['pom'] = pom
                    env['pom_version'] = pom.version


                    env['RUN_BUILD_BRANCH'] = false
                    env['RUN_BUILD_MASTER'] = false
                    if (BRANCH_NAME.startsWith("master")) {
                        echo "***** PERFORMING STEPS ON MASTER *****"
                        env['pom_new_version'] = pom.version.replace("-SNAPSHOT", "")
                        env['RUN_BUILD_MASTER'] = true

                    } else if (BRANCH_NAME.startsWith("release")) {
                        echo "***** PERFORMING STEPS ON RELEASE BRANCH *****"
                        bump_git_tag()
                        env['pom_new_version'] = pom.version.replace("-SNAPSHOT", "-${bumpci_tag}")
                        env['RUN_BUILD_BRANCH'] = true

                    } else if (BRANCH_NAME.startsWith("hotfix")) {
                        echo "***** PERFORMING STEPS ON HOTFIX BRANCH *****"
                        bump_git_tag()
                        env['pom_new_version'] = pom.version.replace("-SNAPSHOT", "-${bumpci_tag}")
                        env['RUN_BUILD_BRANCH'] = true

                    } else {
                        echo "***** BRANCHES MUST START WITH RELEASE OR HOTFIX *****"
                        echo "***** STOPPED BUILD *****"
                        currentBuild.result = 'FAILURE'
                        System.exit(-1)
                    }
                }

                sh 'echo "***** FINISHED PRE-BUILD STEP *****"'
            }
        }


        stage('Build-Snapshot') {
            when {

                environment name: 'RUN_BUILD_BRANCH', value: 'true'
            }
            steps {
                sh 'mvn --batch-mode -B -V -U -e -Dmaven.test.skip=true -Dsurefire.useFile=false deploy -DaltDeploymentRepository=${NEXUS_ID_SNAP}::default::${NEXUS_URL_SNAP}'

            }
            post {
                success {
                    // Jenkins in build artifact registry
                    archive(includes: 'target/*.jar')
                    archive(includes: 'target/*.war')
                    archive(includes: 'target/*.ear')
                }
            }
        }


        stage('Build-Branch') {
            when {
                environment name: 'RUN_BUILD_BRANCH', value: 'true'
            }
            steps {
                sh "mvn --batch-mode -B -V -U -e -DgenerateBackupPoms=false versions:set -Dmaven.test.skip=true -DnewVersion=${env.pom_new_version} -DaltDeploymentRepository=${NEXUS_ID_CI}::default::${NEXUS_URL_CI} "
                sh "mvn --batch-mode -V -U -e -Dsurefire.useFile=false clean source:jar deploy -Dmaven.test.skip=true -DaltDeploymentRepository=${NEXUS_ID_CI}::default::${NEXUS_URL_CI} -B"
                //FIXME ajustar para funcionar com javadoc

            }
            post {
                success {
                    // Jenkins in build artifact registry
                    archive(includes: 'target/*.jar')
                    archive(includes: 'target/*.war')
                    archive(includes: 'target/*.ear')
                }
            }
        }


        stage('Build-Master') {
            when {
                environment name: 'RUN_BUILD_MASTER', value: 'true'
            }
            steps {
                sh 'echo "***** MASTER STEP (BUMP VERSION) *****"'

                //Ajusta a versão para release
                sh "mvn --batch-mode -B -V -U -e -DgenerateBackupPoms=false versions:set -Dmaven.test.skip=true -DnewVersion=${env.pom_new_version}"
                sh "mvn --batch-mode -B -V -U -e -Dsurefire.useFile=false clean source:jar deploy -Dmaven.test.skip=true -DaltDeploymentRepository=${NEXUS_ID_RELEASE}::default::${NEXUS_URL_RELEASE}"
                //FIXME ajustar para funcionar com javadoc
                script {
                    sh 'git checkout master'
                    sh 'git commit -m "Version Bump" .'
                    //sh 'git checkout -- pom.xml'
                    def curr_version = env['pom_new_version']

                    def SNAPSHOT_PART = "-SNAPSHOT";
                    def versionParts = curr_version.tokenize('.');
                    def mayorVersionPart = versionParts[0];
                    def minorVersionPart = versionParts[1];
                    def fixVersionPart = versionParts[2];



                    int minorVersion = minorVersionPart.toInteger();
                    int fixVersion = fixVersionPart.toInteger();


                    def newFixVersion = fixVersion + 1
                    def developmentVersion = mayorVersionPart  + "." + minorVersion + "." + newFixVersion + SNAPSHOT_PART;
                    env['new_development_version'] = developmentVersion
                }

                //BUMP DA VERSAO
                sh "mvn versions:set --batch-mode -B -V -U -e  -Dmaven.test.skip=true -DgenerateBackupPoms=false -DnewVersion=${env.new_development_version} -DupdateMatchingVersions=true"

                script {
                  sh "git commit -m 'Prepare for next Release' ."
                  sh "git push"
                }
            }
            post {
                success {
                    // Jenkins in build artifact registry
                    archive(includes: 'target/*.jar')
                    archive(includes: 'target/*.war')
                    archive(includes: 'target/*.ear')
                }
            }

        }

        stage('HTML Reports') {
            when {
                environment name: 'RUN_SITE', value: 'true'
            }
            steps {
                publishHTML(
                        target:
                                [allowMissing         : false,
                                 alwaysLinkToLastBuild: true,
                                 keepAll              : true,
                                 reportDir            : 'target/site',
                                 reportFiles          : 'index.html',
                                 reportName           : 'Project Site'
                                ])
            }

        }

        stage('Post-Build Actions') {
            when {
                environment name: 'RUN_POST_BUILD', value: 'true'
            }
            steps {
                parallel(
                        //"Nexus Deploy": {
                        //sh 'mvn deploy -DaltDeploymentRepository=${NEXUS_ID_SNAP}::default::${NEXUS_URL_SNAP} -Dmaven.test.skip=true'
                        //},
                        "Get Sonar Status publish": {
                            sh 'echo "Not implemented YET"'
                            // CHECK http://stackoverflow.com/questions/42909439/using-waitforqualitygate-in-a-jenkins-declarative-pipeline
                        }
                )
            }
        }
    }
    post {
        success {
//            script {
//                if (RUN_BUILD_MASTER == true) {
//
//                }
//            }
            notifyBuild('SUCCESSFUL')
        }
        failure {
            notifyBuild('FAILED')
        }
        always {
         deleteDir() //F**k Dawn you jenkins !
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus = buildStatus ?: 'SUCCESSFUL'

    // Default values
    def colorName = 'RED'
    def colorCode = '#FF0000'
    def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
    def summary = "${subject} (${env.BUILD_URL})"
    def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESSFUL') {
        color = 'GREEN'
        colorCode = '#00FF00'
    } else {
        color = 'RED'
        colorCode = '#FF0000'
    }

    // Send notifications
    slackSend(color: colorCode, message: summary)

    emailext(
            subject: subject,
            body: details,
            recipientProviders: [[$class: 'DevelopersRecipientProvider']]
    )
}


def checkComments() {
    sh 'echo "Verifica se branch está com comentário no codigo."'
    script {
        def checkCommentsStatus = sh(script: '''set +x; set +e;
                                                git --no-pager log -1 --oneline 2>&1 |egrep "[A-Z]{2,}-[0-9]+";
                                                if [ ${?} -eq 0 ];
                                                then
                                                  echo Comentários OK;
                                                else
                                                  echo Comentários não referenciam nenhum Jira!;
                                                  exit 1;
                                                fi''',
                returnStatus: true);
        if (checkCommentsStatus != 0) {
            currentBuild.result = 'UNSTABLE'
        }
    }

}

def checkCommitBehind() {
    sh 'echo "Verifica se branch necessita de merge com master."'
    script {
        sh(script: '''set +x; set +e;
                      git fetch;
                      commitsBehind=$(git rev-list --left-right --count origin/master... |awk '{print $1}');
                      if [ ${commitsBehind} -ne 0 ]
                      then
                        echo "Esta branch está ${commitsBehind} commits atrás da master!"
                        exit 1
                      else
                        echo "Esta branch não tem commits atrás da master."
                      fi''')
    }

}

def checkQATagforPR() {
    sh 'echo "Verifica se o PR está com a TAG de QA."'

    script {
        sh(script: '''set +x; set +e;
                    git tag -l --points-at HEAD |grep UAT''')
    }
}


def bump_git_tag() {
    echo "Bumping Git CI Tag"

    script {
        sh "git fetch --tags"
        env['bumpci_tag'] = sh(script: '''
      current_tag=$(git tag -n9 -l |grep bumpci |awk '{print $1}' |sort -n |tail -1)
      if [[ $current_tag == '' ]]
      then
        echo 1 |tr -d '\n'
      else
        echo "${current_tag} + 1" |bc |tr -d '\n'
      fi
      ''', returnStdout: true)
        echo "${bumpci_tag}"
        sh "git tag -a ${bumpci_tag} -m bumpci && git push origin refs/tags/${bumpci_tag}"
    }
<<<<<<< HEAD
}
=======
}
>>>>>>> beb73703cde913956ddce3f6f80aaac0b2472196



