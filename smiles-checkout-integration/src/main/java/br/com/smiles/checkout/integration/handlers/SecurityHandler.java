package br.com.smiles.checkout.integration.handlers;

import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.smiles.checkout.util.ConfigProperties;

/**
 * A classe <code>SecurityHandler</code> é responsavel por tratar as questões de
 * envio dos elementos XML necessários na mensagem SOAP para a realização da
 * autenticação durante o acesso a webservices.
 * <p>
 * <b>OBS: </b>Adicionalmente esta classe tem a responsabilidade de determinar a
 * URL de endpoint do webservice.
 * <p>
 * 
 * @author adautomartins
 *
 */
public class SecurityHandler implements SOAPHandler<SOAPMessageContext> {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String SOAP_ELEMENT_PASSWORD = "Password";
	private static final String SOAP_ELEMENT_USERNAME = "Username";
	private static final String SOAP_ELEMENT_USERNAME_TOKEN = "UsernameToken";
	private static final String SOAP_ELEMENT_SECURITY = "Security";
	private static final String NAMESPACE_SECURITY = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String PREFIX_SECURITY = "wsse";

	@Override
	public boolean handleMessage(SOAPMessageContext messagecontext) {
		Boolean outbound = (Boolean) messagecontext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		// only concerned with outbound
		if (outbound) {
			try {
				String service = String.valueOf(messagecontext.get(MessageContext.WSDL_SERVICE));

				String user = getWebserviceProperty("user", service);
				String password = getWebserviceProperty("password", service);
				String endpoint = getWebserviceProperty("endpoint", service);

				// Check if endpoint was found in property file
				if (endpoint == null || endpoint.isEmpty()) {
					logger.warn(String.format(
							"Não foi encontrado nenhum valor de endpoint para o webservice %s. Será utilizado o endpoint definido no arquivo de WSDL.",
							service));
				} else {
					messagecontext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
				}

				// Attach the username token to the message
				SOAPEnvelope soapEnvelope = messagecontext.getMessage().getSOAPPart().getEnvelope();

				SOAPHeader header = soapEnvelope.getHeader();
				if (header == null) {
					header = soapEnvelope.addHeader();
				}

				SOAPElement soapElementSecurityHeader = header.addChildElement(SOAP_ELEMENT_SECURITY, PREFIX_SECURITY,
						NAMESPACE_SECURITY);

				SOAPElement soapElementUsernameToken = soapElementSecurityHeader
						.addChildElement(SOAP_ELEMENT_USERNAME_TOKEN, PREFIX_SECURITY);
				SOAPElement soapElementUsername = soapElementUsernameToken.addChildElement(SOAP_ELEMENT_USERNAME,
						PREFIX_SECURITY);
				soapElementUsername.addTextNode(user);

				SOAPElement soapElementPassword = soapElementUsernameToken.addChildElement(SOAP_ELEMENT_PASSWORD,
						PREFIX_SECURITY);
				soapElementPassword.addTextNode(password);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	private String getWebserviceProperty(String atributo, String serviceId) {
		String value = ConfigProperties.getProperty(String.format("webservice.%s.%s", atributo, serviceId));
		if (value == null || value.isEmpty()) {
			value = ConfigProperties.getProperty(String.format("webservice.%s", atributo));
		}

		return value;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return false;
	}

	@Override
	public void close(MessageContext context) {
	}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

}
